# Simulation und Analyse von Strategien zur Eindämmung von Epidemien in Multi-Agenten-Systemen

## Beigefügte Dateien

In der folgenden Tabelle ist eine grobe Übersicht zu den beigefügten Ordnern gegeben. Eine detailiertere Auflistung der Videos in _/videos_ kann in den Anlagen der Bachelorarbeit gefunden werden.

| **Ordner**           | **Inhalt**                                                                                       |
|----------------------|--------------------------------------------------------------------------------------------------|
| _/thesis_            | Die Bachelorarbeit als PDF                                                                     |
| _/simulation_        | Quellcode der Simulations-Implementierung                                                        |
| _/visual_            | Quellcode von genutzten Python-Skripten zur Erstellung von Graphiken und Videos für die Arbeit |
| _/experiments_       | Rohdaten zu den einzelnen Experimenten. Die Daten zum Experiment mit der ID *i* sind jeweils im Unterordner */output\_i* zu finden. */output\_-1* enthält die Daten zum Experiment zur Effektivität der Ressourcen-Suche (siehe **4.6 Anmerkungen**).
| _/videos_      | Beispiel-Videos zu den Modellen                                                                  |



