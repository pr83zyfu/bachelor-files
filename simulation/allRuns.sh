#! /usr/bin/bash

if [ $# -ne 2 ]
	then echo "Incorrect number of Arguments. Use: \"./allRuns.sh [number of runs] [random first seed]\""
	exit 1
fi

OUTPUT_DIRECTORY=./output

number_of_runs=$1
random_first_seed=$2

# Clean
./clean_output.sh $OUTPUT_DIRECTORY

for ((i = 7; i <= 7; i++)); 
do 
    echo "================================================================================"
    echo "RUNNING with run type: $i "
    echo "================================================================================"
    
    identifier_suffix=_$i

    type_output_directory=$OUTPUT_DIRECTORY/output$identifier_suffix

    mkdir $type_output_directory
    bash ./fullRunAvgAndGraphs.sh $number_of_runs $random_first_seed $identifier_suffix $type_output_directory $i
done
