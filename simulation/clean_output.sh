#! /usr/bin/bash

if [ $# -ne 1 ]
	then echo "Incorrect number of Arguments. Use: \"./clean_outpur.sh [output directory]\""
	exit 1
fi

rm -rf $1/*
