#! /usr/bin/bash

if [ $# -ne 5 ]
	then echo "Incorrect number of Arguments. Use: \"./fullRunAndGraphs.sh [number of runs] [random first seed] [identifier suffix for graph files] [output directory] [run type]\""
	exit 1
fi

# Load parameters
number_of_runs=$1
first_seed=$2
identifier_suffix_for_graph_files=$3
output_directory=$4
run_type=$5

./runN.sh $number_of_runs $first_seed $output_directory $run_type

avg_directory=$output_directory/avg;
mkdir $avg_directory

python3 ./run_avg.py $output_directory
python3 ./run_dist.py $output_directory
python3 ../visual/revised/seir_graphs.py $avg_directory/infection_sums.csv $output_directory/seir_graphs$identifier_suffix_for_graph_files.pgf
python3 ../visual/revised/resource_graph.py $avg_directory/resource_sum.csv $output_directory/resource_graph$identifier_suffix_for_graph_files.pgf
python3 ../visual/revised/rainclouds.py $output_directory/distribution.csv imax $output_directory/infection_raincloud$identifier_suffix_for_graph_files.pgf
python3 ../visual/revised/rainclouds.py $output_directory/distribution.csv rmax $output_directory/resources_raincloud$identifier_suffix_for_graph_files.pgf