/* ProcessModel.hpp */

#ifndef PROCESSMODEL
#define PROCESSMODEL

#include <vector>

#include <boost/mpi.hpp>
#include "repast_hpc/Schedule.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "repast_hpc/SharedContinuousSpace.h"
#include "repast_hpc/GridComponents.h"

#include "SimulationProperties.hpp"
#include "base/SimulationArena.hpp"
#include "dezi/DeziBot.hpp"
#include "dezi/DeziBotPackage.hpp"
#include "dezi/DeziBotPackageProvider.hpp"
#include "dezi/DeziBotPackageReceiver.hpp"
#include "log/Logger.hpp"

class ProcessModel{
	int rank;

	SimulationProperties* properties;
	repast::ScheduleRunner* scheduleRunner;

	SimulationArena* arena;

	/* Logging */
	std::vector<Logger*> loggers;
	
	void createAgents();
	void createLoggers();
public:
	ProcessModel(SimulationProperties* simProps, repast::ScheduleRunner* _runner);
	~ProcessModel();
	void setup();
	void setupSchedule(repast::ScheduleRunner& runner);
	void update();
};

#endif