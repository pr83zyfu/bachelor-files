#ifndef TESTRUNS
#define TESTRUNS

enum RunType {
    /*
    * Baseline just with resource-collection to check against
    */
    BASELINE_WITHOUT_SEIR = 0,
    /*
    * Baseline for transmitted sickness, but withoid adaptation of the agents
    */
    BASELINE_WITH_SEIR = 1,
    /*
    * When agents notice infection, they stand still to avoid carrying the sickness around
    */
    INFECTIOUS_STAND_STILL = 2,
    /*
    * When susceptible agents notice an infectious agent within transmission range, they turn around / move away
    */
    SUSCEPTIBLE_AVOID = 3,
    /*
    * 2 and 3 together
    */
    SUSCEPTIBLE_AVOID_AND_INFECTIOUS_STAND_STILL = 4,
    /*
    * Infectious draw warn pheromone behind them and other infectious follow warn pheromone
    * to cluster infectious agents. Susceptible agents do not react to warn pheromone
    */
    INFECTIOUS_CLUSTERING = 5,
    /*
    * Infectious cant "smell" resource indicator pheromone and can only do random walk
    */
    INFECTIOUS_ONLY_CRW = 6,
    /*
    * Infectious randomly smell non-existent resource indicator and follow them
    * this can be simplified, by just letting the infectious agents do only crw and
    * randomly walk straight lines in between
    */
    INFECTIOUS_PHANTOM_RESOURCE_INDICATOR = 7,
};

#endif