/* SimulationProperties.hpp */

#ifndef SIMULATIONPROPERTIES
#define SIMULATIONPROPERTIES

#include <boost/mpi.hpp>

#include "repast_hpc/Properties.h"
#include "repast_hpc/Point.h"

#include "RunType.hpp"
struct TechnicalProperties {
    public:
    int randomSeed;
};

struct GeneralProperties {
    public:
    int timeMax;
    int initialAgentsTotal;
    int initialAgentsSusceptible;
    int initialAgentsExposed;
    int initialAgentsInfectious;
    int initialAgentsRecovered;
};

struct ArenaProperties {
    public:
    double width;
    double height;
};

struct AgentProperties {
    public:
    double speed;
    double randomDontChangeDirectionProbability;
    double randomRotationDelta;

    double infectedSpeed;
    double transmissionProbability;
    double transmissionRadius;
    int exposedTime;
    int infectiousTime;
    int realizationTime;

    RunType runType;

    repast::Point<double>* quarantaineZone;

    double resourceDetectionRadius;
    double resourceCollectionRadius;
    int resourceWaitAfterCollectionTime;

    int pheromoneDetectionToSides;
    int pheromoneDetectionToFront;
    double pheromoneDrawRadius;
    int pheromoneDrawWidth;
    int pheromoneDrawHeight;
    //double pheromoneDetectionRadius;
    bool pheromoneResourceIndicatorUse;
    //double pheromoneResourceIndicatorDrawRadius;
    double pheromoneResourceIndicatorDrawLength;
    //bool pheromoneWarningUse;
    //double pheromoneWarningDrawRadius;

    double phantomStartDetectionChance;
    double phantomStopDetectionChance;
};

struct ResourceGeneratorProperties {
    public:
    int count;
    double size;
};

struct PheromoneProperties {
    public:
    double lifetime;
};

struct PheromoneCollectionProperties {
    public:
    PheromoneProperties* resourceIndicator;
    PheromoneProperties* warning;
};

struct LogProperties {
    public:
    bool recordAgentState;
    bool recordAgentStateCondensed;
    bool recordInfectionSums;
    bool recordResourceStateCondensed;
    bool recordResourceSum;
    bool recordResourceIndicatorPheromone;
    bool recordWarningPheromone;
    std::string outputDirectory;
};

struct SimulationProperties {
    public:
    boost::mpi::communicator* communicator;
    const repast::Properties* props;
    TechnicalProperties* technical;
    GeneralProperties* general;
    ArenaProperties* arena;
    AgentProperties* agent;
    ResourceGeneratorProperties* resourceGenerator;
    PheromoneCollectionProperties* pheromone;
    LogProperties* log;
    SimulationProperties();
    SimulationProperties(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm);
    ~SimulationProperties();
    void checkLogicalConsistency();
    int strToInt(const std::string& string);
    double strToDouble(const std::string& string);
    bool strToBool(const std::string& string);
    int intFromProp(const std::string& key);
    double doubleFromProp(const std::string& key);
    bool boolFromProp(const std::string& key);
    std::string strFromProp(const std::string& key);
};

#endif