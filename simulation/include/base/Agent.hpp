/* Agent.hpp */

#ifndef AGENT
#define AGENT

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "repast_hpc/SharedContinuousSpace.h"
#include "repast_hpc/Point.h"

#include "base/Vector2D.hpp"
#include "SimulationProperties.hpp"

/* Forward Declerations to avoid cyclic includes */
class SimulationArena;

class Agent {
    protected:
    // TEMP: constants that should become simulation properties

    // General positioning and repast backend information
    repast::AgentId   id;
    SimulationArena* space;

    // General attributes
    //double rotation;
    Vector2D velocity;
    double currentSpeed;
    double randomDontChangeDirectionProbability;
    double randomRotationDelta;

    // Methods
    void randomWalk();

    public:
    Agent() {}
    Agent(repast::AgentId _id) { id = _id; }
    Agent(repast::AgentId _id,
        SimulationArena* _space,
        AgentProperties* agentProperties);
    Agent(repast::AgentId _id,
        SimulationArena* _space,
        AgentProperties* agentProperties,
        double _speed,
        Vector2D _velocity);
    ~Agent() {}

    /* Required Getters */
    virtual repast::AgentId& getId() { return id; }
    virtual const repast::AgentId& getId() const { return id; }

    /* Getters specific to this kind of Agent (mostly used for packaging) */
    double getSpeed() { return currentSpeed; }
    Vector2D getVelocity() { return velocity; }
    repast::Point<double> getPosition();


    /* Setter */
    void set(int currentRank);
    void setSpeed(double newSpeed);
    void setVelocity(Vector2D newVelocity);

    /* General Functions for initial setup and periodic updates */
    virtual void setup() = 0;
    virtual void update(const double currentTick) = 0;

    /* Actions */

    /**
     * @param angle the angle to rotate by in radians
    */
    void rotate(double angle);
    void rotateTowards(double x, double y);
    void rotateTowards(repast::Point<double> point);
    void rotateTowards(repast::Point<int> point);
    void rotateToPointToFrontAndRight(double distanceToFront, double distanceToRight);
    void turnAround();
    void moveTo(double x, double y);
    void moveForward();
    void moveForwardWithoutCollision(double distance);
};

#endif