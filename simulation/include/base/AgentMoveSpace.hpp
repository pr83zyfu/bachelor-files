/* AgentMoveSpace.hpp */

#ifndef AGENTMOVESPACE
#define AGENTMOVESPACE

#include "repast_hpc/Point.h"
#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContinuousSpace.h"
#include "repast_hpc/SharedContext.h"

#include "base/Vector2D.hpp"
#include "SimulationProperties.hpp"

/* Forward Declarations */
class DeziBot;
class DeziBotPackageProvider;
class DeziBotPackageReceiver;

class SimulationArena;

enum WallDirection {
    EAST = 0,
    SOUTH = 1,
    WEST = 2,
    NORTH = 3,
};

class AgentMoveSpace {
    protected:
        repast::SharedContext<DeziBot>* context;
        repast::SharedContinuousSpace<DeziBot, repast::StrictBorders, repast::SimpleAdder<DeziBot>>* space;
        DeziBotPackageProvider *provider;
        DeziBotPackageReceiver *receiver;
        repast::Point<double>* origin;
        repast::Point<double>* extent;
        repast::Point<double>* maxPoint;

        // private (helper) methods
        repast::Point<double>* getWallIntersection(WallDirection wall, Vector2D* position, Vector2D* velocity);
        Vector2D* reflectVectorOffWall(WallDirection wall, Vector2D* vector);
        double calculateMoveTowardsWallIntersection(Vector2D* position, Vector2D* velocity, WallDirection wall, repast::Point<double>* wallIntersection, double remainingDistance);
    public:
        AgentMoveSpace(
            repast::SharedContext<DeziBot>* _context, 
            boost::mpi::communicator* _comm, 
            repast::Point<double>* _origin, 
            repast::Point<double>* _extent, 
            repast::Point<double>* _maxPoint, 
            double minimalBuffer,
            SimulationArena* _arena,
            AgentProperties* _agentProperties
        );
        ~AgentMoveSpace();

        repast::Point<double> getLocation(repast::AgentId &id);
        double getDistance(repast::Point<double> p1, repast::Point<double> p2){return space->getDistance(p1, p2);}
        double getDistanceSq(repast::Point<double> p1, repast::Point<double> p2){return space->getDistanceSq(p1, p2);}

        void moveTo(repast::AgentId &id, double x, double y);
        void moveToRandomLocation(repast::AgentId id);

        /**
         * @return the new rotation
        */
        Vector2D* moveForwardAndBounceOffWalls(repast::AgentId &id, Vector2D* velocity);

        void synchronize();
};

#endif