/* FilterPredicates.hpp */

#ifndef FILTERPREDICATES
#define FILTERPREDICATES

#include "repast_hpc/Point.h"
#include "repast_hpc/Grid.h"
#include "base/SEIRState.hpp"

template <typename A>
struct Agent_Filter_Predicate {
    public:
        virtual bool operator()(const boost::shared_ptr<A>& ptr) = 0;
};

template <typename A>
struct Radius_Filter_Predicate : public Agent_Filter_Predicate<A> {
    private:
        repast::Grid<A, double>* space;
        repast::Point<double>* center;
        double radiusSq;
    public:
	    Radius_Filter_Predicate(repast::Grid<A, double>* _space, repast::Point<double>* _center, double _radius){
            space = _space;
            center = _center;
            radiusSq = _radius * _radius;
        }

	    virtual bool operator()(const boost::shared_ptr<A>& ptr) {
		    return space->getDistanceSq(*center, ptr->getPosition()) <= radiusSq;
	    }
};

template <typename A>
struct SEIR_Filter_Predicate : public Agent_Filter_Predicate<A> {
    private:
        SEIRState state;
    public: 
        SEIR_Filter_Predicate(SEIRState _state){
            state = _state;
        }

        virtual bool operator()(const boost::shared_ptr<A>& ptr){
            return ptr->getInfectionState() == state;
        }
};

#endif