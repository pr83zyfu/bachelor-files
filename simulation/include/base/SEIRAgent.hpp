/* SEIRAgent.hpp */

#ifndef SEIRAGENT
#define SEIRAGENT

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "repast_hpc/SharedContinuousSpace.h"
#include "repast_hpc/Point.h"

#include "base/Vector2D.hpp"
#include "SimulationProperties.hpp"
#include "base/Agent.hpp"
#include "base/SEIRState.hpp"

class SEIRAgent : public Agent {
    protected:
    // Infection attributes
    SEIRState infectionState;
    double transmissionProbability;
    double transmissionRadius;
    bool knowsAboutInfection;
    int exposedTimer;
    int infectiousTimer;
    int realizationTimer;

    // Values for starting the timers
    int exposedTimerStart;
    int infectiousTimerStart;
    int realizationTimerStart;

    // Attributes for symptoms
    double normalSpeed;
    double infectedSpeed;

    bool isInfectable();
    bool isSymptomatic() { return infectionState == INFECTIOUS && knowsAboutInfection; }
    void updateInfectionState(const double currentTick);

    public:
    SEIRAgent();
    SEIRAgent(repast::AgentId _id) { id = _id; }
    SEIRAgent(
        repast::AgentId _id,
        SimulationArena* _space,
        AgentProperties* agentProperties,
        double _speed,
        Vector2D _velocity,
        SEIRState _infectionState,
        bool _knowsAboutInfection,
        int _exposedTimer,
        int _infectiousTimer,
        int _realizationTimer);
    SEIRAgent(
        repast::AgentId _id,
        SimulationArena* _space,
        AgentProperties* agentProperties);
    ~SEIRAgent();

    /* Getters specific to this kind of Agent (mostly used for packaging) */
    SEIRState getInfectionState() { return infectionState; }
    bool getKnowsAboutInfection() { return knowsAboutInfection; }
    int getExposedTimer() { return exposedTimer; }
    int getInfectiousTimer() { return infectiousTimer; }
    int getRealizationTimer() { return realizationTimer; }

    void switchSEIRstates(SEIRState _oldState, SEIRState _newState);
};

#endif