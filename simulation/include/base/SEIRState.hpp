/* SEIRState.hpp */

#ifndef SEIRSTATE
#define SEIRSTATE

// https://en.wikipedia.org/wiki/Compartmental_models_in_epidemiology#The_SEIR_model
enum SEIRState {
    SUSCEPTIBLE = 0,
    EXPOSED = 1,
    INFECTIOUS = 2,
    RECOVERED = 3,
};

#endif