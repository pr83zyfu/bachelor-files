/* SimulationArena.hpp */

#ifndef SIMULATIONARENA
#define SIMULATIONARENA

#include "repast_hpc/Point.h"
#include "repast_hpc/Context.h"
#include "repast_hpc/SharedContext.h"

#include "base/SEIRState.hpp"
#include "SimulationProperties.hpp"
#include "resource/Resource.hpp"
#include "pheromone/IPheromoneMap.hpp"

/* Forward Declerations to avoid cyclic includes */
class DeziBot;
class ResourceSpace;
class AgentMoveSpace;

class SimulationArena {
    private:
    int rank;
    SimulationProperties* properties;
    repast::Point<double>* origin;
    repast::Point<double>* extent;
    repast::Point<double>* maxPoint;

    repast::SharedContext<DeziBot>* agentContext;
    AgentMoveSpace* agentSpace;

    repast::SharedContext<Resource>* resourceContext;
    ResourceSpace* resourceSpace;

    /* Pheromone Maps */
    IPheromoneMap* resourceIndicatorPheromone;
    IPheromoneMap* warningPheromone;

    /* Private Helper Methods */
    void checkForResourcePickup();
    void checkForBotsToPickup(boost::shared_ptr<Resource> resource, double collectionDistanceSq);

    public:
    SimulationArena(SimulationProperties* _properties, int _rank);
    ~SimulationArena();

    /* Getters */
    repast::Point<double>* getOrigin() { return origin; }
    repast::Point<double>* getExtent() { return extent; }
    repast::Point<double>* getMaxPoint() { return maxPoint; }

    double getDistance(repast::Point<double> p1, repast::Point<double> p2);
    double getDistanceSq(repast::Point<double> p1, repast::Point<double> p2);

    repast::SharedContext<DeziBot>* getAgentContext() { return agentContext; }
    AgentMoveSpace* getAgentSpace() { return agentSpace; }

    repast::SharedContext<Resource>* getResourceContext() { return resourceContext; }
    ResourceSpace* getResourceSpace() { return resourceSpace; }

    IPheromoneMap* getResourceIndicatorPheromone() { return resourceIndicatorPheromone; }
    IPheromoneMap* getWarninigPheromone() { return warningPheromone; }

    void update(const double currentTick);

    void constructNewAgent(int rank, int i, SEIRState infectionState);

    typename repast::Context<DeziBot>::const_bytype_iterator queryAgents();
    typename repast::Context<Resource>::const_bytype_iterator queryResources();
};

#endif