/* Vector2D.hpp */

#ifndef VECTOR2D
#define VECTOR2D

#include <vector>

#include "repast_hpc/Point.h"

class Vector2D {
    private:
    double x;
    double y;
    public:
    /* Constructors */
    Vector2D();
    Vector2D(double _x, double _y);
    Vector2D(repast::Point<double> point);
    Vector2D(std::vector<double> std_vector);
    Vector2D(const Vector2D& other) {
        x = other.getX();
        y = other.getY();
    }
    ~Vector2D();

    Vector2D& operator=(const Vector2D& other) {
        if (this == &other)
            return *this;

        x = other.getX();
        y = other.getY();
        return *this;
    }

    /* Getter */
    double getX() const { return x; }
    double getY() const { return y; }

    /* Setter */
    void setX(double _x) { x = _x; }
    void setY(double _y) { y = _y; }

    /* Special Setter */
    void setCopy(Vector2D& other) { x = other.getX(); y = other.getY(); }
    void setCopy(Vector2D* other) { x = other->getX(); y = other->getY(); }
    void setCopy(repast::Point<double>* point) { x = point->getX(); y = point->getY(); }
    void setAdd(Vector2D& other) { x += other.x; y += other.y; }
    void setAdd(Vector2D* other) { x += other->getX(); y += other->getY(); }
    void setAdd(double otherX, double otherY) { x += otherX; y += otherY; }

    /* Methods */
    void scale(double scalar);
    void scaleTo(double scalar);
    void normalise();

    /**
     * @param angle angle in radians
    */
    void rotate(double angle);
    double length();

    double dotProduct(Vector2D* other);

    std::string toString();
    repast::Point<double> toPoint();
    std::vector<double> toVector();
};

/* Constants */
// Vector2D* V2D_ZERO    = new Vector2D(0.0, 0.0);
// Vector2D* V2D_UP      = new Vector2D(0.0, 1.0);
// Vector2D* V2D_DOWN    = new Vector2D(0.0, -1.0);
// Vector2D* V2D_LEFT    = new Vector2D(-1.0, 0.0);
// Vector2D* V2D_RIGHT   = new Vector2D(1.0, 0.0);

#endif