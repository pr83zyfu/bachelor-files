/* DeziBot.hpp */

#ifndef DEZIBOT
#define DEZIBOT

#include <vector>

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "repast_hpc/SharedContinuousSpace.h"

#include "RunType.hpp"
#include "SimulationProperties.hpp"
#include "base/Vector2D.hpp"
#include "base/SEIRAgent.hpp"
#include "resource/ResourceSpace.hpp"
#include "pheromone/IPheromoneMap.hpp"
#include "base/SEIRState.hpp"

/* Forward Declerations to avoid cyclic includes */
class Resource;
class SimulationArena;

// This state describes the behaviour of a single robot searching for resource
enum ResourceSearchState {
    EXPLORE = 0,    // Explore represents a robot walking around (randomnly) in the environment and looking for resources or Indicator Pheromones
    FOLLOW = 1,     // Follow represents a robot that found a indicator pheromone and tries to follow it to the resource
    DRAW = 2,       // Draw represents a robot that found a resource and draws resource indicator to guide other robots
    WAIT = 3,       // Wait represents a robot that is waiting at the resource for it to be "collected"
};                  //  (This can either be a robot that returned from drawing an indicator or a robot that found the resource via an indicator)

class DeziBot : public SEIRAgent {
    public:
    typedef std::vector<repast::AgentId> ResourceList;
    typedef boost::shared_ptr<std::vector<repast::AgentId>> ResourceListPointer;
    private:
    // TEMP: constants that should become simulation properties

    // type of run and reduction strategy
    RunType runType;

    repast::Point<double>* quarantaineZone;

    // Resource collection attributes
    ResourceSearchState resourceSearchState;
    double resourceDetectionRadius;
    double resourceCollectionRadius;
    ResourceListPointer alreadyCollectedResources;
    int resourceWaitAfterCollectionTimer;
    int resourceWaitAfterCollectionTimerStart;

    // Agent Pheromone settings
    double pheromoneDetectionToSides;
    double pheromoneDetectionToFront;
    double pheromoneDrawRadius;
    int pheromoneDrawWidth;
    int pheromoneDrawHeight;
    bool pheromoneResourceIndicatorUse;
    double pheromoneResourceIndicatorDrawLength;

    // Phantom Resource detection settings
    double phantomStartDetectionChance;
    double phantomStopDetectionChance;
    bool isFollowingPhantomPheromone;

    // Infectious clustering settings
    bool isFollowingWarnPheromone;

    // For draw mode to measure length of drawn line
    double lengthLeftToDraw;

    // For moving out of warning pheromone
    bool movingOutOfWarningArea;

    bool foundResourceViaIndicator;

    // Methods
    void switchResourceSearchState(ResourceSearchState oldState, ResourceSearchState newState);
    void baseBehaviour(const double currentTick, bool canInteractWithResourceIndicator = true, bool noRandomWalk = false);
    boost::shared_ptr<Resource> getFirstDetectableResource();
    boost::shared_ptr<Resource> getFirstDetectableUncollectedResource();
    Vector2D getVectorAwayFromAgents(const double currentTick, std::vector<boost::shared_ptr<DeziBot>>& agents);
    void getInfectiousRobotsInTransmissionRange(const double currentTick, std::vector<boost::shared_ptr<DeziBot>>& out);
    /**
     * gets the intensities of the pheromone in front of the robot.
     *
     * @param out the output of intensitiy values given as a 2D-Vector. The outer vector contains valuesToFront many rows.
     *      The first one is one length unit in front of the bot.
     *      The last one ist valuesToFriont many steps in front of the robot.
     *      Each row contains 1 + (2 * valuesToEachSide) many intensities. The first being far to the left and the last one far to the right.
     *      The value at index valuesToEachSide is directly in front of the robot
    */
    void getIntensitiesInfrontOfRobot(IPheromoneMap* pheromone, int valuesToFront, int valuesToEachSide, const double currentTick, std::vector<std::vector<double>>& out);

    /**
     *  @return whether or not any detectable pheromone was found in front of the robot
    */
    bool detectablePheromoneInFront(IPheromoneMap* pheromone, const double currentTick);

    /**
     * @return whether or not any pheromone was still found
    */
    bool changeDirectionToFollowPheromone(IPheromoneMap* pheromone, const double currentTick);

    bool isKnownInfectious();
    bool detectedWarning(const double currentTick);

    public:
    DeziBot();
    DeziBot(repast::AgentId _id);
    DeziBot(
        repast::AgentId _id,
        SimulationArena* _space,
        AgentProperties* agentProperties,
        double _speed,
        Vector2D _velocity,
        SEIRState _infectionState,
        bool _knowsAboutInfection,
        int _exposedTimer,
        int _infectiousTimer,
        int _realizationTimer,
        ResourceSearchState _resourceSearchState,
        double _lengthLeftToDraw,
        bool _movingOutOfWarningArea,
        ResourceListPointer _resourceList,
        int _resourceWaitAfterCollectionTimer,
        bool _isFollowingPhatomPheromone,
        bool _isFollowingWarnPheromone,
        bool _foundResourceViaIndicator);
    DeziBot(
        repast::AgentId _id,
        SimulationArena* _space,
        AgentProperties* agentProperties);
    ~DeziBot();

    /* for updating remote agents*/
    void updateAttributes(
        double _speed,
        Vector2D _velocity,
        SEIRState _infectionState,
        bool _knowsAboutInfection,
        int _exposedTimer,
        int _infectiousTimer,
        int _realizationTimer,
        ResourceSearchState _resourceSearchState,
        ResourceListPointer _resourceList,
        int _resourceWaitAfterCollectionTimer,
        double _lengthLeftToDraw,
        bool _movingOutOfWarningArea,
        bool _isFollowingPhatomPheromone,
        bool _isFollowingWarnPheromone,
        bool _foundResourceViaIndicator);

    /* Getters specific to this kind of Agent (mostly used for packaging) */
    ResourceSearchState getResourceSearchState() { return resourceSearchState; }
    ResourceListPointer getAlreadyCollectedResources() { return alreadyCollectedResources; }
    bool hasAlreadyCollectedResource(const repast::AgentId& resourceId);
    void addCollectedResource(repast::AgentId resourceId);
    int getResourceWaitAfterCollectionTimer() { return resourceWaitAfterCollectionTimer; }
    double getLengthLeftToDraw() { return lengthLeftToDraw; }
    bool getMovingOutOfWarningArea() { return movingOutOfWarningArea; }
    bool getIsFollowingPhantomPheromone() { return isFollowingPhantomPheromone; }
    bool getIsFollowingWarnPheromone() { return isFollowingWarnPheromone; }
    bool getFoundResourceViaIndicator() { return foundResourceViaIndicator; }

    /* Setter */

    /* General Functions for initial setup and periodic updates */
    virtual void setup();
    virtual void update(const double currentTick);
    void updateWithWarningPheromone(const double currentTick);
    void updateWithQuarantaineZone(const double currentTick);

    /* Actions */
    private:
    void updateBaselineNoSeir(const double currentTick);
    void updateBaselineSeir(const double currentTick);
    void updateInfectiousStandStill(const double currentTick);
    void updateSusceptibleAvoid(const double currentTick);
    void updateSusceptibleAvoidAndInfectiousStandStille(const double currentTick);
    void updateInfectiousClustering(const double currentTick);
    void updateInfectiousOnlyCRW(const double currentTick);
    void updateInfectiousPhantomResourceIndicator(const double currentTick);

};

#endif