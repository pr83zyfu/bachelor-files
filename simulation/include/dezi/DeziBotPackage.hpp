/* DeziBotPackage.hpp */

#ifndef DEZIPACKAGE
#define DEZIPACKAGE

#include <boost/serialization/vector.hpp>

#include "base/Vector2D.hpp"
#include "dezi/DeziBot.hpp"

/* Serializable Agent Package */
struct DeziBotPackage {
    public:
    /* Attributes */
    int         id;
    int         rank;
    int         type;
    int         currentRank;
    double      speed;
    double      velocityX;
    double      velocityY;
    SEIRState   infectionState;
    bool        knowsAboutInfection;
    int         exposedTimer;
    int         infectiousTimer;
    int         realizationTimer;
    ResourceSearchState resourceSearchState;
    DeziBot::ResourceList alreadyCollectedResources;
    int         resourceWaitAfterCollectionTimer;
    double      lengthLeftToDraw;
    bool        movingOutOfWarningArea;
    bool        isFollowingPhantomPheromone;
    bool        isFollowingWarnPheromone;
    bool        foundResourceViaIndicator;

    /* Constructors */
    DeziBotPackage(); // For serialization
    DeziBotPackage(
        int _id,
        int _rank,
        int _type,
        int _currentRank,
        double _speed,
        Vector2D _velocity,
        SEIRState _infectionState,
        bool _knowsAboutInfection,
        int _exposedTimer,
        int _infectiousTimer,
        int _realizationTimer,
        ResourceSearchState _resourceSearchState,
        DeziBot::ResourceList _alreadyCollectedResources,
        int _resourceWaitAfterCollectionTimer,
        double _lengthLeftToDraw,
        bool _movingOutOfWarningArea,
        bool _isFollowingPhantomPheromone,
        bool _isFollowingWarnPheromone,
        bool _foundResourceViaIndicator);

    /* For archive packaging */
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version) {
        ar& id;
        ar& rank;
        ar& type;
        ar& currentRank;
        ar& speed;
        ar& velocityX;
        ar& velocityY;
        ar& infectionState;
        ar& knowsAboutInfection;
        ar& exposedTimer;
        ar& infectiousTimer;
        ar& realizationTimer;
        ar& resourceSearchState;
        ar& alreadyCollectedResources;
        ar& resourceWaitAfterCollectionTimer;
        ar& lengthLeftToDraw;
        ar& movingOutOfWarningArea;
        ar& isFollowingPhantomPheromone;
        ar& isFollowingWarnPheromone;
        ar& foundResourceViaIndicator;
    }
};

#endif