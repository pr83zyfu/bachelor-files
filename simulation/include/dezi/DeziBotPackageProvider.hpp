/* DeziBotPackageProvider.hpp */

#ifndef DEZIBOTPACKAGEPROVIDER
#define DEZIBOTPACKAGEPROVIDER

#include <vector>

#include "repast_hpc/SharedContext.h"
#include "repast_hpc/AgentRequest.h"

#include "dezi/DeziBot.hpp"
#include "dezi/DeziBotPackage.hpp"

class DeziBotPackageProvider {
	
private:
    repast::SharedContext<DeziBot>* agents;
public:
    DeziBotPackageProvider(repast::SharedContext<DeziBot>* agentPtr);
    void providePackage(DeziBot * agent, std::vector<DeziBotPackage>& out);
    void provideContent(repast::AgentRequest req, std::vector<DeziBotPackage>& out);
};

#endif