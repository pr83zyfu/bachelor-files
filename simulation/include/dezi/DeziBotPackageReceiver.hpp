/* DeziBotPackageReceiver.hpp */

#ifndef DEZIBOTPACKAGERECEIVER
#define DEZIBOTPACKAGERECEIVER

#include <vector>

#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedContinuousSpace.h"
#include "repast_hpc/SharedDiscreteSpace.h"

#include "SimulationProperties.hpp"
#include "base/SimulationArena.hpp"
#include "dezi/DeziBot.hpp"
#include "dezi/DeziBotPackage.hpp"

class DeziBotPackageReceiver {
	
private:
    repast::SharedContext<DeziBot>* agents;
	SimulationArena* space;
    AgentProperties* agentProperties;
	
public:
    DeziBotPackageReceiver(repast::SharedContext<DeziBot>* agentPtr, 
		SimulationArena* _space,
        AgentProperties* _agentProperties
    );
	
    DeziBot * createAgent(DeziBotPackage package);
	
    void updateAgent(DeziBotPackage package);
	
};

#endif