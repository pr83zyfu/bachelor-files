/* DataSources.hpp */

#ifndef DATASOURCES
#define DATASOURCES

#include "repast_hpc/SharedContext.h"

#include "repast_hpc/TDataSource.h"
#include "repast_hpc/SVDataSet.h"
#include "repast_hpc/AgentId.h"

#include "dezi/DeziBot.hpp"
#include "resource/Resource.hpp"
#include "log/SEIRDataCollector.hpp"
#include "pheromone/IPheromoneMap.hpp"

/**
 * 
 * 	Data Source Templates
 * 
*/

template <typename AgentType, typename ValueType>
class DataSource_Agent : public repast::TDataSource<ValueType>{
	protected:
		repast::SharedContext<AgentType>* context;
    	repast::AgentId* id;
		virtual ValueType getValue(AgentType* bot) = 0;
		virtual ValueType zero() = 0;
	public:
		ValueType getData();
};

/**
 * 
 *	DeziBot Data Sources 
 * 
*/

class DataSource_DeziBot_X : public DataSource_Agent<DeziBot, double>{
	protected:
		virtual double getValue(DeziBot* bot);
		virtual double zero(){return 0.0;}
	public:
		DataSource_DeziBot_X(repast::SharedContext<DeziBot>* c, repast::AgentId* _id);
};


class DataSource_DeziBot_Y : public DataSource_Agent<DeziBot, double>{
	protected:
		virtual double getValue(DeziBot* bot);
		virtual double zero(){return 0.0;}
	public:
		DataSource_DeziBot_Y(repast::SharedContext<DeziBot>* c, repast::AgentId* _id);
};

class DataSource_DeziBot_InfectionState : public DataSource_Agent<DeziBot, int>{
	protected:
		virtual int getValue(DeziBot* bot);
		virtual int zero(){return 0;}
	public:
		DataSource_DeziBot_InfectionState(repast::SharedContext<DeziBot>* c, repast::AgentId* _id);
};

class DataSource_DeziBot_ResourceSearchState : public DataSource_Agent<DeziBot, int> {
	protected:
		virtual int getValue(DeziBot* bot);
		virtual int zero(){return 0;}
	public:
		DataSource_DeziBot_ResourceSearchState(repast::SharedContext<DeziBot>* c, repast::AgentId* _id);
};

/**
 * 
 *	Infection Simulation Data Sources 
 * 
*/

class DataSource_InfectionState_Sum : public repast::TDataSource<int>{
	private:
		SEIRDataCollector* collector;
		SEIRState state;
	public:
		DataSource_InfectionState_Sum(SEIRDataCollector* _collector, SEIRState _state);
		int getData();
};

/**
 * 
 *	Resource Data Sources 
 * 
*/

class DataSource_Resource_X : public DataSource_Agent<Resource, double>{
	protected:
		virtual double getValue(Resource* resource);
		virtual double zero(){return 0.0;}
	public:
		DataSource_Resource_X(repast::SharedContext<Resource>* c, repast::AgentId* _id);
};

class DataSource_Resource_Y : public DataSource_Agent<Resource, double>{
	protected:
		virtual double getValue(Resource* resource);
		virtual double zero(){return 0.0;}
	public:
		DataSource_Resource_Y(repast::SharedContext<Resource>* c, repast::AgentId* _id);
};

class DataSource_Resource_Size : public DataSource_Agent<Resource, int>{
	protected:
		virtual int getValue(Resource* resource);
		virtual int zero(){return 0;}
	public:
		DataSource_Resource_Size(repast::SharedContext<Resource>* c, repast::AgentId* _id);
};

class DataSource_ResourceSum : public repast::TDataSource<int>{
	private:
		ResourceSpace* resourceSpace;
	public:
		DataSource_ResourceSum(ResourceSpace* _resourceSpace);
		int getData();
};

/**
 * 
 *	Pheromone Data Sources 
 * 
*/

class DataSource_PheromoneCell : public repast::TDataSource<double>{
	private:
		int x;
		int y;
		IPheromoneMap* map;
		repast::ScheduleRunner* runner;
	public:
		DataSource_PheromoneCell(IPheromoneMap* _map, int _x, int _y, repast::ScheduleRunner* _runner);
		double getData();
};

#endif