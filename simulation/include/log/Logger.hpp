/* Logger.hpp */

#ifndef LOGGER
#define LOGGER

#include <vector>

#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SVDataSet.h"

#include "dezi/DeziBot.hpp"
#include "pheromone/IPheromoneMap.hpp"
#include "log/SEIRDataCollector.hpp"

template< class T = void >
struct MaxStruct{
    constexpr T operator()( const T& lhs, const T& rhs ) const {return std::max<T>(lhs, rhs);}
};

class Logger {
    protected:
    public:
        Logger(){}
        virtual ~Logger() {};
        virtual void record() = 0;
        virtual void write() = 0;
};

class AgentStateLogger : public Logger {
    private:
        repast::SharedContext<DeziBot>* context;
        std::vector<repast::SVDataSet*> dataSets;
    public:
        AgentStateLogger(repast::SharedContext<DeziBot>* _context, std::string outputDirectory, std::string outputFilePrefix, int totalAgents, int rank);
        ~AgentStateLogger();
        virtual void record();
        virtual void write();
};

class CondensedAgentStateLogger : public Logger {
    private:
        repast::SharedContext<DeziBot>* context;
        repast::SVDataSet* dataSet;
    public:
        CondensedAgentStateLogger(repast::SharedContext<DeziBot>* _context, std::string outputDirectory, std::string outputFile, int totalAgents, int rank);
        ~CondensedAgentStateLogger();
        virtual void record();
        virtual void write();
};

class InfectionSumLogger : public Logger {
    private:
        repast::SharedContext<DeziBot>* context;
        SEIRDataCollector* collector;
        repast::SVDataSet* dataSet;
    public:
        InfectionSumLogger(repast::SharedContext<DeziBot>* _context, std::string outputDirectory, std::string outputFile);
        ~InfectionSumLogger();
        virtual void record();
        virtual void write();
};

class CondensedResourceStateLogger : public Logger {
    private:
        repast::SharedContext<Resource>* context;
        repast::SVDataSet* dataSet;
    public:
        CondensedResourceStateLogger(repast::SharedContext<Resource>* _context, std::string outputDirectory, std::string outputFile, int totalResources, int rank);
        ~CondensedResourceStateLogger();
        virtual void record();
        virtual void write();
};

class ResourceCollectionLogger : public Logger {
    private:
        repast::SVDataSet* dataSet;
        ResourceSpace* resourceSpace;
    public:
        ResourceCollectionLogger(
            ResourceSpace* _resourceSpace,
            std::string outputDirectory,
            std::string outputFile
        );
        ~ResourceCollectionLogger();
        virtual void record();
        virtual void write();
};

class PheromoneLogger : public Logger {
    private: 
        repast::SVDataSet* dataSet;
    public:
        PheromoneLogger(
            IPheromoneMap* _pheromoneMap,
            repast::Point<double>* _origin,
            repast::Point<double>* _maxPoint,
            std::string outputDirectory,
            std::string outputFile,
            repast::ScheduleRunner* _runner
        );
        ~PheromoneLogger();
        virtual void record();
        virtual void write();
};

#endif