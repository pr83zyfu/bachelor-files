/* SEIRDataCollector.hpp */

#ifndef SEIRDATACOLLECTOR
#define SEIRDATACOLLECTOR

#include <vector>
#include <map>

#include "repast_hpc/SharedContext.h"

#include "dezi/DeziBot.hpp"

class SEIRDataCollector{
    private:
        const std::vector<SEIRState> states{SUSCEPTIBLE, EXPOSED, INFECTIOUS, RECOVERED};
        repast::SharedContext<DeziBot>* context;
        std::map<SEIRState, int> sums;
    public:
        SEIRDataCollector(repast::SharedContext<DeziBot>* _context);
        void update();
        int getSEIRStateSum(SEIRState state);
};

#endif