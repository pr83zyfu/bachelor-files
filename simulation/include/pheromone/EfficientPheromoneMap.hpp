/* EfficientPheromoneMap.hpp */

#ifndef EFFICIENTPHEROMONEMAP
#define EFFICIENTPHEROMONEMAP

#include <boost/mpi/communicator.hpp>

#include "repast_hpc/Point.h"

#include "pheromone/IPheromoneMap.hpp"
#include "repast_ext/SharedValueField.hpp"
#include "base/Vector2D.hpp"

class EfficientPheromoneMap : public IPheromoneMap {
    private:
    double pheromoneLifetime;
    boost::mpi::communicator* comm;
    SharedValueField<double>* intensities;
    SharedValueField<double>* timeStamps;
    repast::Point<double>* origin;
    repast::Point<double>* extent;
    repast::Point<double>* maxPoint;

    /* Private Helper Mathods */
    int makeDiscrete(double continuousValue);
    int offsetXValue(int x);
    int offsetYValue(int y);
    int removeXOffset(int x);
    int removeYOffset(int y);

    public:
    EfficientPheromoneMap(
        repast::Point<double>* _origin,
        repast::Point<double>* _extent,
        repast::Point<double>* _maxPoint,
        double _pheromoneLifetime,
        boost::mpi::communicator* _comm
    );
    virtual ~EfficientPheromoneMap() noexcept;

    virtual double getIntensity(int x, int y, double currentTick);
    virtual double getIntensity(const repast::Point<int>& point, double currentTick);
    virtual double getIntensity(const repast::Point<double>& point, double currentTick);

    virtual void drawPoint(int x, int y, double intensity, double currentTick);
    virtual void drawPoint(const repast::Point<int>& point, double intensity, double currentTick);
    virtual void drawPoint(const repast::Point<double>& point, double intensity, double currentTick);

    virtual void drawCircle(int x, int y, double radius, double intensity, double currentTick);
    virtual void drawCircle(const repast::Point<int>& center, double radius, double intensity, double currentTick);
    virtual void drawCircle(const repast::Point<double>& center, double radius, double intensity, double currentTick);

    virtual void drawRectangleInFrontOfPointWithDirection(int x, int y, Vector2D direction, int width, int height, double intensity, double currentTick);
    virtual void drawRectangleInFrontOfPointWithDirection(const repast::Point<double>& point, Vector2D direction, int width, int height, double intensity, double currentTick);

    virtual void drawRectangleBehindPointWithDirection(const repast::Point<double>& point, Vector2D direction, int width, int height, double intensity, double currentTick);

    virtual void synchronize();
};

#endif