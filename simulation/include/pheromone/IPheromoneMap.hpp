/* IPheromoneMap.hpp */

#ifndef IPHEROMONEMAP
#define IPHEROMONEMAP

#include "repast_hpc/Point.h"
#include "base/Vector2D.hpp"

class IPheromoneMap {
    public:
    virtual ~IPheromoneMap() = 0;

    virtual double getIntensity(int x, int y, double currentTick) = 0;
    virtual double getIntensity(const repast::Point<int>& point, double currentTick) = 0;
    virtual double getIntensity(const repast::Point<double>& point, double currentTick) = 0;

    virtual void drawPoint(int x, int y, double intensity, double currentTick) = 0;
    virtual void drawPoint(const repast::Point<int>& point, double intensity, double currentTick) = 0;
    virtual void drawPoint(const repast::Point<double>& point, double intensity, double currentTick) = 0;

    virtual void drawCircle(int x, int y, double radius, double intensity, double currentTick) = 0;
    virtual void drawCircle(const repast::Point<int>& center, double radius, double intensity, double currentTick) = 0;
    virtual void drawCircle(const repast::Point<double>& center, double radius, double intensity, double currentTick) = 0;

    virtual void drawRectangleInFrontOfPointWithDirection(int x, int y, Vector2D direction, int width, int height, double intensity, double currentTick) = 0;
    virtual void drawRectangleInFrontOfPointWithDirection(const repast::Point<double>& point, Vector2D direction, int width, int height, double intensity, double currentTick) = 0;

    virtual void drawRectangleBehindPointWithDirection(const repast::Point<double>& point, Vector2D direction, int width, int height, double intensity, double currentTick) = 0;


    virtual void synchronize() = 0;
};

#endif