/* Pheromone.hpp */

#ifndef PHEROMONE
#define PHEROMONE

#include "repast_hpc/AgentId.h"

class Pheromone {
    private:
        repast::AgentId id;
        double intensity;
        double timeStamp;
    public:
        Pheromone(repast::AgentId _id, double _intensity, double _timeStamp);
        ~Pheromone();

        /* Required Getters */
        virtual repast::AgentId& getId(){                   return id;    }
        virtual const repast::AgentId& getId() const {      return id;    }

        double getIntensity(){return intensity;}
        double getTimeStamp(){return timeStamp;}

        /* Setter */
        void setCurrentRank(int currentRank){id.currentRank(currentRank);}

        void updateAttributes(double _intensity, double _timeStamp);
};

#endif