/* PheromoneMap.hpp */

#ifndef PHEROMONEMAP
#define PHEROMONEMAP

#include "repast_hpc/Point.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"

#include "pheromone/IPheromoneMap.hpp"
#include "pheromone/Pheromone.hpp"
#include "pheromone/PheromonePackageProvider.hpp"
#include "pheromone/PheromonePackageReceiver.hpp"

class PheromoneMap : public IPheromoneMap {
    private:
        int rank;
        int idCounter;
        double pheromoneLifetime;
        repast::SharedContext<Pheromone>* context;
        repast::SharedDiscreteSpace<Pheromone, repast::StrictBorders, repast::SimpleAdder<Pheromone>>* space;
        repast::Point<double>* origin;
        repast::Point<double>* extent;
        repast::Point<double>* maxPoint;
        PheromonePackageProvider* provider;
        PheromonePackageReceiver* receiver;
    public:
        PheromoneMap(
            int _rank,
            boost::mpi::communicator* _comm,
            repast::Point<double>* _origin,
            repast::Point<double>* _extent,
            repast::Point<double>* _maxPoint,
            double _minimalBuffer,
            std::string _spaceName,
            double _pheromoneLifetime
        );
        virtual ~PheromoneMap();

        virtual double getIntensity(int x, int y, double currentTick);
        virtual double getIntensity(const repast::Point<int> &point, double currentTick);
        virtual double getIntensity(const repast::Point<double> &point, double currentTick);

        virtual void drawPoint(int x, int y, double intensity, double currentTick);
        virtual void drawPoint(const repast::Point<int> &point, double intensity, double currentTick);
        virtual void drawPoint(const repast::Point<double> &point, double intensity, double currentTick);

        virtual void drawCircle(int x, int y, double radius, double intensity, double currentTick);
        virtual void drawCircle(const repast::Point<int> &center, double radius, double intensity, double currentTick);
        virtual void drawCircle(const repast::Point<double> &center, double radius, double intensity, double currentTick);

        virtual void synchronize();
};

#endif