/* PheromonePackage.hpp */

#ifndef PHEROMONEPACKAGE
#define PHEROMONEPACKAGE

/* Serializable Pheromone Package */
struct PheromonePackage {
    public:
        int id;
        int rank;
        int type;
        int currentRank;
        double intensity;
        double timeStamp;
        PheromonePackage(); // For Serialization
        PheromonePackage(
            int _id,
            int _rank,
            int _type,
            int _currentRank,
            double _intensity,
            double _timeStamp
        );
    
    /* For archive packaging */
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar & id;
        ar & rank;
        ar & type;
        ar & currentRank;
        ar & intensity;
        ar & timeStamp;
    }
};

#endif