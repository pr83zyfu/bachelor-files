/* PheromonePackageProvider.hpp */

#ifndef PHEROMONEPACKAGEPROVIDER
#define PHEROMONEPACKAGEPROVIDER

#include "repast_hpc/SharedContext.h"

#include "pheromone/Pheromone.hpp"
#include "pheromone/PheromonePackage.hpp"

class PheromonePackageProvider {
    private:
        repast::SharedContext<Pheromone>* context;
    public:
        PheromonePackageProvider(repast::SharedContext<Pheromone>* _context);
        void providePackage(Pheromone* pheromone, std::vector<PheromonePackage> &out);
        void provideContent(repast::AgentRequest req, std::vector<PheromonePackage> &out);
};

#endif