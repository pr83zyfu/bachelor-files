/* PheromonePackageReceiver.hpp */

#ifndef PHEROMONEPACKAGERECEIVER
#define PHEROMONEPACKAGERECEIVER

#include "repast_hpc/SharedContext.h"

#include "pheromone/Pheromone.hpp"
#include "pheromone/PheromonePackage.hpp"

class PheromonePackageReceiver {
    private:
        repast::SharedContext<Pheromone>* context;
    public:
        PheromonePackageReceiver(repast::SharedContext<Pheromone>* _context);
        Pheromone* createAgent(PheromonePackage package);
        void updateAgent(PheromonePackage package);
};

#endif