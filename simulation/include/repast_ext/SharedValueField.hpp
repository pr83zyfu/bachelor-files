/* SharedValueField.hpp */

#ifndef SHAREDVALUEFIELD
#define SHAREDVALUEFIELD

#include "repast_ext/SharedValueVector.hpp"

template <typename Value>
class SharedValueField {
    private:
        int width;
        int height;
        SharedValueVector<Value>* vector;
        int getIndex(int x, int y);
    public:
        SharedValueField(int _width, int _height, boost::mpi::communicator* _comm);
        ~SharedValueField();

        /* Getter */
        int getWidth(){return width;}
        int getHeight(){return height;}
        
        void initializeValueChange(int x, int y, Value value);
        Value getValue(int x, int y);
        void update();
};

#endif