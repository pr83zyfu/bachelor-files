/* SharedValueVector.hpp */

#ifndef SHAREDVALUEVECTOR
#define SHAREDVALUEVECTOR

#include <vector>

#include <boost/mpi.hpp>

template <typename Value>
struct ValueChangePackage {
    public:
        int index;
        Value value;
        ValueChangePackage(){} // For Serialization
        ValueChangePackage(int _index, Value _value){index = _index; value = _value;}

        /* For archive packaging */
        template <class Archive>
        void serialize(Archive &ar, const unsigned int version){
            ar & index;
            ar & value;
        }
};

template <typename Value>
class SharedValueVector {
    private:
        int size;
        boost::mpi::communicator* comm;
        std::vector<Value>* values;
        std::vector<ValueChangePackage<Value>>* changes;
    public:
        SharedValueVector(int _size, boost::mpi::communicator* _comm);
        ~SharedValueVector();

        /* Getters */
        int getSize(){return size;}

        void initializeValueChange(int index, Value value);
        Value getValue(int index);
        void update();
};

#endif