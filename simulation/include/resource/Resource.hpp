/* Resource.hpp */

#ifndef RESOURCE
#define RESOURCE

#include <boost/algorithm/clamp.hpp>

#include "repast_hpc/AgentId.h"
#include "resource/ResourceSpace.hpp"

class Resource {
    private:
    repast::AgentId id;
    ResourceSpace* space;
    int size;
    public:
    Resource(repast::AgentId _id, ResourceSpace* _space, int _size);

    /* Required Getters */
    virtual repast::AgentId& getId() { return id; }
    virtual const repast::AgentId& getId() const { return id; }

    /* Specific Getters */
    int getSize() { return size; }
    bool isEmpty() { return size <= 0; }
    repast::Point<double> getPosition();

    /* Setter */
    void setCurrentRank(int currentRank) { id.currentRank(currentRank); }
    void setSize(int _size) { size = _size; }
    inline void collect(int amount) {
        int collectedAmount = boost::algorithm::clamp(amount, 0, size);
        size -= collectedAmount;
        space->addCollectedResources(collectedAmount);
    }

    void updateAttributes(int _size);
};

#endif