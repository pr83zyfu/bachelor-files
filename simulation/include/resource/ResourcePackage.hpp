/* ResourcePackage.hpp */

#ifndef RESOURCEPACKAGE
#define RESOURCEPACKAGE

/* Serializable Resource package */
struct ResourcePackage {
    public:
        int id;
        int rank;
        int type;
        int currentRank;
        int size;
        ResourcePackage(); // For serialization
        ResourcePackage(
            int _id, 
            int _rank, 
            int _type, 
            int _currentRank,
            int _size
);

    /* For archive packaging */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar & id;
        ar & rank;
        ar & type;
        ar & currentRank;
        ar & size;
    }
};

#endif