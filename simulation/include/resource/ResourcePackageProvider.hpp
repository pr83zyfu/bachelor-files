/* ResourcePackageProvider.hpp */

#ifndef RESOURCEPACKAGEPROVIDER
#define RESOURCEPACKAGEPROVIDER

#include "repast_hpc/SharedContext.h"

#include "resource/Resource.hpp"
#include "resource/ResourcePackage.hpp"

class ResourcePackageProvider {
    private:
        repast::SharedContext<Resource>* context;
    public:
        ResourcePackageProvider(repast::SharedContext<Resource>* _context);
        void providePackage(Resource * resource, std::vector<ResourcePackage>& out);
        void provideContent(repast::AgentRequest req, std::vector<ResourcePackage>& out);
};

#endif