/* ResourcePackageReceiver.hpp */

#ifndef RESOURCEPACKAGERECEIVER
#define RESOURCEPACKAGERECEIVER

#include "resource/ResourcePackage.hpp"

class ResourcePackageReceiver{

private:
    repast::SharedContext<Resource> *context;
    ResourceSpace* space;
public:
    ResourcePackageReceiver(repast::SharedContext<Resource> *_context, ResourceSpace* _space);
    Resource *createAgent(ResourcePackage package);
    void updateAgent(ResourcePackage package);
};

#endif