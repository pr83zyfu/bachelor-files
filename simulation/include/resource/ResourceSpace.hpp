/* ResourceSpace.hpp */

#ifndef RESOURCESPACE
#define RESOURCESPACE

#include <vector>

#include "repast_hpc/Point.h"
#include "repast_hpc/SharedContinuousSpace.h"
#include "repast_hpc/SharedContext.h"

#include "SimulationProperties.hpp"
#include "dezi/DeziBot.hpp"

/* Forward Declarations to avoid cyclic includes */
class Resource;
class ResourcePackageProvider;
class ResourcePackageReceiver;

class ResourceSpace {
    private:
        int collectedResourceTotal;
        repast::SharedContext<Resource>* context;
        ResourcePackageProvider* provider;
	    ResourcePackageReceiver* receiver;
        repast::SharedContinuousSpace<Resource, repast::StrictBorders, repast::SimpleAdder<Resource>>* space;
        repast::Point<double>* origin;
        repast::Point<double>* extent;
        repast::Point<double>* maxPoint;
        int rank;
    public:
        ResourceSpace(
            repast::SharedContext<Resource>* _context, 
            boost::mpi::communicator* _comm, 
            repast::Point<double>* _origin, 
            repast::Point<double>* _extent, 
            repast::Point<double>* _maxPoint, 
            double minimalBuffer,
            int _rank);
        ~ResourceSpace();

        /* Getters */
        repast::Point<double> getLocation(repast::AgentId id);
        int getCollectedResourcesTotal(){return collectedResourceTotal;}

        /* Setters */
        void addCollectedResources(int amount){collectedResourceTotal += amount;}

        void constructNewResource(repast::AgentId id, int size, repast::Point<double> &position);
        void constructRandomResource(repast::AgentId id, int minSize, int maxSize);
        void remove(repast::AgentId &id);
        void synchronize();
        void moveTo(repast::AgentId id, double x, double y);
        void moveTo(repast::AgentId id, repast::Point<double> &position);
        void moveToRandomLocation(repast::AgentId id);
};

#endif