#! /usr/bin/bash

if [ $# -ne 1 ]
	then echo "Incorrect number of Arguments. Use: \"./run.sh [random seed]\""
	exit 1
fi

# Constants
OUTPUT_DIRECTORY=./output

random_seed=$1

# Clean
./clean_output.sh $OUTPUT_DIRECTORY

mpirun -n 4 ./bin/Simulation.exe ./props/config.props ./props/model.props log.output.directory=$OUTPUT_DIRECTORY random.seed=$random_seed
