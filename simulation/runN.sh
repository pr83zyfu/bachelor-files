#! /usr/bin/bash

# Constants
DEFAULT_OUTPUT_DIRECTORY=./output

if [ $# -eq 2 ] 
then
	output_directory=$DEFAULT_OUTPUT_DIRECTORY
	run_type_string=
elif [ $# -eq 3 ] 
then
	output_directory=$3
	run_type_string=
elif [ $# -eq 4 ]
then
	output_directory=$3
	run_type_string=agent.run.type=$4
else
	echo "Incorrect number of Arguments. Use: \"./runN.sh [number of runs] [random first seed]\""
	echo "or \"./runN.sh [number of runs] [random first seed] [output directory (this directory will be completely cleaned!!)]\""
	echo "or \"./runN.sh [number of runs] [random first seed] [output directory (this directory will be completely cleaned!!)] [run type (override)]\""
	exit 1
fi

# Load parameters
number_of_runs=$1
first_seed=$2
last_seed=$((first_seed + number_of_runs - 1))

# Clean
./clean_output.sh $output_directory

echo "================================================================================"
echo "RUNNING $number_of_runs times, starting with $first_seed, ending with $last_seed"
echo "================================================================================"


for ((i = first_seed; i <= last_seed; i++)); 
do 
	run_directory=$output_directory/run_$i;
	mkdir $run_directory;
	mpirun -n 4 ./bin/Simulation.exe ./props/config.props ./props/model.props log.output.directory=$run_directory random.seed=$i $run_type_string;
done