import os
import re
import pandas as pd

import sys

RUN_DIRECTORY_REGEX: str = "run_[0-9]+"
INFECTION_SUMS_FILE_NAME: str = "infection_sums.csv"
RESOURCE_SUMS_FILE_NAME: str = "resource_sum.csv"
INPUT_DIRECTORY_DEFAULT: str = "./output"
OUTPUT_DIRECTORY_DEFAULT: str = "./output/avg"


def get_files_of_runs(directory: str, file_name: str) -> list[str]:
    result = list()

    with os.scandir(directory) as files_in_input:
        for run_dir in files_in_input:
            if run_dir.is_dir and re.match(RUN_DIRECTORY_REGEX, run_dir.name):
                with os.scandir(run_dir.path) as files_in_run_dir:
                    for file in files_in_run_dir:
                        if file.is_file and re.match(file_name, file.name):
                            result.append(file.path)

    return result


def average_seir(input_directory: str, output_directory: str):
    result: dict[int, dict[str, int]] = dict()

    infection_sum_files: list[str] = get_files_of_runs(
        input_directory, INFECTION_SUMS_FILE_NAME
    )
    total: int = len(infection_sum_files)

    # sum all runs
    for file_path in infection_sum_files:
        with open(file_path) as file:
            df = pd.read_csv(file, index_col="tick")

            for state in df.keys():
                if state not in result:
                    result[state] = dict()

                for tick in df[state].keys():
                    if tick not in result[state]:
                        result[state][tick] = 0

                    result[state][tick] += df[state][tick]

    # average
    for state in result.keys():
        for tick in result[state].keys():
            result[state][tick] /= total

    infection_df = pd.DataFrame.from_dict(result)

    infection_df.to_csv(
        output_directory + "/" + INFECTION_SUMS_FILE_NAME,
        index=True,
        index_label="tick",
        header=True,
    )
    print(infection_df)


def average_resources(input_directory: str, output_directory: str):
    result: dict[int, dict[str, int]] = dict()

    resource_sum_files: list[str] = get_files_of_runs(
        input_directory, RESOURCE_SUMS_FILE_NAME
    )
    total: int = len(resource_sum_files)

    # sum all runs
    for file_path in resource_sum_files:
        with open(file_path) as file:
            df = pd.read_csv(file, index_col="tick")

            for state in df.keys():
                if state not in result:
                    result[state] = dict()

                for tick in df[state].keys():
                    if tick not in result[state]:
                        result[state][tick] = 0

                    result[state][tick] += df[state][tick]

    # average
    for state in result.keys():
        for tick in result[state].keys():
            result[state][tick] /= total

    resource_df = pd.DataFrame.from_dict(result)

    resource_df.to_csv(
        output_directory + "/" + RESOURCE_SUMS_FILE_NAME,
        index=True,
        index_label="tick",
        header=True,
    )
    print(resource_df)


def main():
    command_arguments = sys.argv[1:]
    if len(command_arguments) == 0:
        input_directory = INPUT_DIRECTORY_DEFAULT
        output_directory = OUTPUT_DIRECTORY_DEFAULT
    elif len(command_arguments) == 1:
        input_directory = command_arguments[0]
        output_directory = input_directory + "/avg"
    elif len(command_arguments) == 2:
        input_directory = command_arguments[0]
        output_directory = command_arguments[1]

    average_seir(input_directory, output_directory)
    average_resources(input_directory, output_directory)


if __name__ == "__main__":
    main()
