import os
import re
import pandas as pd
import sys

RUN_DIRECTORY_REGEX: str = "run_[0-9]+"
INFECTION_SUMS_FILE_NAME: str = "infection_sums.csv"
RESOURCE_SUMS_FILE_NAME: str = "resource_sum.csv"
INPUT_DIRECTORY_DEFAULT: str = "./output"
OUTPUT_DIRECTORY_DEFAULT: str = INPUT_DIRECTORY_DEFAULT
OUTPUT_FILE: str = "distribution.csv"

INFECTIOUS_KEY = "infectious"
RESOURCE_SUMS_KEY = "total"


def get_ids_of_runs(directory: str) -> list[int]:
    result = list()

    with os.scandir(directory) as files_in_input:
        for run_dir in files_in_input:
            if run_dir.is_dir and re.match(RUN_DIRECTORY_REGEX, run_dir.name):
                id_string = run_dir.name.removeprefix("run_")
                id = int(id_string)
                result.append(id)

    return result


def get_files_of_runs(directory: str, file_name: str) -> dict[str]:
    result = dict()

    with os.scandir(directory) as files_in_input:
        for run_dir in files_in_input:
            if run_dir.is_dir and re.match(RUN_DIRECTORY_REGEX, run_dir.name):
                id_string = run_dir.name.removeprefix("run_")
                id = int(id_string)
                with os.scandir(run_dir.path) as files_in_run_dir:
                    for file in files_in_run_dir:
                        if file.is_file and re.match(file_name, file.name):
                            result[id] = file.path
    return result


def get_imax(infection_sums_file: str) -> int:
    result: int = 0

    with open(infection_sums_file) as file:
        df = pd.read_csv(file, index_col="tick")
        result = max([df[INFECTIOUS_KEY][tick] for tick in df[INFECTIOUS_KEY].keys()])

    return result


def get_rmax(resource_sums_file: str) -> int:
    result: int = 0

    with open(resource_sums_file) as file:
        df = pd.read_csv(file, index_col="tick")
        result = max(
            [df[RESOURCE_SUMS_KEY][tick] for tick in df[RESOURCE_SUMS_KEY].keys()]
        )

    return result


def main():
    command_arguments = sys.argv[1:]
    if len(command_arguments) == 0:
        input_directory = INPUT_DIRECTORY_DEFAULT
        output_directory = OUTPUT_DIRECTORY_DEFAULT
    elif len(command_arguments) == 1:
        input_directory = command_arguments[0]
        output_directory = input_directory
    elif len(command_arguments) == 2:
        input_directory = command_arguments[0]
        output_directory = command_arguments[1]

    ids = get_ids_of_runs(input_directory)
    ids.sort()
    infection_sum_files = get_files_of_runs(input_directory, INFECTION_SUMS_FILE_NAME)
    resource_sum_files = get_files_of_runs(input_directory, RESOURCE_SUMS_FILE_NAME)

    result = dict()
    result["imax"] = dict()
    result["rmax"] = dict()

    for id in ids:
        result["imax"][id] = get_imax(infection_sum_files[id])
        result["rmax"][id] = get_rmax(resource_sum_files[id])

    resource_df = pd.DataFrame.from_dict(result)

    resource_df.to_csv(
        output_directory + "/" + OUTPUT_FILE, index=True, index_label="run", header=True
    )


if __name__ == "__main__":
    main()
