/* Main.cpp */
#include <fstream>
#include <iostream>

#include <boost/mpi.hpp>
#include <boost/filesystem.hpp>
#include "repast_hpc/RepastProcess.h"

#include "SimulationProperties.hpp"
#include "ProcessModel.hpp"

int main(int argc, char** argv){
	// initialization
	std::string configFile = argv[1]; // The name of the configuration file is Arg 1
	std::string propsFile  = argv[2]; // The name of the properties file is Arg 2
	
	boost::mpi::environment env(argc, argv);
	boost::mpi::communicator world;

	// record start time (barrier sync to make sure all processes are started)
	world.barrier();
	boost::mpi::timer simTime = boost::mpi::timer();

	int rank;
	{ /* Run Simulation */
		repast::RepastProcess::init(configFile);
		rank = repast::RepastProcess::instance()->rank();

		SimulationProperties* simProps = new SimulationProperties(propsFile, argc, argv, &world);
		simProps->checkLogicalConsistency();

		if (rank== 0) {
			std::cout << "Random Seed given: " << simProps->technical->randomSeed << std::endl;

			// copy props files
			{
				using namespace boost::filesystem;
				copy_file(configFile, simProps->log->outputDirectory + "/config.props", copy_option::overwrite_if_exists);
				copy_file(propsFile, simProps->log->outputDirectory + "/model.props", copy_option::overwrite_if_exists);
			}
		}
	
		repast::ScheduleRunner& runner = repast::RepastProcess::instance()->getScheduleRunner();
		ProcessModel* model = new ProcessModel(simProps, &runner);

		// simulation setup
		model->setup();
		model->setupSchedule(runner);

		// simulation execution
		runner.run();

		// clean up
		delete model;
		delete simProps;
	}

	// record elapsed time (barrier sync to make sure all processes are started)
	world.barrier();
	double elapsed = simTime.elapsed();

	repast::RepastProcess::instance()->done();

	if (rank == 0) {
		std::cout << "Finished in: " << (int) elapsed << " seconds." <<std::endl;
	}
}