/* ProcessModel.cpp */

#include <stdio.h>
#include <vector>
#include <math.h>

#include <boost/mpi.hpp>
#include <boost/iterator.hpp>

#include "repast_hpc/SharedContext.h"
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/SVDataSetBuilder.h"
#include "repast_hpc/Point.h"

#include "repast_hpc/SVDataSetBuilder.h"

#include "SimulationProperties.hpp"
#include "base/SimulationArena.hpp"
#include "dezi/DeziBot.hpp"
#include "dezi/DeziBotPackage.hpp"
#include "resource/ResourceSpace.hpp"
#include "log/Logger.hpp"

#include "ProcessModel.hpp"

void ProcessModel::createAgents(){
	// Add DeziBots
	{
		int i = 0;

		// Add susceptible agents
		int currentSum = properties->general->initialAgentsSusceptible;
		while (i < currentSum){
			arena->constructNewAgent(rank, i, SUSCEPTIBLE);
			i++;
		}

		// Add exposed agents
		currentSum += properties->general->initialAgentsExposed;
		while (i < currentSum){
			arena->constructNewAgent(rank, i, EXPOSED);
			i++;
		}

		// Add infectious agents
		currentSum += properties->general->initialAgentsInfectious;
		while (i < currentSum){
			arena->constructNewAgent(rank, i, INFECTIOUS);
			i++;
		}

		// Add recovered agents
		currentSum += properties->general->initialAgentsRecovered;
		while (i < currentSum){
			arena->constructNewAgent(rank, i, RECOVERED);
			i++;
		}
	}

	// Add Resources
	{
        for (size_t i = 0; i < properties->resourceGenerator->count; i++){
            repast::AgentId id(i, rank, 1, rank);
            arena->getResourceSpace()->constructRandomResource(id, properties->resourceGenerator->size, properties->resourceGenerator->size);
    	}
	}
}

void ProcessModel::createLoggers(){
	// Data collection. 
	// Agent State log creation. Expects all agents to be initialized on process 0 and identified just by their id (see setup)
	if (properties->log->recordAgentState){
		loggers.push_back(new AgentStateLogger(
			arena->getAgentContext(), 
			properties->log->outputDirectory, 
			"agent_",
			properties->general->initialAgentsTotal, 
			rank
			)
		);
	}

	if (properties->log->recordAgentStateCondensed){
		loggers.push_back(new CondensedAgentStateLogger(
			arena->getAgentContext(), 
			properties->log->outputDirectory, 
			"agents_condensed.csv",
			properties->general->initialAgentsTotal, 
			rank
			)
		);
	}

	// Infection sum log creation
	if (properties->log->recordInfectionSums){
		loggers.push_back(new InfectionSumLogger(
			arena->getAgentContext(), 
			properties->log->outputDirectory,
			"infection_sums.csv"
			)
		);
	}

	if (properties->log->recordResourceStateCondensed){
		loggers.push_back(new CondensedResourceStateLogger(
			arena->getResourceContext(), 
			properties->log->outputDirectory, 
			"resources_condensed.csv",
			properties->resourceGenerator->count,
			rank
			)
		);
	}

	if (properties->log->recordResourceSum) {
		loggers.push_back(new ResourceCollectionLogger(
			arena->getResourceSpace(), 
			properties->log->outputDirectory, 
			"resource_sum.csv"
			)
		);
	}

	// Pheromone resourceIndicator
	if (properties->log->recordResourceIndicatorPheromone){
		loggers.push_back(new PheromoneLogger(
			arena->getResourceIndicatorPheromone(),
			arena->getOrigin(),
			arena->getMaxPoint(),
			properties->log->outputDirectory,
			"resource_indicator_pheromone.csv",
			scheduleRunner
			)
		);
	}

	// Pheromone warning
	if (properties->log->recordWarningPheromone){
		loggers.push_back(new PheromoneLogger(
			arena->getWarninigPheromone(), 
			arena->getOrigin(), 
			arena->getMaxPoint(), 
			properties->log->outputDirectory,
			"warning_pheromone.csv",
			scheduleRunner
			)
		);
	}
}

void ProcessModel::setupSchedule(repast::ScheduleRunner& runner){
	scheduleRunner = &runner;
	runner.scheduleEvent(1, 1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<ProcessModel> (this, &ProcessModel::update)));
	runner.scheduleStop(properties->general->timeMax);

	// Data collection
	for (int i = 0; i < loggers.size(); i++) {
		runner.scheduleEvent(0, 1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Logger>(loggers[i], &Logger::record)));
		runner.scheduleEndEvent(repast::Schedule::FunctorPtr(new repast::MethodFunctor<Logger>(loggers[i], &Logger::write)));
	}
}

ProcessModel::ProcessModel(SimulationProperties* simProps, repast::ScheduleRunner* _runner){
	properties = simProps;
	scheduleRunner = _runner;
	rank = repast::RepastProcess::instance()->rank();
	
	arena = new SimulationArena(properties, rank);
}

ProcessModel::~ProcessModel(){
	delete arena;
	for(auto it = std::begin(loggers); it != std::end(loggers); ++it) {
		delete *it;
	}
}

void ProcessModel::setup(){
	// only rank 0 adds agents and resources. 
	if (rank == 0) {
		createAgents();
	}
	createLoggers();
}

void ProcessModel::update(){
	double currentTick = scheduleRunner->currentTick();

	arena->update(currentTick);
	
	if (rank == 0) {
		std::cout << "TICK: " << currentTick << " / " << properties->general->timeMax << "\r";
	}
}