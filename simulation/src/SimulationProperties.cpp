/* SimulationProperties.cpp */

#include "repast_hpc/Utilities.h"
#include "repast_hpc/initialize_random.h"

#include "SimulationProperties.hpp"

SimulationProperties::SimulationProperties() {

}

SimulationProperties::SimulationProperties(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm) {
	communicator = comm;

	// initialize properties
	technical = new TechnicalProperties();
	general = new GeneralProperties();
	arena = new ArenaProperties();
	agent = new AgentProperties();
	resourceGenerator = new ResourceGeneratorProperties();
	pheromone = new PheromoneCollectionProperties();
	pheromone->resourceIndicator = new PheromoneProperties();
	pheromone->warning = new PheromoneProperties();
	log = new LogProperties();

	// initialize props reference
	props = new repast::Properties(propsFile, argc, argv, comm);

	// Load all properties
	technical->randomSeed = intFromProp("random.seed");

	general->timeMax = intFromProp("time.max");
	general->initialAgentsSusceptible = intFromProp("initial.agents.susceptible");
	general->initialAgentsExposed = intFromProp("initial.agents.exposed");
	general->initialAgentsInfectious = intFromProp("initial.agents.infectious");
	general->initialAgentsRecovered = intFromProp("initial.agents.recovered");
	general->initialAgentsTotal =
		general->initialAgentsSusceptible + general->initialAgentsExposed + general->initialAgentsInfectious + general->initialAgentsRecovered;

	arena->width = doubleFromProp("arena.width");
	arena->height = doubleFromProp("arena.height");

	agent->speed = doubleFromProp("agent.move.speed");
	agent->randomDontChangeDirectionProbability = doubleFromProp("agent.move.random.dontChangeDirectionProbability");
	agent->randomRotationDelta = doubleFromProp("agent.move.random.rotationDelta");

	agent->transmissionProbability = doubleFromProp("agent.transmission.probability");
	agent->transmissionRadius = doubleFromProp("agent.transmission.radius");
	agent->exposedTime = intFromProp("agent.time.exposed");
	agent->infectiousTime = intFromProp("agent.time.infectious");
	agent->realizationTime = intFromProp("agent.time.realization");
	agent->infectedSpeed = doubleFromProp("agent.infection.symptoms.speed");

	agent->runType = RunType(intFromProp("agent.run.type"));

	agent->quarantaineZone = new repast::Point<double>(
		doubleFromProp("agent.reduction.strategy.quarantaine.zone.x"),
		doubleFromProp("agent.reduction.strategy.quarantaine.zone.y"));

	agent->resourceDetectionRadius = doubleFromProp("agent.resource.detection.radius");
	agent->resourceCollectionRadius = doubleFromProp("agent.resource.collection.radius");
	agent->resourceWaitAfterCollectionTime = intFromProp("agent.resource.wait.time.after.collection");

	agent->pheromoneDetectionToSides = intFromProp("agent.pheromone.detection.to.sides");
	agent->pheromoneDetectionToFront = intFromProp("agent.pheromone.detection.to.front");
	agent->pheromoneDrawRadius = doubleFromProp("agent.pheromone.draw.radius");
	agent->pheromoneDrawWidth = intFromProp("agent.pheromone.draw.width");
	agent->pheromoneDrawHeight = intFromProp("agent.pheromone.draw.height");
	agent->pheromoneResourceIndicatorUse = boolFromProp("agent.pheromone.resourceIndicator.use");
	agent->pheromoneResourceIndicatorDrawLength = doubleFromProp("agent.pheromone.resourceIndicator.draw.length");

	agent->phantomStartDetectionChance = doubleFromProp("agent.follow.phantom.pheromone.start");
	agent->phantomStopDetectionChance = doubleFromProp("agent.follow.phantom.pheromone.stop");

	resourceGenerator->count = intFromProp("resource.generator.count");
	resourceGenerator->size = intFromProp("resource.generator.size");

	pheromone->resourceIndicator->lifetime = doubleFromProp("pheromone.resourceIndicator.lifetime");
	pheromone->warning->lifetime = doubleFromProp("pheromone.warning.lifetime");

	log->recordAgentState = boolFromProp("log.agent.state");
	log->recordAgentStateCondensed = boolFromProp("log.agent.state.condensed");
	log->recordInfectionSums = boolFromProp("log.infection.sum");
	log->recordResourceStateCondensed = boolFromProp("log.resource.state.condensed");
	log->recordResourceSum = boolFromProp("log.resource.sum");
	log->recordResourceIndicatorPheromone = boolFromProp("log.pheromone.resourceIndicator");
	log->recordWarningPheromone = boolFromProp("log.pheromone.warning");
	log->outputDirectory = strFromProp("log.output.directory");

	// initializes the random generator used for all randomness in the simulation with the seed given in the properties file as random.seed 
	repast::Properties propReference = *props;
	initializeRandom(propReference, communicator);
}

SimulationProperties::~SimulationProperties() {
	delete technical;
	delete general;
	delete arena;
	delete agent;
	delete props;
	delete log;
}

void SimulationProperties::checkLogicalConsistency() {
	assert(general->timeMax >= 0);
	assert(general->initialAgentsSusceptible >= 0);
	assert(general->initialAgentsExposed >= 0);
	assert(general->initialAgentsInfectious >= 0);
	assert(general->initialAgentsRecovered >= 0);
	assert(general->initialAgentsTotal >= 1); // Simulation with 0 agents would make no sense

	assert(arena->width > 0.0);
	assert(arena->height > 0.0);

	assert(agent->speed >= 0.0);
	assert(agent->infectedSpeed >= 0.0);
	assert(agent->transmissionProbability >= 0.0 && agent->transmissionProbability <= 1.0);
	assert(agent->transmissionRadius >= 0.0);
	assert(agent->randomDontChangeDirectionProbability >= 0.0 && agent->randomDontChangeDirectionProbability <= 1.0);
	assert(agent->randomRotationDelta >= 0.0);
	assert(agent->exposedTime >= 0);
	assert(agent->infectiousTime >= 0);
	assert(agent->realizationTime >= 0);
	assert(agent->resourceDetectionRadius >= 0.0);
	assert(agent->resourceCollectionRadius >= 0.0);
	assert(agent->resourceWaitAfterCollectionTime >= 0);
	assert(agent->pheromoneResourceIndicatorDrawLength >= 0.0);
	assert(agent->phantomStartDetectionChance >= 0.0 && agent->phantomStartDetectionChance <= 1.0);
	assert(agent->phantomStopDetectionChance >= 0.0 && agent->phantomStopDetectionChance <= 1.0);
	assert(agent->pheromoneDrawRadius >= 0.0);
	assert(agent->pheromoneDrawWidth >= 0);
	assert(agent->pheromoneDrawHeight >= 0);

	assert(resourceGenerator->count >= 0);
	assert(resourceGenerator->size >= 0);

	assert(pheromone->resourceIndicator->lifetime >= 0.0);
	assert(pheromone->warning->lifetime >= 0.0);

	// logical checks for bools can be omitted as the only possible value are true/false or the program would already have crashed
	assert(!log->outputDirectory.empty());
}

int SimulationProperties::strToInt(const std::string& string) {
	return repast::strToInt(string);
}

double SimulationProperties::strToDouble(const std::string& string) {
	return repast::strToDouble(string);
}

bool SimulationProperties::strToBool(const std::string& string) {
	return strToInt(string) == 0 ? false : true;
}

int SimulationProperties::intFromProp(const std::string& key) {
	std::string value = props->getProperty(key);
	return strToInt(value);
}

double SimulationProperties::doubleFromProp(const std::string& key) {
	std::string value = props->getProperty(key);
	return strToDouble(value);
}

bool SimulationProperties::boolFromProp(const std::string& key) {
	std::string value = props->getProperty(key);
	return strToBool(value);
}

std::string SimulationProperties::strFromProp(const std::string& key) {
	return props->getProperty(key);
}