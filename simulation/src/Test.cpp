#include <vector>

#include <boost/mpi.hpp>
#include "repast_ext/SharedValueField.hpp"

template <typename Value>
void printSharedValueField(SharedValueField<Value> &field, int commSize, int commRank, boost::mpi::communicator &comm) {
    for (int i = 0; i < commSize; i++) {
        if (i == commRank) {
            std::cout << "RANK " << i << ": " << std::endl;
            for (int y = 0; y < field.getHeight(); y++) {
                for (int x = 0; x < field.getWidth(); x++) {
                    std::cout << field.getValue(x, y);
                    if (x < field.getWidth() - 1) {
                        std::cout << ", ";
                    }
                }
                std::cout << std::endl;
            }
            std::cout << std::endl;
        }
        comm.barrier();
    }
}

int main(int argc, char** argv){
    boost::mpi::environment env(argc, argv);
	boost::mpi::communicator world;
    int commRank = world.rank();
    int commSize = world.size();

    SharedValueField<int> field = SharedValueField<int>(3, 3, &world);

    switch (commRank) {
        case 0:
            field.initializeValueChange(0, 0, 24);
            field.initializeValueChange(2, 1, 13);
            break;
        case 1:
            field.initializeValueChange(1, 2, 5);
            break;
        case 2:
            break;
        case 3:
            field.initializeValueChange(0, 1, 89);
            field.initializeValueChange(1, 0, 74);
            field.initializeValueChange(2, 2, 54);
            break;
    }

    printSharedValueField<int>(field, commSize, commRank, world);
    field.update();
    printSharedValueField<int>(field, commSize, commRank, world);

    switch (commRank) {
        case 0:
            break;
        case 1:
            field.initializeValueChange(2, 2, 5);
            break;
        case 2:
            break;
        case 3:
            field.initializeValueChange(1, 0, 63);
            field.initializeValueChange(2, 2, 52);
            break;
    }

    field.update();
    printSharedValueField<int>(field, commSize, commRank, world);

    return 0;
}