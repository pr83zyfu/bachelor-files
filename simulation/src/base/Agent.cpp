/* Agent.cpp */

#include <vector>
#include <math.h>

#include <boost/algorithm/clamp.hpp>

#include "repast_hpc/AgentId.h"

#include "SimulationProperties.hpp"
#include "base/Agent.hpp"
#include "base/SimulationArena.hpp"
#include "base/AgentMoveSpace.hpp"

Agent::Agent(repast::AgentId _id,
    SimulationArena* _space,
    AgentProperties* agentProperties) {
    id = _id;
    space = _space;

    currentSpeed = agentProperties->speed;
    randomDontChangeDirectionProbability = agentProperties->randomDontChangeDirectionProbability;
    randomRotationDelta = agentProperties->randomRotationDelta;

    // Initialization of Default values
    // 0.0 rotation is vector (1, 0)
    velocity = Vector2D{ currentSpeed, 0.0 };
}

Agent::Agent(repast::AgentId _id,
    SimulationArena* _space,
    AgentProperties* agentProperties,
    double _speed,
    Vector2D _velocity) {
    id = _id;
    space = _space;

    currentSpeed = _speed;
    velocity = _velocity;
    randomDontChangeDirectionProbability = agentProperties->randomDontChangeDirectionProbability;
    randomRotationDelta = agentProperties->randomRotationDelta;
}

repast::Point<double> Agent::getPosition() {
    return space->getAgentSpace()->getLocation(id);
}

void Agent::set(int currentRank) {
    id.currentRank(currentRank);
}

void Agent::setSpeed(double newSpeed) {
    currentSpeed = newSpeed;
    velocity.scaleTo(currentSpeed);
}

void Agent::setVelocity(Vector2D newVelocity) {
    velocity = newVelocity;
    velocity.scaleTo(currentSpeed);
}

void Agent::randomWalk() {
    if (repast::Random::instance()->nextDouble() >= randomDontChangeDirectionProbability) {
        // rotates randomly between -ROTATION_DELTA and +ROTATION_DELTA
        rotate((repast::Random::instance()->nextDouble() - 0.5) * 2.0 * randomRotationDelta);
    }

    moveForward();
}

void Agent::rotate(double angle) {
    velocity.rotate(angle);
    velocity.scaleTo(currentSpeed);
}

void Agent::rotateTowards(double x, double y) {
    repast::Point<double> position = getPosition();
    velocity.setX(x - position.getX());
    velocity.setY(y - position.getY());
    velocity.scaleTo(currentSpeed);
}

void Agent::rotateTowards(repast::Point<double> point) {
    rotateTowards(point.getX(), point.getY());
}

void Agent::rotateTowards(repast::Point<int> point) {
    rotateTowards(point.getX(), point.getY());
}

void Agent::rotateToPointToFrontAndRight(double distanceToFront, double distanceToRight) {
    auto position = getPosition();
    Vector2D forward = Vector2D();
    Vector2D right = Vector2D();

    forward.setCopy(velocity);
    forward.scaleTo(distanceToFront);
    right.setCopy(forward);
    right.rotate(M_PI_2);
    right.scaleTo(distanceToRight);

    rotateTowards(
        position.getX() + forward.getX() + right.getX(),
        position.getY() + forward.getY() + right.getY()
    );
}

void Agent::turnAround() {
    velocity.scale(-1.0);
}

void Agent::moveTo(double x, double y) {
    space->getAgentSpace()->moveTo(id, x, y);
}

void Agent::moveForward() {
    /*
        For rotation 0.0 the robot moves along the vector (distance, 0)
    */

    // repast::Point<double> position = getPosition();
    // double x = position.getX();
    // double y = position.getY();

    // double deltaX = cos(rotation) * distance; 
    // double deltaY = sin(rotation) * distance;

    // moveTo(x + velocity.getX(), y + velocity.getY());
    velocity = *space->getAgentSpace()->moveForwardAndBounceOffWalls(id, &velocity);
    // rotation = space->moveForwardAndBounceOffWalls(id, rotation, speed);
}

void Agent::moveForwardWithoutCollision(double distance) {
    repast::Point<double> position = getPosition();
    double velocityLength = velocity.length();
    // move along velocity vector (first normalised then scaled to distance)
    moveTo(
        position.getX() + (distance * (velocity.getX() / velocityLength)),
        position.getY() + (distance * (velocity.getY() / velocityLength))
    );
}