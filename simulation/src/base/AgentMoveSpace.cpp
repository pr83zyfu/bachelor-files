/* AgentMoveSpace.cpp */

#include <boost/algorithm/clamp.hpp>

#include "base/Vector2D.hpp"
#include "base/AgentMoveSpace.hpp"
#include "base/SimulationArena.hpp"
#include "SimulationProperties.hpp"
#include "dezi/DeziBot.hpp"
#include "dezi/DeziBotPackageProvider.hpp"
#include "dezi/DeziBotPackageReceiver.hpp"

repast::Point<double>* AgentMoveSpace::getWallIntersection(WallDirection wall, Vector2D* position, Vector2D* velocity){
    // (inefficient) check for devision by 0
    if (velocity->getX() == 0.0) {
        return nullptr;
    }

    // line given by two points can be expressed as linear polynominal (x |--> lineSlope * x + lineConstant)
    double lineSlope = velocity->getY() / velocity->getX();
    double lineConstant = position->getY() - lineSlope * position->getX();

    // (inefficient) check for devision by 0
    if (lineSlope == 0.0) {
        return nullptr;
    }

    double x;
    double y;

    switch (wall){
        case EAST:
            x = maxPoint->getX();
            y = (lineSlope * x) + lineConstant;
            break;
        case SOUTH:
            y = origin->getY();
            x = (y - lineConstant) / lineSlope;
            //std::cout << x << " : " << lineSlope << std::endl;
            //std::cout << linePoint1.getX() << " | " << linePoint2.getX();
            break;
        case WEST:
            x = origin->getX();
            y = (lineSlope * x) + lineConstant;
            break;
        case NORTH:
            y = maxPoint->getY();
            x = (y - lineConstant) / lineSlope;
            break;
    }

    return new repast::Point<double>(x, y);
}

Vector2D* AgentMoveSpace::reflectVectorOffWall(WallDirection wall, Vector2D* vector){
    switch (wall) {
        case EAST:
            vector->setX(vector->getX() * -1.0);
            break;
        case SOUTH:
            vector->setY(vector->getY() * -1.0);
            break;
        case WEST:
            vector->setX(vector->getX() * -1.0);
            break;
        case NORTH:
            vector->setY(vector->getY() * -1.0);
            break;
    }

    return vector;
}

double AgentMoveSpace::calculateMoveTowardsWallIntersection(Vector2D* position, Vector2D* velocity, WallDirection wall, repast::Point<double>* wallIntersection, double remainingDistance){
    // move along velocity vector and check for collision with wall
    double wallIntersectionDistance = getDistance(position->toPoint(), *wallIntersection);

    if (wallIntersectionDistance >= remainingDistance) {
        // move along velocity vector scaled to remaining distance
        double scaleFactor = remainingDistance / velocity->length();
        position->setAdd(velocity->getX() * scaleFactor, velocity->getY() * scaleFactor);
        return 0.0;
    } else {
        // wall gets hit
        position->setCopy(wallIntersection);
        // rotate velocity
        reflectVectorOffWall(wall, velocity);
        //velocity->setX(newVelocity->getX());
        //velocity->setY(newVelocity->getY());
        //velocity->setCopy
        return remainingDistance - wallIntersectionDistance;
    }
}

AgentMoveSpace::AgentMoveSpace(
            repast::SharedContext<DeziBot>* _context, 
            boost::mpi::communicator* _comm, 
            repast::Point<double>* _origin, 
            repast::Point<double>* _extent, 
            repast::Point<double>* _maxPoint, 
            double minimalBuffer,
            SimulationArena* _arena,
            AgentProperties* _agentProperties){
    context = _context;
    origin = _origin;
    extent = _extent;
    maxPoint = _maxPoint;

    repast::GridDimensions gd(*origin, *extent);
    std::vector<int> processDims;
    processDims.push_back(2);
    processDims.push_back(2);

    space = new repast::SharedContinuousSpace<DeziBot, repast::StrictBorders, repast::SimpleAdder<DeziBot>>(
        "AgentSpace", 
        gd, 
        processDims, 
        minimalBuffer, 
        _comm
    );

    context->addProjection(space);

    provider = new DeziBotPackageProvider(context);
    receiver = new DeziBotPackageReceiver(context, _arena, _agentProperties);
}

AgentMoveSpace::~AgentMoveSpace(){
    delete provider;
    delete receiver;
}

repast::Point<double> AgentMoveSpace::getLocation(repast::AgentId &id){
    std::vector<double> result;
    space->getLocation(id, result);
    return repast::Point<double>(result[0], result[1]);
}

void AgentMoveSpace::moveTo(repast::AgentId &id, double x, double y){
    std::vector<double> agentNewLocContinuous;
    
    // clamp value
    agentNewLocContinuous.push_back(boost::algorithm::clamp(x, origin->getX(), maxPoint->getX()));
    agentNewLocContinuous.push_back(boost::algorithm::clamp(y, origin->getY(), maxPoint->getY()));

    space->moveTo(id, agentNewLocContinuous);
}

void AgentMoveSpace::moveToRandomLocation(repast::AgentId id){
    double randomX = (repast::Random::instance()->nextDouble() * extent->getX()) - extent->getX()/2.0;
	double randomY = (repast::Random::instance()->nextDouble() * extent->getY()) - extent->getY()/2.0;
	moveTo(id, randomX, randomY);
}

Vector2D* AgentMoveSpace::moveForwardAndBounceOffWalls(repast::AgentId &id, Vector2D* velocity){
    Vector2D position = Vector2D(getLocation(id));

    double remainingDistance = velocity->length();
    while(remainingDistance > 0.0) {
        // Based on velocity direction only two walls can be colided with
        WallDirection verticalWall = (velocity->getX() >= 0.0) ? EAST : WEST;
        WallDirection horizontalWall = (velocity->getY() >= 0.0) ? NORTH : SOUTH;
        repast::Point<double>* wallIntersectionVertical = getWallIntersection(verticalWall, &position, velocity);
        repast::Point<double>* wallIntersectionHorizontal = getWallIntersection(horizontalWall, &position, velocity);

        double verticalWallDistanceSq;
        double horizontalWallDistanceSq;

        if (wallIntersectionVertical == nullptr) {
            // only one collision possible
            remainingDistance = calculateMoveTowardsWallIntersection(&position, velocity, horizontalWall, wallIntersectionHorizontal, remainingDistance);
        } else if (wallIntersectionHorizontal == nullptr) {
            // only one collision possible
            remainingDistance = calculateMoveTowardsWallIntersection(&position, velocity, verticalWall, wallIntersectionVertical, remainingDistance);
        } else {
            // multiple collisions possible and order of collision has to be determined
            verticalWallDistanceSq = getDistanceSq(position.toPoint(), *wallIntersectionVertical);
            horizontalWallDistanceSq = getDistanceSq(position.toPoint(), *wallIntersectionHorizontal);

            // check which collision comes first
            if (horizontalWallDistanceSq <= verticalWallDistanceSq) {
                remainingDistance = calculateMoveTowardsWallIntersection(&position, velocity, horizontalWall, wallIntersectionHorizontal, remainingDistance);
            } else {
                remainingDistance = calculateMoveTowardsWallIntersection(&position, velocity, verticalWall, wallIntersectionVertical, remainingDistance);
            }
        }
    }

    moveTo(id, position.getX(), position.getY());
    return velocity;
}

void AgentMoveSpace::synchronize(){
    space->balance();
    repast::RepastProcess::instance()->synchronizeAgentStatus<DeziBot, DeziBotPackage, DeziBotPackageProvider, DeziBotPackageReceiver>(*context, *provider, *receiver, *receiver);
    repast::RepastProcess::instance()->synchronizeProjectionInfo<DeziBot, DeziBotPackage, DeziBotPackageProvider, DeziBotPackageReceiver>(*context, *provider, *receiver, *receiver);
	repast::RepastProcess::instance()->synchronizeAgentStates<DeziBotPackage, DeziBotPackageProvider, DeziBotPackageReceiver>(*provider, *receiver);
}