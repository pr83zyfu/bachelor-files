/* SEIRAgent.cpp */

#include "repast_hpc/AgentId.h"

#include "base/SimulationArena.hpp"
#include "base/SEIRAgent.hpp"

bool SEIRAgent::isInfectable() {
    double transmissionRadiusSq = transmissionRadius * transmissionRadius;

    auto iter = space->queryAgents();

    while (iter.base() != iter.end()) {
        /* Check infection State first for efficiency */
        if ((*iter)->getInfectionState() == INFECTIOUS
            && space->getDistanceSq(getPosition(), (*iter)->getPosition()) <= transmissionRadiusSq) {
            return true;
        }
        iter++;
    }
    return false;
}

void SEIRAgent::updateInfectionState(const double currentTick) {
    switch (infectionState) {
    case SUSCEPTIBLE:
        if (isInfectable()) {
            // get random value and infect based on Probability from properties
            if (repast::Random::instance()->nextDouble() <= transmissionProbability) {
                switchSEIRstates(infectionState, EXPOSED);
                return;
            }
        }
        break;
    case EXPOSED:
        if (exposedTimer == 0) {
            switchSEIRstates(infectionState, INFECTIOUS);
            return;
        } else {
            exposedTimer--;
        }

        break;
    case INFECTIOUS:
        if (infectiousTimer == 0) {
            // symptoms end
            setSpeed(normalSpeed);
            switchSEIRstates(infectionState, RECOVERED);
            return;
        } else {
            infectiousTimer--;
        }

        if (realizationTimer == 0) {
            knowsAboutInfection = true;
            // symptoms start
            setSpeed(infectedSpeed);
        } else {
            realizationTimer--;
        }

        break;
    case RECOVERED:
        break;
    }
}

SEIRAgent::SEIRAgent() {

}

SEIRAgent::SEIRAgent(repast::AgentId _id,
    SimulationArena* _space,
    AgentProperties* agentProperties,
    double _speed,
    Vector2D _velocity,
    SEIRState _infectionState,
    bool _knowsAboutInfection,
    int _exposedTimer,
    int _infectiousTimer,
    int _realizationTimer) : Agent(_id, _space, agentProperties, _speed, _velocity) {
    infectionState = _infectionState;
    transmissionProbability = agentProperties->transmissionProbability;
    transmissionRadius = agentProperties->transmissionRadius;
    knowsAboutInfection = _knowsAboutInfection;

    normalSpeed = agentProperties->speed;
    infectedSpeed = agentProperties->infectedSpeed;

    exposedTimer = _exposedTimer;
    infectiousTimer = _infectiousTimer;
    realizationTimer = _realizationTimer;

    exposedTimerStart = agentProperties->exposedTime;
    infectiousTimerStart = agentProperties->infectiousTime;
    realizationTimerStart = agentProperties->realizationTime;
}

SEIRAgent::SEIRAgent(repast::AgentId _id,
    SimulationArena* _space,
    AgentProperties* agentProperties) : Agent(_id, _space, agentProperties) {
    transmissionProbability = agentProperties->transmissionProbability;
    transmissionRadius = agentProperties->transmissionRadius;

    exposedTimerStart = agentProperties->exposedTime;
    infectiousTimerStart = agentProperties->infectiousTime;
    realizationTimerStart = agentProperties->realizationTime;

    normalSpeed = agentProperties->speed;
    infectedSpeed = agentProperties->infectedSpeed;

    // Initialization of Default values

    infectionState = SUSCEPTIBLE;
    knowsAboutInfection = false;

    exposedTimer = 0;
    infectiousTimer = 0;
    realizationTimer = 0;
}

SEIRAgent::~SEIRAgent() {

}

void SEIRAgent::switchSEIRstates(SEIRState _oldState, SEIRState _newState) {
    // Start timers
    switch (_newState) {
    case SUSCEPTIBLE:
        break;
    case EXPOSED:
        exposedTimer = exposedTimerStart;
        break;
    case INFECTIOUS:
        knowsAboutInfection = false;
        infectiousTimer = infectiousTimerStart;
        realizationTimer = realizationTimerStart;
        break;
    case RECOVERED:
        break;
    }

    infectionState = _newState;
}