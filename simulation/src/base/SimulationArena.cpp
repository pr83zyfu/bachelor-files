/* SimulationArena.cpp */

#include "repast_hpc/SharedContinuousSpace.h"
#include "repast_hpc/Context.h"

#include "SimulationProperties.hpp"
#include "base/AgentMoveSpace.hpp"
#include "base/SimulationArena.hpp"
#include "resource/ResourceSpace.hpp"
#include "pheromone/EfficientPheromoneMap.hpp"

void SimulationArena::checkForResourcePickup() {
    double collectionDistanceSq = properties->agent->resourceCollectionRadius * properties->agent->resourceCollectionRadius;

    auto resIter = queryResources();

    std::vector<repast::AgentId>* toRemove = new std::vector<repast::AgentId>();

    while (resIter.base() != resIter.end()) {
        // only check for local resources
        if ((*resIter)->getId().currentRank() != rank) {
            resIter++;
            continue;
        }
        checkForBotsToPickup((*resIter), collectionDistanceSq);

        if ((*resIter)->isEmpty()) {
            toRemove->push_back((*resIter)->getId());
        }

        resIter++;
    }

    for (size_t i = 0; i < toRemove->size(); i++) {
        repast::AgentId id = toRemove->at(i);
        // remove resource
        resourceSpace->remove(id);

        // construct new resource
        resourceSpace->constructRandomResource(id, properties->resourceGenerator->size, properties->resourceGenerator->size);
    }
}

void SimulationArena::checkForBotsToPickup(boost::shared_ptr<Resource> resource, double collectionDistanceSq) {
    repast::AgentId resId = resource->getId();
    repast::Point<double> resPos = resourceSpace->getLocation(resId);

    auto botIter = queryAgents();
    while (botIter.base() != botIter.end()) {
        if (getDistanceSq(resPos, (*botIter)->getPosition()) <= collectionDistanceSq) {
            // bot is in range to collect
            if (!(*botIter)->hasAlreadyCollectedResource(resId)) {
                // collect
                resource->collect(1);
                (*botIter)->addCollectedResource(resId);

                if (resource->isEmpty()) {
                    return;
                }
            }
        }
        botIter++;
    }
}

SimulationArena::SimulationArena(SimulationProperties* _properties, int _rank) {
    properties = _properties;
    rank = _rank;

    // init origin, extent, maxPoint
    extent = new repast::Point<double>(properties->arena->width, properties->arena->height);
    maxPoint = new repast::Point<double>(extent->getX() / 2.0, extent->getY() / 2.0);
    origin = new repast::Point<double>(maxPoint->getX() * -1.0, maxPoint->getY() * -1.0);

    agentContext = new repast::SharedContext<DeziBot>(properties->communicator);

    // the buffer has to be the maximum of all queried radiusses for the simulation to work correctly
    double minimalAgentBuffer = std::max(properties->agent->transmissionRadius, properties->agent->resourceCollectionRadius);

    agentSpace = new AgentMoveSpace(
        agentContext,
        properties->communicator,
        origin,
        extent,
        maxPoint,
        minimalAgentBuffer,
        this,
        properties->agent);

    // init resourceContext
    resourceContext = new repast::SharedContext<Resource>(properties->communicator);

    double minimalResourceBuffer = std::max(properties->agent->resourceCollectionRadius, properties->agent->resourceDetectionRadius);
    resourceSpace = new ResourceSpace(
        resourceContext,
        properties->communicator,
        origin,
        extent,
        maxPoint,
        minimalResourceBuffer,
        rank);

    // init resourceIndicatorPheromone
    // double minimalResourceIndicatorPheromoneBuffer = std::max(properties->agent->pheromoneDetectionRadius, properties->agent->pheromoneResourceIndicatorDrawRadius);
    // resourceIndicatorPheromone = new PheromoneMap(
    //     rank,
    //     properties->communicator,
    //     origin,
    //     extent,
    //     maxPoint,
    //     minimalResourceIndicatorPheromoneBuffer,
    //     "Resource Indicator Space",
    //     properties->pheromone->resourceIndicator->lifetime
    // );
    resourceIndicatorPheromone = new EfficientPheromoneMap(
        origin,
        extent,
        maxPoint,
        properties->pheromone->resourceIndicator->lifetime,
        properties->communicator);

    // init warning pheromone
    // double minimalWarningPheromoneBuffer = std::max(properties->agent->pheromoneDetectionRadius, properties->agent->pheromoneWarningDrawRadius);
    // warningPheromone = new PheromoneMap(
    //     rank,
    //     properties->communicator,
    //     origin,
    //     extent,
    //     maxPoint,
    //     minimalWarningPheromoneBuffer,
    //     "Warning Pheromone Space",
    //     properties->pheromone->warning->lifetime
    // );
    warningPheromone = new EfficientPheromoneMap(
        origin,
        extent,
        maxPoint,
        properties->pheromone->warning->lifetime,
        properties->communicator);
}

SimulationArena::~SimulationArena() {
    delete origin;
    delete extent;
    delete maxPoint;

    delete agentContext;
    delete agentSpace;

    delete resourceContext;
    delete resourceSpace;

    delete resourceIndicatorPheromone;
    delete warningPheromone;
}

double SimulationArena::getDistance(repast::Point<double> p1, repast::Point<double> p2) {
    return agentSpace->getDistance(p1, p2);
}

double SimulationArena::getDistanceSq(repast::Point<double> p1, repast::Point<double> p2) {
    return agentSpace->getDistanceSq(p1, p2);
}

void SimulationArena::update(const double currentTick) {
    /* Update Agents */
    auto iter = queryAgents();

    while (iter.base() != iter.end()) {
        if ((*iter)->getId().currentRank() == rank) {
            (*iter)->update(currentTick);
        }
        iter++;
    }

    /* Check for Resource Pickup */
    checkForResourcePickup();

    // Synchronize spaces
    resourceSpace->synchronize();
    agentSpace->synchronize();
    resourceIndicatorPheromone->synchronize();
    warningPheromone->synchronize();
}

void SimulationArena::constructNewAgent(int rank, int i, SEIRState infectionState) {
    repast::AgentId id(i, rank, 0);
    id.currentRank(rank);

    DeziBot* agent = new DeziBot(
        id,
        this,
        properties->agent);

    agentContext->addAgent(agent);

    // randomise initial rotation (in radians: max = 2pi)
    double randomRotation = repast::Random::instance()->nextDouble() * (2.0 * M_PI);
    agent->rotate(randomRotation);

    // randomise initial location
    agentSpace->moveToRandomLocation(id);

    // TEMP override rotation and location
    // agentSpace->moveTo(agent->getId(), 40.0, 0.0);
    // agent->rotateTowards(0.0, 0.0);

    // call initial setup
    agent->setup();

    agent->switchSEIRstates(agent->getInfectionState(), infectionState);
}

typename repast::Context<DeziBot>::const_bytype_iterator SimulationArena::queryAgents() {
    return agentContext->byTypeBegin(0);
}

typename repast::Context<Resource>::const_bytype_iterator SimulationArena::queryResources() {
    return resourceContext->byTypeBegin(1);
}