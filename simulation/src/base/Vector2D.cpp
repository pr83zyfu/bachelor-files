/* Vector2D.cpp */

#include <vector>
#include <math.h>

#include "repast_hpc/Point.h"

#include "base/Vector2D.hpp"

Vector2D::Vector2D(){
    x = 0.0;
    y = 0.0;
}

Vector2D::Vector2D(double _x, double _y){
    x = _x;
    y = _y;
}

Vector2D::Vector2D(repast::Point<double> point){
    x = point.getX();
    y = point.getY();
}

Vector2D::Vector2D(std::vector<double> std_vector){
    x = std_vector[0];
    y = std_vector[1];
}

Vector2D::~Vector2D(){
    
}

void Vector2D::scale(double scalar){
    x *= scalar;
    y *= scalar;
}

void Vector2D::scaleTo(double scalar){
    scale(scalar / length());
}

void Vector2D::normalise() {
    scaleTo(1.0);
}

void Vector2D::rotate(double angle){
    double newX = (cos(angle) * x) - (sin(angle) * y);
    double newY = (sin(angle) * x) + (cos(angle) * y);

    x = newX;
    y = newY;
}

double Vector2D::length(){
    return sqrt((x*x) + (y*y));
}

double Vector2D::dotProduct(Vector2D* other){
    return (x * other->getX()) + (y * other->getY());
}

std::string Vector2D::toString(){
    return "(" + std::to_string(x) + ", " + std::to_string(y) + ")";
}

repast::Point<double> Vector2D::toPoint(){
    return repast::Point<double>(x, y);
}

std::vector<double> Vector2D::toVector(){
    return std::vector<double>(x, y);
}