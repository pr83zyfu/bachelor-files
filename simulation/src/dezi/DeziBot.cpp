/* DeziBot.cpp */

#include <vector>
#include <stdio.h>
#include <stdexcept>

#include "SimulationProperties.hpp"
#include "base/SimulationArena.hpp"
#include "dezi/DeziBot.hpp"
#include "base/SEIRState.hpp"
#include "resource/ResourceSpace.hpp"
#include "pheromone/PheromoneMap.hpp"

#include "repast_hpc/Context.h"
#include "repast_hpc/SharedContext.h"

#include "repast_hpc/Moore2DGridQuery.h"
#include "repast_hpc/Point.h"
#include "repast_hpc/GridDimensions.h"

boost::shared_ptr<Resource> DeziBot::getFirstDetectableResource() {
    double resourceDetectionRadiusSq = resourceDetectionRadius * resourceDetectionRadius;
    auto resIter = space->queryResources();
    repast::Point<double> position = getPosition();
    while (resIter.base() != resIter.end()) {
        repast::Point<double> resPosition = (*resIter)->getPosition();
        if (space->getDistanceSq(position, resPosition) <= resourceDetectionRadiusSq) {
            return (*resIter);
        }
        resIter++;
    }

    return nullptr;
}

boost::shared_ptr<Resource> DeziBot::getFirstDetectableUncollectedResource() {
    double resourceDetectionRadiusSq = resourceDetectionRadius * resourceDetectionRadius;
    auto resIter = space->queryResources();
    repast::Point<double> position = getPosition();
    while (resIter.base() != resIter.end()) {
        repast::Point<double> resPosition = (*resIter)->getPosition();
        if (space->getDistanceSq(position, resPosition) <= resourceDetectionRadiusSq) {
            if (!hasAlreadyCollectedResource((*resIter)->getId())) {
                return (*resIter);
            }
        }
        resIter++;
    }

    return nullptr;
}

Vector2D DeziBot::getVectorAwayFromAgents(const double currentTick, std::vector<boost::shared_ptr<DeziBot>>& agents) {
    auto directionVectorAwayFromAgents = Vector2D();
    auto pos = getPosition();
    for (auto agent : agents) {
        auto agentPos = agent->getPosition();
        directionVectorAwayFromAgents.setAdd(pos.getX() - agentPos.getX(), pos.getY() - agentPos.getY());
    }

    return directionVectorAwayFromAgents;
}

void DeziBot::getInfectiousRobotsInTransmissionRange(const double currentTick, std::vector<boost::shared_ptr<DeziBot>>& out) {
    double transmissionRadiusSq = transmissionRadius * transmissionRadius;

    auto iter = space->queryAgents();

    while (iter.base() != iter.end()) {
        /* Check infection State first for efficiency */
        if ((*iter)->getInfectionState() == INFECTIOUS
            && space->getDistanceSq(getPosition(), (*iter)->getPosition()) <= transmissionRadiusSq) {
            out.push_back((*iter));
        }
        iter++;
    }
}

void DeziBot::getIntensitiesInfrontOfRobot(IPheromoneMap* pheromone, const int valuesToFront, const int valuesToEachSide, const double currentTick, std::vector<std::vector<double>>& out) {
    Vector2D currentMidPoint = Vector2D(getPosition());

    Vector2D normalisedVelocity = Vector2D();
    Vector2D normalisedRightVector = Vector2D();
    normalisedVelocity.setCopy(velocity);
    normalisedVelocity.normalise();
    normalisedRightVector.setCopy(normalisedVelocity);
    normalisedRightVector.rotate(M_PI_2);

    for (int i = 0; i < valuesToFront; i++) {
        // move a distance of 1 along velocity vector
        currentMidPoint.setAdd(normalisedVelocity);
        // size of vector: 1 for middle strip + values to each side (per side: * 2)
        out.push_back(std::vector<double>(1 + (2 * valuesToEachSide)));
        // out.at(i) = std::vector<double>(1 + (2 * valuesToEachSide));

        // fill middle values
        out.at(i).at(valuesToEachSide) = pheromone->getIntensity(currentMidPoint.getX(), currentMidPoint.getY(), currentTick);
        // fill sides (from middle outwards)
        for (int j = 1; j < valuesToEachSide; j++) {
            // fill to left
            out.at(i).at(valuesToEachSide - j) = pheromone->getIntensity(
                currentMidPoint.getX() - (j * normalisedRightVector.getX()),
                currentMidPoint.getY() - (j * normalisedRightVector.getY()),
                currentTick);
            // fill to right
            out.at(i).at(valuesToEachSide + j) = pheromone->getIntensity(
                currentMidPoint.getX() + (j * normalisedRightVector.getX()),
                currentMidPoint.getY() + (j * normalisedRightVector.getY()),
                currentTick);
        }
    }
}

bool DeziBot::detectablePheromoneInFront(IPheromoneMap* pheromone, const double currentTick) {
    auto intensities = std::vector<std::vector<double>>();
    getIntensitiesInfrontOfRobot(pheromone, pheromoneDetectionToFront, pheromoneDetectionToSides, currentTick, intensities);

    for (int i = 0; i < intensities.size(); i++) {
        for (int j = 0; j < intensities.at(i).size(); j++) {
            if (intensities.at(i).at(j) > 0.0) {
                return true;
            }
        }
    }

    return false;
}


bool DeziBot::changeDirectionToFollowPheromone(IPheromoneMap* pheromone, const double currentTick) {
    auto intensities = std::vector<std::vector<double>>();
    getIntensitiesInfrontOfRobot(pheromone, pheromoneDetectionToFront, pheromoneDetectionToSides, currentTick, intensities);

    for (int i = 0; i < intensities.size(); i++) {
        if (intensities.at(i).at(pheromoneDetectionToFront) > 0.0) {
            return true;
        }

        for (int j = 0; j < pheromoneDetectionToSides; j++) {
            if (intensities.at(i).at(pheromoneDetectionToSides - j) > 0.0) {
                rotateToPointToFrontAndRight(1 + i, -j);
                return true;
            }
            if (intensities.at(i).at(pheromoneDetectionToSides + j) > 0.0) {
                rotateToPointToFrontAndRight(1 + i, j);
                return true;
            }
        }
    }

    return false;
}

bool DeziBot::isKnownInfectious() {
    return (infectionState == INFECTIOUS && knowsAboutInfection);
}

bool DeziBot::detectedWarning(const double currentTick) {
    return detectablePheromoneInFront(space->getWarninigPheromone(), currentTick);
}

DeziBot::DeziBot() {}

DeziBot::DeziBot(repast::AgentId _id) { id = _id; }

DeziBot::DeziBot(
    repast::AgentId _id,
    SimulationArena* _space,
    AgentProperties* agentProperties,
    double _speed,
    Vector2D _velocity,
    SEIRState _infectionState,
    bool _knowsAboutInfection,
    int _exposedTimer,
    int _infectiousTimer,
    int _realizationTimer,
    ResourceSearchState _resourceSearchState,
    double _lengthLeftToDraw,
    bool _movingOutOfWarningArea,
    ResourceListPointer _resourceList,
    int _resourceWaitAfterCollectionTimer,
    bool _isFollowingPhatomPheromone,
    bool _isFollowingWarnPheromone,
    bool _foundResourceViaIndicator) : SEIRAgent(_id, _space, agentProperties, _speed, _velocity, _infectionState, _knowsAboutInfection, _exposedTimer, _infectiousTimer, _realizationTimer) {
    resourceSearchState = _resourceSearchState;
    lengthLeftToDraw = _lengthLeftToDraw;
    movingOutOfWarningArea = _movingOutOfWarningArea;

    phantomStartDetectionChance = agentProperties->phantomStartDetectionChance;
    phantomStopDetectionChance = agentProperties->phantomStopDetectionChance;
    isFollowingPhantomPheromone = _isFollowingPhatomPheromone;
    isFollowingWarnPheromone = _isFollowingWarnPheromone;

    runType = agentProperties->runType;
    quarantaineZone = agentProperties->quarantaineZone;

    resourceDetectionRadius = agentProperties->resourceDetectionRadius;
    resourceCollectionRadius = agentProperties->resourceCollectionRadius;
    alreadyCollectedResources = _resourceList;
    resourceWaitAfterCollectionTimer = _resourceWaitAfterCollectionTimer;
    resourceWaitAfterCollectionTimerStart = agentProperties->resourceWaitAfterCollectionTime;

    pheromoneDetectionToSides = agentProperties->pheromoneDetectionToSides;
    pheromoneDetectionToFront = agentProperties->pheromoneDetectionToFront;
    pheromoneDrawRadius = agentProperties->pheromoneDrawRadius;
    pheromoneDrawWidth = agentProperties->pheromoneDrawWidth;
    pheromoneDrawHeight = agentProperties->pheromoneDrawHeight;

    pheromoneResourceIndicatorUse = agentProperties->pheromoneResourceIndicatorUse;
    pheromoneResourceIndicatorDrawLength = agentProperties->pheromoneResourceIndicatorDrawLength;
    foundResourceViaIndicator = _foundResourceViaIndicator;
}

DeziBot::DeziBot(
    repast::AgentId _id,
    SimulationArena* _space,
    AgentProperties* agentProperties) : SEIRAgent(_id, _space, agentProperties) {
    resourceSearchState = EXPLORE;
    movingOutOfWarningArea = false;

    phantomStartDetectionChance = agentProperties->phantomStartDetectionChance;
    phantomStopDetectionChance = agentProperties->phantomStopDetectionChance;
    isFollowingPhantomPheromone = false;
    isFollowingWarnPheromone = false;

    runType = agentProperties->runType;
    quarantaineZone = agentProperties->quarantaineZone;

    resourceDetectionRadius = agentProperties->resourceDetectionRadius;
    resourceCollectionRadius = agentProperties->resourceCollectionRadius;
    alreadyCollectedResources = ResourceListPointer(new ResourceList());
    resourceWaitAfterCollectionTimer = 0;
    resourceWaitAfterCollectionTimerStart = agentProperties->resourceWaitAfterCollectionTime;

    pheromoneDetectionToSides = agentProperties->pheromoneDetectionToSides;
    pheromoneDetectionToFront = agentProperties->pheromoneDetectionToFront;
    pheromoneDrawRadius = agentProperties->pheromoneDrawRadius;
    pheromoneDrawWidth = agentProperties->pheromoneDrawWidth;
    pheromoneDrawHeight = agentProperties->pheromoneDrawHeight;

    pheromoneResourceIndicatorUse = agentProperties->pheromoneResourceIndicatorUse;
    pheromoneResourceIndicatorDrawLength = agentProperties->pheromoneResourceIndicatorDrawLength;
    foundResourceViaIndicator = false;
}

DeziBot::~DeziBot() {
}

void DeziBot::updateAttributes(
    double _speed,
    Vector2D _velocity,
    SEIRState _infectionState,
    bool _knowsAboutInfection,
    int _exposedTimer,
    int _infectiousTimer,
    int _realizationTimer,
    ResourceSearchState _resourceSearchState,
    ResourceListPointer _resourceList,
    int _resourceWaitAfterCollectionTimer,
    double _lengthLeftToDraw,
    bool _movingOutOfWarningArea,
    bool _isFollowingPhatomPheromone,
    bool _isFollowingWarnPheromone,
    bool _foundResourceViaIndicator) {
    currentSpeed = _speed;
    velocity = _velocity;

    infectionState = _infectionState;
    knowsAboutInfection = _knowsAboutInfection;
    exposedTimer = _exposedTimer;
    infectiousTimer = _infectiousTimer;
    realizationTimer = _realizationTimer;

    resourceSearchState = _resourceSearchState;
    alreadyCollectedResources = _resourceList;
    resourceWaitAfterCollectionTimer = _resourceWaitAfterCollectionTimer;

    lengthLeftToDraw = _lengthLeftToDraw;
    movingOutOfWarningArea = _movingOutOfWarningArea;
    isFollowingPhantomPheromone = _isFollowingPhatomPheromone;
    isFollowingWarnPheromone = _isFollowingWarnPheromone;
    foundResourceViaIndicator = _foundResourceViaIndicator;
}

bool DeziBot::hasAlreadyCollectedResource(const repast::AgentId& resourceId) {
    for (repast::AgentId iterId : *alreadyCollectedResources) {
        // ignore current rank
        if (resourceId.id() == iterId.id()
            && resourceId.agentType() == iterId.agentType()
            && resourceId.startingRank() == iterId.startingRank()) {
            return true;
        }
    }

    return false;
}

void DeziBot::addCollectedResource(repast::AgentId resourceId) {
    alreadyCollectedResources->push_back(resourceId);
}

void DeziBot::setup() {
    infectionState = SUSCEPTIBLE;
}

static double degreeToRad(double degree) { return degree * (M_PI / 180.0); };

void DeziBot::update(const double currentTick) {
    switch (runType) {
    case INFECTIOUS_PHANTOM_RESOURCE_INDICATOR:
        updateInfectiousPhantomResourceIndicator(currentTick);
        break;
    case INFECTIOUS_ONLY_CRW:
        updateInfectiousOnlyCRW(currentTick);
        break;
    case INFECTIOUS_CLUSTERING:
        updateInfectiousClustering(currentTick);
        break;
    case SUSCEPTIBLE_AVOID_AND_INFECTIOUS_STAND_STILL:
        updateSusceptibleAvoidAndInfectiousStandStille(currentTick);
        break;
    case SUSCEPTIBLE_AVOID:
        updateSusceptibleAvoid(currentTick);
        break;
    case INFECTIOUS_STAND_STILL:
        updateInfectiousStandStill(currentTick);
        break;
    case BASELINE_WITH_SEIR:
        updateBaselineSeir(currentTick);
        break;
    case BASELINE_WITHOUT_SEIR:
        updateBaselineNoSeir(currentTick);
        break;
    default:
        throw std::out_of_range("Invalid RunType loaded");
        break;
    }
}

void DeziBot::updateWithWarningPheromone(const double currentTick) {
    if (movingOutOfWarningArea) {
        moveForward();

        if (!detectedWarning(currentTick)) {
            // not in warning area anymore
            movingOutOfWarningArea = false;
        }

        return;
    }

    if (detectedWarning(currentTick) && !isKnownInfectious()) {
        // detected warning and is not itself warning
        turnAround();
        movingOutOfWarningArea = true;

        // do first move
        updateWithWarningPheromone(currentTick);

        return;
    }

    if (isKnownInfectious()) {
        // draw warn pheromone
        space->getWarninigPheromone()->drawCircle(getPosition(), pheromoneDrawRadius, 1.0, currentTick);
        baseBehaviour(currentTick);
    }
}

void DeziBot::updateWithQuarantaineZone(const double currentTick) {
    if (isKnownInfectious()) {
        // move To Quarantaine Zone
        rotateTowards(*quarantaineZone);
        moveForward();
        return;
    }

    baseBehaviour(currentTick);
}

void DeziBot::switchResourceSearchState(ResourceSearchState oldState, ResourceSearchState newState) {
    switch (newState) {
    case EXPLORE:
        break;
    case FOLLOW:
        if (oldState == DRAW) {
            turnAround();
        }
        break;
    case DRAW:
        turnAround();
        lengthLeftToDraw = pheromoneResourceIndicatorDrawLength;
        break;
    case WAIT:
        foundResourceViaIndicator = (oldState == FOLLOW);
        resourceWaitAfterCollectionTimer = resourceWaitAfterCollectionTimerStart;
        break;
    }

    resourceSearchState = newState;
}

void DeziBot::baseBehaviour(const double currentTick, bool canInteractWithResourceIndicator, bool noRandomWalk) {
    if (resourceSearchState == EXPLORE) {
        if (noRandomWalk) {
            moveForward();
        } else {
            randomWalk();
        }

        // look for resource
        auto res = getFirstDetectableUncollectedResource();
        if (pheromoneResourceIndicatorUse && canInteractWithResourceIndicator) {
            if (res != nullptr) {
                rotateTowards(res->getPosition());
                switchResourceSearchState(resourceSearchState, WAIT);
                return;
            }

            if (detectablePheromoneInFront(space->getResourceIndicatorPheromone(), currentTick)) {
                //switchResourceSearchState(resourceSearchState, FOLLOW);
                return;
            }
        } else {
            if (res != nullptr) {
                rotateTowards(res->getPosition());
                switchResourceSearchState(resourceSearchState, WAIT);
                return;
            }
        }
        return;
    }

    if (resourceSearchState == FOLLOW) {
        // if cant interact stop follow
        if (!canInteractWithResourceIndicator) {
            switchResourceSearchState(resourceSearchState, EXPLORE);
            randomWalk();
            return;
        }

        // if found resource
        auto res = getFirstDetectableUncollectedResource();
        if (res != nullptr) {
            rotateTowards(res->getPosition());
            //std::cout << "Found Source" << std::endl;
            switchResourceSearchState(resourceSearchState, WAIT);
            return;
        }

        if (changeDirectionToFollowPheromone(space->getResourceIndicatorPheromone(), currentTick)) {
            moveForward();
            return;
        }

        // no pheromone found anymore
        randomWalk();
        switchResourceSearchState(resourceSearchState, EXPLORE);
        return;
    }

    if (resourceSearchState == DRAW) {
        // if cant interact stop draw
        if (!canInteractWithResourceIndicator) {
            switchResourceSearchState(resourceSearchState, EXPLORE);
            randomWalk();
            return;
        }

        space->getResourceIndicatorPheromone()->drawRectangleBehindPointWithDirection(getPosition(), getVelocity(), pheromoneDrawWidth, pheromoneDrawHeight, 1.0, currentTick);
        moveForward();
        lengthLeftToDraw -= velocity.length();
        if (lengthLeftToDraw <= 0.0) {
            switchResourceSearchState(resourceSearchState, EXPLORE);
            return;
        }
        return;
    }

    if (resourceSearchState == WAIT) {
        resourceWaitAfterCollectionTimer--;
        if (resourceWaitAfterCollectionTimer <= 0) {
            if (!foundResourceViaIndicator) {
                //switchResourceSearchState(resourceSearchState, DRAW);
                switchResourceSearchState(resourceSearchState, EXPLORE);
            } else {
                switchResourceSearchState(resourceSearchState, EXPLORE);
            }
        }
        return;
    }
}

void DeziBot::updateBaselineNoSeir(const double currentTick) {
    // no infection state update
    baseBehaviour(currentTick);
}

void DeziBot::updateBaselineSeir(const double currentTick) {
    updateInfectionState(currentTick);
    baseBehaviour(currentTick);
}

void DeziBot::updateInfectiousStandStill(const double currentTick) {
    updateInfectionState(currentTick);

    if (isSymptomatic()) {
        return;
    }

    baseBehaviour(currentTick);
}

void DeziBot::updateSusceptibleAvoid(const double currentTick) {
    updateInfectionState(currentTick);

    if (infectionState == SUSCEPTIBLE) {
        auto infectiousAgents = std::vector<boost::shared_ptr<DeziBot>>();
        getInfectiousRobotsInTransmissionRange(currentTick, infectiousAgents);

        if (!infectiousAgents.empty()) {
            setVelocity(getVectorAwayFromAgents(currentTick, infectiousAgents));
        }
    }

    baseBehaviour(currentTick);
}

void DeziBot::updateSusceptibleAvoidAndInfectiousStandStille(const double currentTick) {
    updateInfectionState(currentTick);

    if (isSymptomatic()) {
        return;
    }

    if (infectionState == SUSCEPTIBLE) {
        auto infectiousAgents = std::vector<boost::shared_ptr<DeziBot>>();
        getInfectiousRobotsInTransmissionRange(currentTick, infectiousAgents);

        if (!infectiousAgents.empty()) {
            setVelocity(getVectorAwayFromAgents(currentTick, infectiousAgents));
        }
    }

    baseBehaviour(currentTick);
}

void DeziBot::updateInfectiousClustering(const double currentTick) {
    updateInfectionState(currentTick);

    if (!isSymptomatic()) {
        baseBehaviour(currentTick);
        return;
    }

    if (detectablePheromoneInFront(space->getWarninigPheromone(), currentTick)) {
        isFollowingWarnPheromone = true;
    }

    // change direction to follow warn pheromone
    if (isFollowingWarnPheromone) {
        if (!changeDirectionToFollowPheromone(space->getWarninigPheromone(), currentTick)) {
            isFollowingWarnPheromone == false;
        }
    }

    moveForward();

    // draw warn pheromone behind
    space->getWarninigPheromone()->drawRectangleBehindPointWithDirection(getPosition(), getVelocity(), pheromoneDrawWidth, pheromoneDrawHeight, 1.0, currentTick);
}

void DeziBot::updateInfectiousOnlyCRW(const double currentTick) {
    updateInfectionState(currentTick);

    baseBehaviour(currentTick, !isSymptomatic());
}

void DeziBot::updateInfectiousPhantomResourceIndicator(const double currentTick) {
    updateInfectionState(currentTick);

    if (!isSymptomatic()) {
        baseBehaviour(currentTick);
        return;
    }

    // agent is symptomatic => cant work with real resource indicator and detects random pheromone

    // randomly start following phantom
    if (!isFollowingPhantomPheromone && repast::Random::instance()->nextDouble() <= phantomStartDetectionChance) {
        isFollowingPhantomPheromone = true;
    }

    if (isFollowingPhantomPheromone) {
        auto id = getId();

        // randomly stop following phantom
        if (repast::Random::instance()->nextDouble() <= phantomStopDetectionChance) {
            isFollowingPhantomPheromone = false;
        } else {
            baseBehaviour(currentTick, false, true);
            return;
        }
    }

    baseBehaviour(currentTick, false);
}