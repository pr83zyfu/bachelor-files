/* DeziBotPackage.cpp */

#include "base/Vector2D.hpp"

#include "dezi/DeziBotPackage.hpp"
#include "dezi/DeziBot.hpp"

/* Serializable Agent Package Data */

DeziBotPackage::DeziBotPackage() {}

DeziBotPackage::DeziBotPackage(
    int _id,
    int _rank,
    int _type,
    int _currentRank,
    double _speed,
    Vector2D _velocity,
    SEIRState _infectionState,
    bool _knowsAboutInfection,
    int _exposedTimer,
    int _infectiousTimer,
    int _realizationTimer,
    ResourceSearchState _resourceSearchState,
    DeziBot::ResourceList _alreadyCollectedResources,
    int _resourceWaitAfterCollectionTimer,
    double _lengthLeftToDraw,
    bool _movingOutOfWarningArea,
    bool _isFollowingPhatomPheromone,
    bool _isFollowingWarnPheromone,
    bool _foundResourceViaIndicator) :
    id(_id),
    rank(_rank),
    type(_type),
    currentRank(_currentRank),
    speed(_speed),
    velocityX(_velocity.getX()),
    velocityY(_velocity.getY()),
    infectionState(_infectionState),
    knowsAboutInfection(_knowsAboutInfection),
    exposedTimer(_exposedTimer),
    infectiousTimer(_infectiousTimer),
    realizationTimer(_realizationTimer),
    resourceSearchState(_resourceSearchState),
    alreadyCollectedResources(_alreadyCollectedResources),
    resourceWaitAfterCollectionTimer(_resourceWaitAfterCollectionTimer),
    lengthLeftToDraw(_lengthLeftToDraw),
    movingOutOfWarningArea(_movingOutOfWarningArea),
    isFollowingPhantomPheromone(_isFollowingPhatomPheromone),
    isFollowingWarnPheromone(_isFollowingWarnPheromone),
    foundResourceViaIndicator(_foundResourceViaIndicator) {
}