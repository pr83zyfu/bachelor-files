/* DeziBotPackageProvider.cpp */

#include "dezi/DeziBotPackageProvider.hpp"

DeziBotPackageProvider::DeziBotPackageProvider(repast::SharedContext<DeziBot>* agentPtr) : agents(agentPtr) {}

void DeziBotPackageProvider::providePackage(DeziBot* agent, std::vector<DeziBotPackage>& out) {
    repast::AgentId id = agent->getId();
    DeziBotPackage package(
        id.id(),
        id.startingRank(),
        id.agentType(),
        id.currentRank(),
        agent->getSpeed(),
        agent->getVelocity(),
        agent->getInfectionState(),
        agent->getKnowsAboutInfection(),
        agent->getExposedTimer(),
        agent->getInfectiousTimer(),
        agent->getRealizationTimer(),
        agent->getResourceSearchState(),
        *(agent->getAlreadyCollectedResources()),
        agent->getResourceWaitAfterCollectionTimer(),
        agent->getLengthLeftToDraw(),
        agent->getMovingOutOfWarningArea(),
        agent->getIsFollowingPhantomPheromone(),
        agent->getIsFollowingWarnPheromone(),
        agent->getFoundResourceViaIndicator());
    out.push_back(package);
}

void DeziBotPackageProvider::provideContent(repast::AgentRequest req, std::vector<DeziBotPackage>& out) {
    std::vector<repast::AgentId> ids = req.requestedAgents();
    for (size_t i = 0; i < ids.size(); i++) {
        providePackage(agents->getAgent(ids[i]), out);
    }
}