/* DeziBotPackageReceiver.cpp */

#include <vector>

#include "base/Vector2D.hpp"
#include "base/SimulationArena.hpp"
#include "dezi/DeziBotPackageReceiver.hpp"
#include "dezi/DeziBot.hpp"
#include "resource/ResourceSpace.hpp"
#include "SimulationProperties.hpp"

DeziBotPackageReceiver::DeziBotPackageReceiver(
    repast::SharedContext<DeziBot>* agentPtr,
    SimulationArena* _space,
    AgentProperties* _agentProperties) :
    agents(agentPtr),
    space(_space),
    agentProperties(_agentProperties) {
}

DeziBot* DeziBotPackageReceiver::createAgent(DeziBotPackage package) {
    repast::AgentId id(package.id, package.rank, package.type, package.currentRank);
    Vector2D velocity{ package.velocityX, package.velocityY };
    return new DeziBot(
        id,
        space,
        agentProperties,
        package.speed,
        velocity,
        package.infectionState,
        package.knowsAboutInfection,
        package.exposedTimer,
        package.infectiousTimer,
        package.realizationTimer,
        package.resourceSearchState,
        package.lengthLeftToDraw,
        package.movingOutOfWarningArea,
        boost::make_shared<DeziBot::ResourceList>(package.alreadyCollectedResources),
        package.resourceWaitAfterCollectionTimer,
        package.isFollowingPhantomPheromone,
        package.isFollowingWarnPheromone,
        package.foundResourceViaIndicator
    );
}

void DeziBotPackageReceiver::updateAgent(DeziBotPackage package) {
    repast::AgentId id(package.id, package.rank, package.type);
    DeziBot* agent = agents->getAgent(id);
    agent->Agent::set(package.currentRank);
    Vector2D velocity{ package.velocityX, package.velocityY };
    agent->updateAttributes(
        package.speed,
        velocity,
        package.infectionState,
        package.knowsAboutInfection,
        package.exposedTimer,
        package.infectiousTimer,
        package.realizationTimer,
        package.resourceSearchState,
        boost::make_shared<DeziBot::ResourceList>(package.alreadyCollectedResources),
        package.resourceWaitAfterCollectionTimer,
        package.lengthLeftToDraw,
        package.movingOutOfWarningArea,
        package.isFollowingPhantomPheromone,
        package.isFollowingWarnPheromone,
        package.foundResourceViaIndicator
    );
}