/* DataSources.cpp */

#include "log/DataSources.hpp"

/**
 * 
 *	DeziBot Data Sources 
 * 
*/

template <typename AgentType, typename ValueType>
ValueType DataSource_Agent<AgentType, ValueType>::getData() {
    AgentType* agent = context->getAgent(*id);
    // currenRank check is needed, because getAgent only looks for the first three id attributes
    if(agent != nullptr && (*agent).getId().currentRank() == id->currentRank()){
        return getValue(agent);
    } else {
        return zero();
    }
}

DataSource_DeziBot_X::DataSource_DeziBot_X(repast::SharedContext<DeziBot>* _context, repast::AgentId* _id){
    context = _context;
    id = _id;
}

double DataSource_DeziBot_X::getValue(DeziBot* bot) {
    return bot->getPosition().getX();
}

DataSource_DeziBot_Y::DataSource_DeziBot_Y(repast::SharedContext<DeziBot>* _context, repast::AgentId* _id){
    context = _context;
    id = _id;
}

double DataSource_DeziBot_Y::getValue(DeziBot* bot) {
    return bot->getPosition().getY();
}

DataSource_DeziBot_InfectionState::DataSource_DeziBot_InfectionState(repast::SharedContext<DeziBot>* _context, repast::AgentId* _id){
    context = _context;
    id = _id;
}

int DataSource_DeziBot_InfectionState::getValue(DeziBot* bot){
    return bot->getInfectionState();
}

DataSource_DeziBot_ResourceSearchState::DataSource_DeziBot_ResourceSearchState(repast::SharedContext<DeziBot>* _context, repast::AgentId* _id){
    context = _context;
    id = _id;
}

int DataSource_DeziBot_ResourceSearchState::getValue(DeziBot* bot) {
    return bot->getResourceSearchState();
}

/**
 * 
 *	Infection Simulation Data Sources 
 * 
*/

DataSource_InfectionState_Sum::DataSource_InfectionState_Sum(SEIRDataCollector* _collector, SEIRState _state){
    collector = _collector;
    state = _state;
}

int DataSource_InfectionState_Sum::getData(){
    return collector->getSEIRStateSum(state);
}

/**
 * 
 *	Resource Data Sources 
 * 
*/

DataSource_Resource_X::DataSource_Resource_X(repast::SharedContext<Resource>* c, repast::AgentId* _id){
    context = c;
    id = _id;
}

double DataSource_Resource_X::getValue(Resource* resource){
    return resource->getPosition().getX();
}

DataSource_Resource_Y::DataSource_Resource_Y(repast::SharedContext<Resource>* c, repast::AgentId* _id){
    context = c;
    id = _id;
}

double DataSource_Resource_Y::getValue(Resource* resource){
    return resource->getPosition().getY();
}

DataSource_Resource_Size::DataSource_Resource_Size(repast::SharedContext<Resource>* c, repast::AgentId* _id){
    context = c;
    id = _id;
}

int DataSource_Resource_Size::getValue(Resource* resource){
    return resource->getSize();
}

DataSource_ResourceSum::DataSource_ResourceSum(ResourceSpace* _resourceSpace){
    resourceSpace = _resourceSpace;
}

int DataSource_ResourceSum::getData(){
    return resourceSpace->getCollectedResourcesTotal();
}

/**
 * 
 *	Pheromone Data Sources 
 * 
*/

DataSource_PheromoneCell::DataSource_PheromoneCell(IPheromoneMap* _map, int _x, int _y, repast::ScheduleRunner* _runner){
    map = _map;
    x = _x;
    y = _y;
    runner = _runner;
}

double DataSource_PheromoneCell::getData(){
    return map->getIntensity(x, y, runner->currentTick());
}