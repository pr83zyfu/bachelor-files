/* Logger.cpp */

#include <vector>

#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SVDataSet.h"
#include "repast_hpc/SVDataSetBuilder.h"


#include "dezi/DeziBot.hpp"
#include "log/Logger.hpp"
#include "log/DataSources.hpp"
#include "log/SEIRDataCollector.hpp"
#include "pheromone/IPheromoneMap.hpp"

AgentStateLogger::AgentStateLogger(repast::SharedContext<DeziBot>* _context, std::string outputDirectory, std::string outputFilePrefix, int totalAgents, int rank) : Logger(){
	context = _context;

    for (int i = 0; i < totalAgents; i ++) {
		std::string fileOutputName(outputDirectory + "/" + outputFilePrefix + std::to_string(i) + ".csv");
		repast::SVDataSetBuilder builder(fileOutputName.c_str(), ",", repast::RepastProcess::instance()->getScheduleRunner().schedule());

		repast::AgentId* id = new repast::AgentId(i, 0, 0, rank);

		// Create the individual data sets to be added to the builder
		DataSource_DeziBot_X* agentX = new DataSource_DeziBot_X(context, id);
		builder.addDataSource(repast::createSVDataSource("x", agentX, std::plus<double>()));

		DataSource_DeziBot_Y* agentY = new DataSource_DeziBot_Y(context, id);
		builder.addDataSource(repast::createSVDataSource("y", agentY, std::plus<double>()));

		DataSource_DeziBot_InfectionState* agentInfection = new DataSource_DeziBot_InfectionState(context, id);
		builder.addDataSource(repast::createSVDataSource("infection state", agentInfection, std::plus<int>()));

		dataSets.push_back(builder.createDataSet());
	}
}

AgentStateLogger::~AgentStateLogger(){
    for(auto it = std::begin(dataSets); it != std::end(dataSets); ++it) {
		delete *it;
	}
}

void AgentStateLogger::record() {
    for(auto it = std::begin(dataSets); it != std::end(dataSets); ++it) {
		(*it)->record();
	}
}

void AgentStateLogger::write() {
    for(auto it = std::begin(dataSets); it != std::end(dataSets); ++it) {
		(*it)->write();
	}
}

CondensedAgentStateLogger::CondensedAgentStateLogger(repast::SharedContext<DeziBot>* _context, std::string outputDirectory, std::string outputFile , int totalAgents, int rank){
	context = _context;
	std::string fileOutputName(outputDirectory + "/" + outputFile);
	repast::SVDataSetBuilder builder(fileOutputName.c_str(), ",", repast::RepastProcess::instance()->getScheduleRunner().schedule());
	
	for (size_t i = 0; i < totalAgents; i ++) {
		repast::AgentId* id = new repast::AgentId(i, 0, 0, rank);

		DataSource_DeziBot_X* agentX = new DataSource_DeziBot_X(context, id);
		builder.addDataSource(repast::createSVDataSource("x_" + std::to_string(i), agentX, std::plus<double>()));

		DataSource_DeziBot_Y* agentY = new DataSource_DeziBot_Y(context, id);
		builder.addDataSource(repast::createSVDataSource("y_" + std::to_string(i), agentY, std::plus<double>()));

		DataSource_DeziBot_InfectionState* agentInfection = new DataSource_DeziBot_InfectionState(context, id);
		builder.addDataSource(repast::createSVDataSource("infection_state_" + std::to_string(i), agentInfection, std::plus<int>()));

		DataSource_DeziBot_ResourceSearchState* agentResourceSearchState = new DataSource_DeziBot_ResourceSearchState(context, id);
		builder.addDataSource(repast::createSVDataSource("resource_search_state_" + std::to_string(i), agentResourceSearchState, std::plus<int>()));
	}

	dataSet = builder.createDataSet();
}

CondensedAgentStateLogger::~CondensedAgentStateLogger(){
	delete dataSet;
}

void CondensedAgentStateLogger::record(){
	dataSet->record();
}

void CondensedAgentStateLogger::write(){
	dataSet->write();
}

InfectionSumLogger::InfectionSumLogger(repast::SharedContext<DeziBot>* _context, std::string outputDirectory, std::string outputFile) : Logger(){
	context = _context;

    collector = new SEIRDataCollector(context);

	std::string outputName(outputDirectory + "/" + outputFile);
	repast::SVDataSetBuilder builder(outputName.c_str(), ",", repast::RepastProcess::instance()->getScheduleRunner().schedule());

	DataSource_InfectionState_Sum* sumS = new DataSource_InfectionState_Sum(collector, SUSCEPTIBLE);
	builder.addDataSource(repast::createSVDataSource("susceptible", sumS, std::plus<int>()));

	DataSource_InfectionState_Sum* sumE = new DataSource_InfectionState_Sum(collector, EXPOSED);
	builder.addDataSource(repast::createSVDataSource("exposed", sumE, std::plus<int>()));

	DataSource_InfectionState_Sum* sumI = new DataSource_InfectionState_Sum(collector, INFECTIOUS);
	builder.addDataSource(repast::createSVDataSource("infectious", sumI, std::plus<int>()));

	DataSource_InfectionState_Sum* sumR = new DataSource_InfectionState_Sum(collector, RECOVERED);
	builder.addDataSource(repast::createSVDataSource("recovered", sumR, std::plus<int>()));

	dataSet = builder.createDataSet();
}

InfectionSumLogger::~InfectionSumLogger(){
    delete collector;
    delete dataSet;
}

void InfectionSumLogger::record(){
    collector->update();
    dataSet->record();
}

void InfectionSumLogger::write(){
    dataSet->write();
}

CondensedResourceStateLogger::CondensedResourceStateLogger(repast::SharedContext<Resource>* _context, std::string outputDirectory, std::string outputFile, int totalResources, int rank){
	context = _context;
	std::string fileOutputName(outputDirectory + "/" + outputFile);
	repast::SVDataSetBuilder builder(fileOutputName.c_str(), ",", repast::RepastProcess::instance()->getScheduleRunner().schedule());
	
	for (size_t i = 0; i < totalResources; i++) {
		repast::AgentId* id = new repast::AgentId(i, 0, 1, rank);

		DataSource_Resource_X* resourceX = new DataSource_Resource_X(context, id);
		builder.addDataSource(repast::createSVDataSource("x_" + std::to_string(i), resourceX, std::plus<double>()));

		DataSource_Resource_Y* resourceY = new DataSource_Resource_Y(context, id);
		builder.addDataSource(repast::createSVDataSource("y_" + std::to_string(i), resourceY, std::plus<double>()));

		DataSource_Resource_Size* resourceSize = new DataSource_Resource_Size(context, id);
		builder.addDataSource(repast::createSVDataSource("size_" + std::to_string(i), resourceSize, std::plus<int>()));
	}

	dataSet = builder.createDataSet();
}

CondensedResourceStateLogger::~CondensedResourceStateLogger(){
	delete dataSet;
}

void CondensedResourceStateLogger::record(){
	dataSet->record();
}

void CondensedResourceStateLogger::write(){
	dataSet->write();
}

ResourceCollectionLogger::ResourceCollectionLogger(
	ResourceSpace* _resourceSpace,
	std::string outputDirectory,
	std::string outputFile
){
	resourceSpace = _resourceSpace;

	std::string outputName(outputDirectory + "/" + outputFile);
	repast::SVDataSetBuilder builder(outputName.c_str(), ",", repast::RepastProcess::instance()->getScheduleRunner().schedule());

	DataSource_ResourceSum* dataSource = new DataSource_ResourceSum(resourceSpace);
	builder.addDataSource(repast::createSVDataSource("total", dataSource, std::plus<int>()));

	dataSet = builder.createDataSet();
}

ResourceCollectionLogger::~ResourceCollectionLogger(){
	delete dataSet;
}

void ResourceCollectionLogger::record(){
	dataSet->record();
}

void ResourceCollectionLogger::write(){
	dataSet->write();
}

PheromoneLogger::PheromoneLogger(
	IPheromoneMap* _pheromoneMap, 
	repast::Point<double>* _origin, 
	repast::Point<double>* _maxPoint,
	std::string outputDirectory,
	std::string outputFile,
	repast::ScheduleRunner* _runner
	){
	std::string outputName(outputDirectory + "/" + outputFile);
	repast::SVDataSetBuilder builder(outputName.c_str(), ",", repast::RepastProcess::instance()->getScheduleRunner().schedule());

	for (int x = (int) round(_origin->getX()); x <= (int) round(_maxPoint->getX()); x++){
		for (int y = (int) round(_origin->getY()); y <= (int) round(_maxPoint->getY()); y++){
			DataSource_PheromoneCell* dataSource = new DataSource_PheromoneCell(_pheromoneMap, x, y, _runner);
			builder.addDataSource(repast::createSVDataSource(std::to_string(x) + "_" + std::to_string(y), dataSource, MaxStruct<double>()));
		}
	}

	dataSet = builder.createDataSet();
}

PheromoneLogger::~PheromoneLogger(){
	delete dataSet;
}

void PheromoneLogger::record(){
	dataSet->record();
}

void PheromoneLogger::write(){
	dataSet->write();
}