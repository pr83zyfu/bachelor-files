/* SEIRDataCollector.cpp */

#include <map>

#include "repast_hpc/SharedContext.h"

#include "log/SEIRDataCollector.hpp"
#include "dezi/DeziBot.hpp"

SEIRDataCollector::SEIRDataCollector(repast::SharedContext<DeziBot>* _context){
    context = _context;

    // initialize sums as 0
    for (size_t i = 0; i < states.size(); i++){
        sums[states[i]] = 0;
    }
}

void SEIRDataCollector::update(){
    // initialize sums as 0
    for (size_t i = 0; i < states.size(); i++){
        sums[states[i]] = 0;
    }

    // sum up all states of local bots
    auto iter = context->localBegin();
	auto iterEnd = context->localEnd();

	while (iter != iterEnd){
        SEIRState botState = (*iter)->getInfectionState();
        sums[botState] = sums[botState] + 1;

		iter++;
	}
}

int SEIRDataCollector::getSEIRStateSum(SEIRState state){
    return sums[state];
}
