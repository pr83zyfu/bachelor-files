/* EfficientPheromoneMap.cpp */

#include "pheromone/EfficientPheromoneMap.hpp"

int EfficientPheromoneMap::makeDiscrete(double continuousValue) {
    return std::floor(continuousValue);
}

int EfficientPheromoneMap::offsetXValue(int x) {
    return origin->getX() + x;
}

int EfficientPheromoneMap::offsetYValue(int y) {
    return origin->getY() + y;
}

int EfficientPheromoneMap::removeXOffset(int x) {
    return x - origin->getX();
}

int EfficientPheromoneMap::removeYOffset(int y) {
    return y - origin->getY();
}

EfficientPheromoneMap::EfficientPheromoneMap(
    repast::Point<double>* _origin,
    repast::Point<double>* _extent,
    repast::Point<double>* _maxPoint,
    double _pheromoneLifetime,
    boost::mpi::communicator* _comm
) {
    origin = _origin;
    extent = _extent;
    maxPoint = _maxPoint;
    pheromoneLifetime = _pheromoneLifetime;
    comm = _comm;

    // initialize shared value fields
    intensities = new SharedValueField<double>(makeDiscrete(extent->getX()), makeDiscrete(extent->getY()), comm);
    timeStamps = new SharedValueField<double>(makeDiscrete(extent->getX()), makeDiscrete(extent->getY()), comm);
}

EfficientPheromoneMap::~EfficientPheromoneMap() {
    delete intensities;
    delete timeStamps;
}

double EfficientPheromoneMap::getIntensity(int x, int y, double currentTick) {
    // guard clause for out of bounds
    if (x < origin->getX() || maxPoint->getX() <= x || y < origin->getY() || maxPoint->getY() <= y) {
        return 0.0;
    }

    int normX = removeXOffset(x);
    int normY = removeYOffset(y);

    // guard clause for dead/dissepated pheromone
    if (currentTick - timeStamps->getValue(normX, normY) > pheromoneLifetime) {
        return 0.0;
    }

    return intensities->getValue(normX, normY);
}

double EfficientPheromoneMap::getIntensity(const repast::Point<int>& point, double currentTick) {
    return getIntensity(point.getX(), point.getY(), currentTick);
}

double EfficientPheromoneMap::getIntensity(const repast::Point<double>& point, double currentTick) {
    return getIntensity(makeDiscrete(point.getX()), makeDiscrete(point.getY()), currentTick);
}

void EfficientPheromoneMap::drawPoint(int x, int y, double intensity, double currentTick) {
    // guard clause for out of bounds
    if (x < origin->getX() || maxPoint->getX() <= x || y < origin->getY() || maxPoint->getY() <= y) {
        return;
    }

    int normX = removeXOffset(x);
    int normY = removeYOffset(y);

    intensities->initializeValueChange(normX, normY, intensity);
    timeStamps->initializeValueChange(normX, normY, currentTick);
}

void EfficientPheromoneMap::drawPoint(const repast::Point<int>& point, double intensity, double currentTick) {
    return drawPoint(point.getX(), point.getY(), intensity, currentTick);
}

void EfficientPheromoneMap::drawPoint(const repast::Point<double>& point, double intensity, double currentTick) {
    return drawPoint(makeDiscrete(point.getX()), makeDiscrete(point.getY()), intensity, currentTick);
}

void EfficientPheromoneMap::drawCircle(int x, int y, double radius, double intensity, double currentTick) {
    // stupid version: 
    double radiusSq = radius * radius;

    int range = (int)ceil(radius);
    for (int xIter = x - range; xIter <= x + range; xIter++) {
        for (int yIter = y - range; yIter <= y + range; yIter++) {
            int xOffset = xIter - x;
            int yOffset = yIter - y;
            if ((xOffset * xOffset) + (yOffset * yOffset) <= radiusSq) {
                drawPoint(xIter, yIter, intensity, currentTick);
            }
        }
    }
}

void EfficientPheromoneMap::drawCircle(const repast::Point<int>& center, double radius, double intensity, double currentTick) {
    return drawCircle(center.getX(), center.getY(), radius, intensity, currentTick);
}

void EfficientPheromoneMap::drawCircle(const repast::Point<double>& center, double radius, double intensity, double currentTick) {
    return drawCircle(makeDiscrete(center.getX()), makeDiscrete(center.getY()), radius, intensity, currentTick);
}

void EfficientPheromoneMap::drawRectangleInFrontOfPointWithDirection(int x, int y, Vector2D direction, int width, int height, double intensity, double currentTick) {
    Vector2D normalisedForward = Vector2D();
    Vector2D normalisedRight = Vector2D();
    normalisedForward.setCopy(direction);
    normalisedForward.normalise();
    normalisedRight.setCopy(normalisedForward);
    normalisedRight.rotate(M_PI_2);

    double halfWidth = ((double)width) / 2.0;
    // -1 because in x-loop cur is first moved and then the point is drawn
    Vector2D leftPoint = Vector2D(
        x - (normalisedRight.getX() * halfWidth) - 1.0,
        y - (normalisedRight.getY() * halfWidth) - 1.0);

    for (int y = 0; y < height; y++) {
        // move a distance of 1 along velocity vector
        leftPoint.setAdd(normalisedForward);
        Vector2D cur = Vector2D(leftPoint);

        for (int x = 0; x < width; x++) {
            cur.setAdd(normalisedRight);
            drawPoint(cur.getX(), cur.getY(), intensity, currentTick);
        }
    }
}

void EfficientPheromoneMap::drawRectangleInFrontOfPointWithDirection(const repast::Point<double>& point, Vector2D direction, int width, int height, double intensity, double currentTick) {
    drawRectangleInFrontOfPointWithDirection(point.getX(), point.getY(), direction, width, height, intensity, currentTick);
}

void EfficientPheromoneMap::drawRectangleBehindPointWithDirection(const repast::Point<double>& point, Vector2D direction, int width, int height, double intensity, double currentTick) {
    direction.scale(-1.0);
    drawRectangleInFrontOfPointWithDirection(point.getX(), point.getY(), direction, width, height, intensity, currentTick);
}

void EfficientPheromoneMap::synchronize() {
    intensities->update();
    timeStamps->update();
}