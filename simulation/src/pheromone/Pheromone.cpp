/* Pheromone.cpp */

#include "pheromone/Pheromone.hpp"

Pheromone::Pheromone(repast::AgentId _id, double _intensity, double _timeStamp){
    id = _id;
    intensity = _intensity;
    timeStamp = _timeStamp;
}

Pheromone::~Pheromone(){
    
}

void Pheromone::updateAttributes(double _intensity, double _timeStamp){
    intensity = _intensity;
    timeStamp = _timeStamp;
}
