/* PheromoneMap.cpp */

#include "pheromone/PheromoneMap.hpp"

PheromoneMap::PheromoneMap(
    int _rank,
    boost::mpi::communicator* _comm,
    repast::Point<double>* _origin,
    repast::Point<double>* _extent,
    repast::Point<double>* _maxPoint,
    double _minimalBuffer,
    std::string _spaceName,
    double _pheromoneLifetime
){
    rank = _rank;
    idCounter = 0;
    pheromoneLifetime = _pheromoneLifetime;
    origin = _origin;
    extent = _extent;
    maxPoint = _maxPoint;

    context = new repast::SharedContext<Pheromone>(_comm);

    repast::GridDimensions gd(*origin, *extent);
    std::vector<int> processDims;
    processDims.push_back(2);
    processDims.push_back(2);

    space = new repast::SharedDiscreteSpace<Pheromone, repast::StrictBorders, repast::SimpleAdder<Pheromone>>(
        _spaceName,
        gd,
        processDims,
        _minimalBuffer,
        _comm
    );

    context->addProjection(space);

    provider = new PheromonePackageProvider(context);
    receiver = new PheromonePackageReceiver(context);
}

PheromoneMap::~PheromoneMap(){
    delete context;
    delete provider;
    delete receiver;
}

double PheromoneMap::getIntensity(int x, int y, double currentTick){
    repast::Point<int> point(x, y);
    return getIntensity(point, currentTick);
}

double PheromoneMap::getIntensity(const repast::Point<int> &point, double currentTick){
    std::vector<Pheromone*> pheromones = std::vector<Pheromone*>();
    space->getObjectsAt(point, pheromones);

    std::vector<repast::AgentId> toRemove = std::vector<repast::AgentId>();
    double intensity = 0.0;
    double maxTimeStamp = 0;

    for (Pheromone* pheromone : pheromones) {
        if (currentTick - pheromone->getTimeStamp() > pheromoneLifetime){
            toRemove.push_back(pheromone->getId());
        } else if (pheromone->getTimeStamp() > maxTimeStamp){
            // Pheromone is still "alive" and the most recent pheromone found yet
            intensity = pheromone->getIntensity();
            maxTimeStamp = pheromone->getTimeStamp();
        } 
    }

    for (repast::AgentId id : toRemove) {
        // check whether or not the pheromone is local
        if (id.currentRank() == rank){
            repast::RepastProcess::instance()->agentRemoved(id);
            context->removeAgent(id);
        }
    }

    return intensity;
}

double PheromoneMap::getIntensity(const repast::Point<double> &point, double currentTick){
    repast::Point<int> discretePoint((int) std::floor(point.getX()), (int) std::floor(point.getY()));
    return getIntensity(discretePoint, currentTick);
}

void PheromoneMap::drawPoint(int x, int y, double intensity, double currentTick){
    repast::Point<int> point(x, y);
    drawPoint(point, intensity, currentTick);
}

void PheromoneMap::drawPoint(const repast::Point<int> &point, double intensity, double currentTick){
    if (!(  origin->getX() <= point.getX() && point.getX() < maxPoint->getX()
        &&  origin->getY() <= point.getY() && point.getY() < maxPoint->getY())){
        // point is not within arena
        return;
    }

    // get already exisiting pheromone at the given position
    std::vector<Pheromone*> pheromones = std::vector<Pheromone*>();
    space->getObjectsAt(point, pheromones);

    // check for already existing pheromones and whether or not they are local (if one is local, all are)
    if (pheromones.size() > 0 && pheromones.at(0)->getId().currentRank() == rank) {
        // update first pheromone
        pheromones.at(0)->updateAttributes(intensity, currentTick);

        // remove all pheromones except for first
        std::vector<repast::AgentId> toRemove = std::vector<repast::AgentId>();
        for (int i = 1; i < pheromones.size(); i++) {
            toRemove.push_back(pheromones.at(i)->getId());
        }

        for (repast::AgentId id : toRemove){
            repast::RepastProcess::instance()->agentRemoved(id);
            context->removeAgent(id);
        }
    } else {
        // create new pheromone
        // rank is also used as type (with an offset to avoid conflicts with other agent contexts), 
        // to make sure all ids exist only once across processes (pheromone id counter would have to be synced otherwise)
        repast::AgentId id(idCounter, rank, 100 + rank, rank);
        idCounter++;

        Pheromone* pheromone = new Pheromone(id , intensity, currentTick);
        context->addAgent(pheromone);
        space->moveTo(id, point);
    }
}

void PheromoneMap::drawPoint(const repast::Point<double> &point, double intensity, double currentTick){
    repast::Point<int> discretePoint((int) std::floor(point.getX()), (int) std::floor(point.getY()));
    drawPoint(discretePoint, intensity, currentTick);
}

void PheromoneMap::drawCircle(int x, int y, double radius, double intensity, double currentTick){
    // stupid version: 
    double radiusSq = radius * radius;

    int range = (int) ceil(radius);
    for (int xIter = x - range; xIter <= x + range; xIter++){
        for (int yIter = y - range; yIter <= y + range; yIter++){
            int xOffset = xIter - x;
            int yOffset = yIter - y;
            if ((xOffset * xOffset) + (yOffset * yOffset) <= radiusSq){
                // check that values are within bounds
                if (origin->getX() <= xIter && xIter <= maxPoint->getX()
                    && origin->getY() <= yIter && yIter <= maxPoint->getY()){
                    drawPoint(xIter, yIter, intensity, currentTick);
                }
            }
        }
    }
}

void PheromoneMap::drawCircle(const repast::Point<int> &center, double radius, double intensity, double currentTick){
    drawCircle(center.getX(), center.getY(), radius, intensity, currentTick);
}

void PheromoneMap::drawCircle(const repast::Point<double> &center, double radius, double intensity, double currentTick){
    drawCircle((int) std::floor(center.getX()), (int) std::floor(center.getY()), radius, intensity, currentTick);
}

void PheromoneMap::synchronize(){
    space->balance();
    repast::RepastProcess::instance()->synchronizeAgentStatus<Pheromone, PheromonePackage, PheromonePackageProvider, PheromonePackageReceiver>(*context, *provider, *receiver, *receiver);
    repast::RepastProcess::instance()->synchronizeProjectionInfo<Pheromone, PheromonePackage, PheromonePackageProvider, PheromonePackageReceiver>(*context, *provider, *receiver, *receiver);
	repast::RepastProcess::instance()->synchronizeAgentStates<PheromonePackage, PheromonePackageProvider, PheromonePackageReceiver>(*provider, *receiver);
}