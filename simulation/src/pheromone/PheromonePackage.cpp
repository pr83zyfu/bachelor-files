/* PheromonePackage.cpp */

#include "pheromone/PheromonePackage.hpp"

PheromonePackage::PheromonePackage(){

}

PheromonePackage::PheromonePackage(
    int _id,
    int _rank,
    int _type,
    int _currentRank,
    double _intensity,
    double _timeStamp
){
    id = _id;
    rank = _rank;
    type = _type;
    currentRank = _currentRank;
    intensity = _intensity;
    timeStamp = _timeStamp;
}