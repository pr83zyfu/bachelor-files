/* PheromonePackageProvider.cpp */

#include "pheromone/PheromonePackage.hpp"
#include "pheromone/PheromonePackageProvider.hpp"

PheromonePackageProvider::PheromonePackageProvider(repast::SharedContext<Pheromone>* _context){
    context = _context;
}

void PheromonePackageProvider::providePackage(Pheromone *pheromone, std::vector<PheromonePackage> &out){
    repast::AgentId id = pheromone->getId();

    PheromonePackage package(
        id.id(),
        id.startingRank(),
        id.agentType(),
        id.currentRank(),
        pheromone->getIntensity(),
        pheromone->getTimeStamp()
    );

    out.push_back(package);
}

void PheromonePackageProvider::provideContent(repast::AgentRequest req, std::vector<PheromonePackage> &out){
    std::vector<repast::AgentId> ids = req.requestedAgents();
    for (size_t i = 0; i < ids.size(); i++){
        providePackage(context->getAgent(ids[i]), out);
    }
}