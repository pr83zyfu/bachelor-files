/* PheromonePackageReceiver.cpp */

#include "pheromone/PheromonePackageReceiver.hpp"

PheromonePackageReceiver::PheromonePackageReceiver(repast::SharedContext<Pheromone>* _context){
    context = _context;
}

Pheromone* PheromonePackageReceiver::createAgent(PheromonePackage package){
    repast::AgentId id(package.id, package.rank, package.type, package.currentRank);
    return new Pheromone(
        id,
        package.intensity,
        package.timeStamp
    );
}

void PheromonePackageReceiver::updateAgent(PheromonePackage package){
    repast::AgentId id(package.id, package.rank, package.type);
    Pheromone* pheromone = context->getAgent(id);
    pheromone->setCurrentRank(package.currentRank);
    pheromone->updateAttributes(
        package.intensity,
        package.timeStamp
    );
}