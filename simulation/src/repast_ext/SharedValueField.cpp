/* SharedValueField.cpp */

#include "repast_ext/SharedValueField.hpp"

template <typename Value>
int SharedValueField<Value>::getIndex(int x, int y){
    return (y * width) + x;
}

template <typename Value>
SharedValueField<Value>::SharedValueField(int _width, int _height, boost::mpi::communicator* _comm){
    width = _width;
    height = _height;
    vector = new SharedValueVector<Value>(_width * _height, _comm);
}

template <typename Value>        
SharedValueField<Value>::~SharedValueField(){
    delete vector;
}

template <typename Value>   
void SharedValueField<Value>::initializeValueChange(int x, int y, Value value){
    return vector->initializeValueChange(getIndex(x, y), value);
}

template <typename Value>
Value SharedValueField<Value>::getValue(int x, int y){
    return vector->getValue(getIndex(x, y));
}

template <typename Value>
void SharedValueField<Value>::update(){
    vector->update();
}

/* Explicit instantiation */
template class SharedValueField<double>;
//template class SharedValueField<int>; // TEMP