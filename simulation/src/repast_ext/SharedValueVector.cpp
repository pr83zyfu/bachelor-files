/* SharedValueVector.cpp */

#include "repast_ext/SharedValueVector.hpp"
#include <boost/mpi.hpp>


template <typename Value>
SharedValueVector<Value>::SharedValueVector(int _size, boost::mpi::communicator* _comm) {
    size = _size;
    comm = _comm;
    values = new std::vector<Value>(size);
    changes = new std::vector<ValueChangePackage<Value>>();
}

template <typename Value>
SharedValueVector<Value>::~SharedValueVector() {
    delete values;
    delete changes;
}

template <typename Value>
void SharedValueVector<Value>::initializeValueChange(int index, Value value) {
    changes->push_back(ValueChangePackage<Value>(index, value));
}

template <typename Value>
Value SharedValueVector<Value>::getValue(int index) {
    return values->at(index);
}  

template <typename Value>
void SharedValueVector<Value>::update() {
    int commRank = comm->rank();
    int commSize = comm->size();

    // communicate sizes of changes vector
    std::vector<int>* sizes = new std::vector<int>(commSize);
    sizes->at(commRank) = changes->size();
    for (int i = 0; i < commSize; i++) {
        boost::mpi::broadcast(*comm, sizes->at(i), i);
    }

    // communicate values
    auto valuesVectors = std::vector<std::vector<ValueChangePackage<Value>>*>(commSize);
    for (int i = 0; i < commSize; i++) {
        valuesVectors.at(i) = new std::vector<ValueChangePackage<Value>>(sizes->at(i));
    }
    valuesVectors.at(commRank) = changes;

    for (int i = 0; i < commSize; i++) {
        boost::mpi::broadcast(*comm, valuesVectors.at(i)->data(), sizes->at(i), i);
    }

    // execute changes in order of communicator rank
    for (int i = 0; i < commSize; i++) {
        for (int j = 0; j < valuesVectors.at(i)->size(); j++) {
            ValueChangePackage<Value> change = valuesVectors.at(i)->at(j);
            values->at(change.index) = change.value;
        }
    }

    changes->clear();
}

/* Explicit instantiation */
template class SharedValueVector<double>;
//template class SharedValueVector<int>; //TEMP