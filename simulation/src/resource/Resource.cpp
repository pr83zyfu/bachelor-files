/* Resource.cpp */

#include "repast_hpc/AgentId.h"
#include "repast_hpc/Point.h"

#include "resource/Resource.hpp"
#include "resource/ResourceSpace.hpp"

Resource::Resource(repast::AgentId _id, ResourceSpace* _space, int _size){
    id = _id;
    space = _space;
    size = _size;
}

repast::Point<double> Resource::getPosition(){
    return space->getLocation(id);
}

void Resource::updateAttributes(int _size){
    size = _size;
}