/* ResourcePackage.cpp */

#include "resource/ResourcePackage.hpp"

/* Serializable Resource package */

ResourcePackage::ResourcePackage(){}

ResourcePackage::ResourcePackage(int _id, int _rank, int _type, int _currentRank, int _size){
    id = _id;
    rank = _rank;
    type = _type;
    currentRank = _currentRank;
    size = _size;
}