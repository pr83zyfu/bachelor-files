/* ResourcePackageProvider.cpp */

#include "resource/ResourcePackage.hpp"
#include "resource/ResourcePackageProvider.hpp"

ResourcePackageProvider::ResourcePackageProvider(repast::SharedContext<Resource>* _context){
    context = _context;
}

void ResourcePackageProvider::providePackage(Resource * resource, std::vector<ResourcePackage>& out){
    repast::AgentId id = resource->getId();

    ResourcePackage package(
        id.id(),
        id.startingRank(),
        id.agentType(),
        id.currentRank(),
        resource->getSize()
    );

    out.push_back(package);
}

void ResourcePackageProvider::provideContent(repast::AgentRequest req, std::vector<ResourcePackage>& out){
    std::vector<repast::AgentId> ids = req.requestedAgents();
    for(size_t i = 0; i < ids.size(); i++){
        providePackage(context->getAgent(ids[i]), out);
    }
}