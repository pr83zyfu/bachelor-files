/* ResourcePackageReceiver.cpp */

#include "repast_hpc/SharedContext.h"

#include "resource/Resource.hpp"
#include "resource/ResourceSpace.hpp"
#include "resource/ResourcePackageReceiver.hpp"

ResourcePackageReceiver::ResourcePackageReceiver(repast::SharedContext<Resource>* _context, ResourceSpace* _space){
    context = _context;
    space = _space;
}

Resource * ResourcePackageReceiver::createAgent(ResourcePackage package){
    repast::AgentId id(package.id, package.rank, package.type, package.currentRank);
    return new Resource(
        id, 
        space,
        package.size
    );    
}

void ResourcePackageReceiver::updateAgent(ResourcePackage package){
    repast::AgentId id(package.id, package.rank, package.type);
    Resource* resource = context->getAgent(id);
    resource->setCurrentRank(package.currentRank);
    resource->updateAttributes(
        package.size
    );
}