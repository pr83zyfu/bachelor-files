/* ResourceSpace.cpp */

#include <vector>
#include <math.h>

#include <boost/algorithm/clamp.hpp>

#include "dezi/DeziBot.hpp"
#include "resource/Resource.hpp"
#include "resource/ResourcePackage.hpp"
#include "resource/ResourcePackageProvider.hpp"
#include "resource/ResourcePackageReceiver.hpp"
#include "resource/ResourceSpace.hpp"
#include "SimulationProperties.hpp"

#include "repast_hpc/Moore2DGridQuery.h"
#include "repast_hpc/Point.h"
#include "repast_hpc/GridDimensions.h"

ResourceSpace::ResourceSpace(repast::SharedContext<Resource>* _context, 
            boost::mpi::communicator* _comm, 
            repast::Point<double>* _origin, 
            repast::Point<double>* _extent, 
            repast::Point<double>* _maxPoint, 
            double minimalBuffer,
            int _rank){
    context = _context;
    origin = _origin;
    extent = _extent;
    maxPoint = _maxPoint;

    repast::GridDimensions gd(*origin, *extent);
    std::vector<int> processDims;
    processDims.push_back(2);
    processDims.push_back(2);

    space = new repast::SharedContinuousSpace<Resource, repast::StrictBorders, repast::SimpleAdder<Resource>>(
        "ResourceSpace", 
        gd, 
        processDims, 
        minimalBuffer, 
        _comm
    );

    context->addProjection(space);

    provider = new ResourcePackageProvider(context);
    receiver = new ResourcePackageReceiver(context, this);

    collectedResourceTotal = 0;
    rank = _rank;
}

ResourceSpace::~ResourceSpace(){
    delete provider;
    delete receiver;
}

repast::Point<double> ResourceSpace::getLocation(repast::AgentId id){
    std::vector<double> result;
    space->getLocation(id, result);
    return repast::Point<double>(result[0], result[1]);
}

void ResourceSpace::constructNewResource(repast::AgentId id, int size, repast::Point<double> &position){
    id.currentRank(rank);

    Resource* resource = new Resource(id, this, size);

    context->addAgent(resource);
    moveTo(id, position);
}

void ResourceSpace::constructRandomResource(repast::AgentId id, int minSize, int maxSize){
    double randomX = (repast::Random::instance()->nextDouble() * extent->getX()) - extent->getX()/2.0;
	double randomY = (repast::Random::instance()->nextDouble() * extent->getY()) - extent->getY()/2.0;
    repast::Point<double> position = repast::Point<double>(randomX, randomY);

    double randomSize = minSize + (repast::Random::instance()->nextDouble() * (maxSize - minSize));
        
    constructNewResource(id, randomSize, position);
}

void ResourceSpace::remove(repast::AgentId &id){
    repast::RepastProcess::instance()->agentRemoved(id);
    context->removeAgent(id);
}

void ResourceSpace::synchronize(){
    // Synchronization
	space->balance();
    repast::RepastProcess::instance()->synchronizeAgentStatus<Resource, ResourcePackage, ResourcePackageProvider, ResourcePackageReceiver>(*context, *provider, *receiver, *receiver);
    repast::RepastProcess::instance()->synchronizeProjectionInfo<Resource, ResourcePackage, ResourcePackageProvider, ResourcePackageReceiver>(*context, *provider, *receiver, *receiver);
	repast::RepastProcess::instance()->synchronizeAgentStates<ResourcePackage, ResourcePackageProvider, ResourcePackageReceiver>(*provider, *receiver);

}

void ResourceSpace::moveTo(repast::AgentId id, double x, double y){
    // clamp value
    repast::Point<double> newPosition = repast::Point<double>(
        boost::algorithm::clamp(x, (int) origin->getX(), (int) maxPoint->getX()),
        boost::algorithm::clamp(y, (int) origin->getY(), (int) maxPoint->getY())
    );
    space->moveTo(
        id,
        newPosition
    );
}

void ResourceSpace::moveTo(repast::AgentId id, repast::Point<double> &position){
    moveTo(id, position.getX(), position.getY());
}

void ResourceSpace::moveToRandomLocation(repast::AgentId id){
    double randomX = (repast::Random::instance()->nextDouble() * extent->getX()) - extent->getX()/2.0;
	double randomY = (repast::Random::instance()->nextDouble() * extent->getY()) - extent->getY()/2.0;
	moveTo(id, randomX, randomY);
}