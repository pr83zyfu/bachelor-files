import os
import re
import pandas as pd

AGENT_FILE_NAME_REGEX = "agent_[0-9]+.csv"

CONDENSED_AGENT_X_COLUMN_PREFIX = "x_"
CONDENSED_AGENT_Y_COLUMN_PREFIX = "y_"
CONDENSED_AGENT_SEIR_COLUMN_PREFIX = "infection_state_"


def load_bots_data(input_directory: str):
    result = dict()

    with os.scandir(input_directory) as files:
        for file in files:
            if re.match(AGENT_FILE_NAME_REGEX, file.name):
                index: int = int(file.name.split(".")[0].split("_")[1])

                result[index] = pd.read_csv(file, index_col="tick")                

    return result

def load_bots_data_condensed(input_file: str):
    result = dict()
    with open(input_file) as csv:
        base_dict = pd.read_csv(csv, index_col="tick")
        # Every bot has 3 columns (x, y, seir)
        num_of_bots, remainder = divmod((len(base_dict.keys())), 3)

        assert remainder == 0 # assert that the division left no remainder

        for i in range(num_of_bots):
            i_str = str(i)
            result[i] = dict()
            result[i]["x"] = base_dict[CONDENSED_AGENT_X_COLUMN_PREFIX + i_str]
            result[i]["y"] = base_dict[CONDENSED_AGENT_Y_COLUMN_PREFIX + i_str]
            result[i]["infection state"] = base_dict[CONDENSED_AGENT_SEIR_COLUMN_PREFIX + i_str]

    return result

def load_infection_sums(input_file: str):
    return pd.read_csv(input_file, index_col="tick")

def load_pheromone(input_file: str):
    result = dict()
    base_dict = pd.read_csv(input_file, index_col="tick", low_memory=False)
    for x in range(-250, 250):
        result[x] = dict()
        for y in range(-250, 250):
            result[x][y] = dict()
            for i in base_dict["0_0"].keys():
                result[x][y][i] = base_dict[str(x) + "_" + str(y)][i]
                
    return result