import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)


import data_loader as dl
import settings as st


def setup(xmax: int, ymax: int):
    fig, ax = plt.subplots()
    ax.set(xlim=(0, xmax), ylim=(0, ymax))

    ax.spines['bottom'].set_position('zero')
    ax.spines['left'].set_position('zero')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    #ax.spines["bottom"].set
    #ax.spines["left"].set_axisline_style("-|>")

    #ax.plot(1, 0, ">k", transform=ax.get_yaxis_transform(), clip_on=False)
    #ax.plot(0, 1, "^k", transform=ax.get_xaxis_transform(), clip_on=False)

    ax.set_xlabel("Simulation tick")
    ax.set_ylabel("Number of Robots in Group")

    ax.xaxis.set_major_locator(MultipleLocator(xmax / st.GRAPHS_X_TICK_SECTIONS))
    ax.yaxis.set_major_locator(MultipleLocator(ymax / st.GRAPHS_Y_TICK_SECTIONS))
    ax.xaxis.set_minor_locator(AutoMinorLocator(5.0))
    ax.yaxis.set_minor_locator(AutoMinorLocator(5.0))

    ax.grid(which='major', color='grey', linewidth=1, linestyle='-', alpha=0.4)
    ax.grid(which='minor', color='grey', linewidth=1, linestyle='-', alpha=0.1)

    return fig, ax


if __name__ == "__main__":
    infection_sums = dl.load_infection_sums(st.INPUT_DIRECTORY_PATH + "/infection_sums.csv")

    # total bots is calculated as the sum of the sizes of all infection groups in tick 0
    total_bots = sum([infection_sums[st.SEIR_SUMS_FILE_STATE_NAME[i]][0] for i in st.SEIR_SUMS_FILE_STATE_NAME.keys()])
    # max tick is retrieved by selecting the last element in the list of all ticks
    max_tick = infection_sums[st.SEIR_SUMS_FILE_STATE_NAME[0]].keys()[len(infection_sums[st.SEIR_SUMS_FILE_STATE_NAME[0]]) - 1]
    fig, ax = setup(max_tick, total_bots)

    tick_list = infection_sums[st.SEIR_SUMS_FILE_STATE_NAME[0]].keys()

    states_lists = dict()
    for i in st.SEIR_SUMS_FILE_STATE_NAME.keys():
        states_lists[i] = [n for n in infection_sums[st.SEIR_SUMS_FILE_STATE_NAME[i]]]                       
    
    draw_order = [0, 3, 1, 2]
    for j in states_lists:
        i = draw_order[j]
        ax.plot(
            tick_list, 
            states_lists[i], 
            color=st.SEIR_STATE_COLOUR[i], 
            label=st.SEIR_STATE_NAME[i],
            linestyle=st.SEIR_LINE_STYLE[i], 
            alpha=st.SEIR_LINE_OPACITY[i],
            linewidth=st.SEIR_LINE_WIDTH[i])
    
    ax.legend()
        
    plt.show()
        

