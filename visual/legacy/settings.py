INPUT_DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/simulation/output"
INPUT_TEST_DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/visual/legacy/input"
INPUT_SMALL_TEST_DIRECTORY_PATH =  "/home/philipp/Gits/bachelor-repast/visual/legacy/input/small"
OUTPUT_DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/visual/legacy/output"

# For video output (images an be exported as svg and dont need resolution)
OUTPUT_RESOLUTION_SCALE = 2.0

SEIR_STATE_NAME = {
    0: "Susceptible",
    1: "Exposed",
    2: "Infectious",
    3: "Recovered",
}

SEIR_SUMS_FILE_STATE_NAME = {
    0: "susceptible",
    1: "exposed",
    2: "infectious",
    3: "recovered"
}

SEIR_STATE_NAME_SHORT = {
    0: "S",
    1: "E",
    2: "I",
    3: "R",
}

SEIR_ORIGINAL_STATE_COLOUR = {
    0: "yellow",
    1: "orange",
    2: "red",
    3: "lime",
}

# alternative colours
SEIR_STATE_COLOUR = {
    0: "orange",
    1: "red",
    2: "red",
    3: "green",
}

SEIR_LINE_STYLE = {
    0: "-",
    1: ":",
    2: "-",
    3: "--",
}

SEIR_LINE_OPACITY = {
    0: 1.0,
    1: 0.5,
    2: 1.0,
    3: 1.0,
}

SEIR_LINE_WIDTH = {
    0: 2.0,
    1: 2.0,
    2: 2.0,
    3: 2.0,
}

SEIR_BOT_MARKER = {
    0: "o",
    1: "s",
    2: "^",
    3: "p",
}

# SEIR_BOT_MARKER_SIZE = {
#     0: 3.0,
#     1: 5.0,
#     2: 5.0,
#     3: 1.0,
# }

SEIR_BOT_MARKER_SIZE = {
    0: 3.0,
    1: 3.0,
    2: 3.0,
    3: 1.0,
}

SEIR_BOT_MARKER_EDGE_COLOUR = {
    0: "black",
    1: "black",
    2: "black",
    3: "black",
}

# Simulation Parameters
ARENA_WIDTH = 500.0
ARENA_HEIGHT = 500.0

# Plot Parameters
GRAPHS_X_TICK_SECTIONS = 10
GRAPHS_Y_TICK_SECTIONS = 10

ARENA_X_TICK_SECTIONS = 10
ARENA_Y_TICK_SECTIONS = 10

BOT_TAIL_LENGTH = 0
BOT_TAIL_COLOUR = "black"
BOT_TAIL_OPACITY = 0.5