import numpy as np
import math
import matplotlib.pyplot as plt
from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)
from matplotlib.animation import FuncAnimation
import matplotlib.animation as anim

import data_loader as dl
import warn_pheromone as wp
import settings as st

def setup_coordinate_plot():
     # Set up cartesian plot (https://pygmalion.nitri.org/cartesian-coordinates-with-matplotlib-1263.html)
    fig.patch.set_facecolor('#ffffff')

    ax.set(xlim=(xmin, xmax), ylim=(ymin, ymax), aspect='equal')

    ax.spines['bottom'].set_position(('outward', 10.0))
    ax.spines['left'].set_position(('outward', 10.0))
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    ax.set_xlabel('$x$', size=14, labelpad=-24, x=1.02)
    ax.set_ylabel('$y$', size=14, labelpad=-21, y=1.02, rotation=0)
    #plt.text(0.49, 0.49, r"$O$", ha='right', va='top', transform=ax.transAxes, horizontalalignment='center', fontsize=14)
    
    # Change grid lines to be shown every TICK_FREQUENCY steps in big and smaller every TICK_FREQUENCY/5 steps
    ax.xaxis.set_major_locator(MultipleLocator(st.ARENA_WIDTH / st.ARENA_X_TICK_SECTIONS))
    ax.yaxis.set_major_locator(MultipleLocator(st.ARENA_HEIGHT / st.ARENA_Y_TICK_SECTIONS))
    ax.xaxis.set_minor_locator(AutoMinorLocator(5.0))
    ax.yaxis.set_minor_locator(AutoMinorLocator(5.0))

    ax.grid(which='major', color='grey', linewidth=2, linestyle='-', alpha=0.3)
    ax.grid(which='minor', color='grey', linewidth=1, linestyle='-', alpha=0.1)

def animate(i):
    while len(ax.lines) > 0:
        ax.lines.pop(0)

    #ax.clear()
    #setup_coordinate_plot()

    current_tick = frame_to_tick_map[i]

    #wp.draw_warn_pheromone(ax, resource_indicator_pheromone, current_tick, round(xmin), round(xmax), round(ymin), round(ymax))
    wp.draw_warn_pheromone(ax, warn_pheromone, current_tick, round(xmin), round(xmax), round(ymin), round(ymax))

    # for every bot
    for k in bots_data:
        df = bots_data[k]

        # draw bot tail
        tail_x = []
        tail_y = []
        for l in range(st.BOT_TAIL_LENGTH):
            if current_tick - l < 1:
                break
            tail_x.append(df["x"][current_tick - l])
            tail_y.append(df["y"][current_tick - l])

        ax.plot(tail_x, tail_y, color=st.BOT_TAIL_COLOUR, alpha=st.BOT_TAIL_OPACITY)

        # draw bot current position in infection state colour
        x = [df["x"][current_tick]]
        y = [df["y"][current_tick]]
        seir_state = df["infection state"][current_tick]
        ax.plot(
            x, 
            y, 
            marker=st.SEIR_BOT_MARKER[seir_state], 
            markersize=st.SEIR_BOT_MARKER_SIZE[seir_state], 
            markerfacecolor=st.SEIR_STATE_COLOUR[seir_state], 
            markeredgecolor=st.SEIR_BOT_MARKER_EDGE_COLOUR[seir_state],
            linestyle='None'
        )
    

    # setup_legend(..) needs x and y coordinates that are outside of the plotted arena as inputs
    setup_legend(-1000.0, -1000.0)
    ax.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0)

    print("Plotted Frame: " + str(i) + " of " + str(total_frames) + " total frames")

def setup_legend(x_outside_view: float, y_outside_view: float):
    markers = list(st.SEIR_BOT_MARKER.keys())

    for j in st.SEIR_BOT_MARKER.keys():
        markers[j] = ax.plot(
            x_outside_view,
            y_outside_view,
            marker=st.SEIR_BOT_MARKER[j],
            markersize=st.SEIR_BOT_MARKER_SIZE[j],
            markerfacecolor=st.SEIR_STATE_COLOUR[j],
            markeredgecolor=st.SEIR_BOT_MARKER_EDGE_COLOUR[j],
            label=st.SEIR_STATE_NAME_SHORT[j],
            linestyle='None'
        )

    return markers


if __name__ == "__main__":
    fig, ax = plt.subplots()
    fig.set_dpi(fig.get_dpi() * st.OUTPUT_RESOLUTION_SCALE)

    xmin, xmax, ymin, ymax = -st.ARENA_WIDTH/2.0, st.ARENA_WIDTH/2.0, -st.ARENA_HEIGHT/2.0, st.ARENA_HEIGHT/2.0

    bots_data = dl.load_bots_data_condensed(st.INPUT_DIRECTORY_PATH + "/agents_condensed.csv")
    print("Loaded data of " + str(len(bots_data)) + " bots.")

    #resource_indicator_pheromone = dl.load_pheromone(st.INPUT_DIRECTORY_PATH + "/resource_indicator_pheromone.csv")
    warn_pheromone = dl.load_pheromone(st.INPUT_DIRECTORY_PATH + "/warning_pheromone.csv")

    first_key = list(bots_data.keys())[0]
    total_frames = len(bots_data[first_key]["x"].keys())

    # this assums even distribution of recorded ticks in input files
    frame_to_tick_map = bots_data[first_key]["x"].keys()

    setup_coordinate_plot()

    animation = FuncAnimation(fig, animate, frames=total_frames, interval=20, repeat=False)

    # saving to gif using FFMpeg writer
    writer = anim.FFMpegWriter(fps=60)
    animation.save(st.OUTPUT_DIRECTORY_PATH + "/animation.mp4", writer=writer)        

