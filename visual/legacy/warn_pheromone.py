import matplotlib.pyplot as plt
from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)
from matplotlib.animation import FuncAnimation
import matplotlib.animation as anim

import data_loader as dl
import settings as st

def setup_coordinate_plot():
     # Set up cartesian plot (https://pygmalion.nitri.org/cartesian-coordinates-with-matplotlib-1263.html)
    fig.patch.set_facecolor('#ffffff')

    xmin, xmax, ymin, ymax = -st.ARENA_WIDTH/2.0, st.ARENA_WIDTH/2.0, -st.ARENA_HEIGHT/2.0, st.ARENA_HEIGHT/2.0
    ax.set(xlim=(xmin, xmax), ylim=(ymin, ymax), aspect='equal')

    ax.spines['bottom'].set_position(('outward', 10.0))
    ax.spines['left'].set_position(('outward', 10.0))
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    ax.set_xlabel('$x$', size=14, labelpad=-24, x=1.02)
    ax.set_ylabel('$y$', size=14, labelpad=-21, y=1.02, rotation=0)
    #plt.text(0.49, 0.49, r"$O$", ha='right', va='top', transform=ax.transAxes, horizontalalignment='center', fontsize=14)
    
    # Change grid lines to be shown every TICK_FREQUENCY steps in big and smaller every TICK_FREQUENCY/5 steps
    ax.xaxis.set_major_locator(MultipleLocator(st.ARENA_WIDTH / st.ARENA_X_TICK_SECTIONS))
    ax.yaxis.set_major_locator(MultipleLocator(st.ARENA_HEIGHT / st.ARENA_Y_TICK_SECTIONS))
    ax.xaxis.set_minor_locator(AutoMinorLocator(5.0))
    ax.yaxis.set_minor_locator(AutoMinorLocator(5.0))

    ax.grid(which='major', color='grey', linewidth=2, linestyle='-', alpha=0.3)
    ax.grid(which='minor', color='grey', linewidth=1, linestyle='-', alpha=0.1)

def draw_warn_pheromone(ax, pheromone_map: dict, current_tick: int, xmin: int, xmax: int, ymin: int, ymax: int):
    # for every cell
    for x in range(xmin, xmax):
        for y in range(ymin, ymax):
            if pheromone_map[x][y][current_tick] > 0:
                ax.plot(x, y, marker="s", markersize=2, markerfacecolor="red", markeredgecolor="red", linestyle="None")

def animate(i):
    ax.clear()
    setup_coordinate_plot()

    current_tick = frame_to_tick_map[i]

    draw_warn_pheromone(ax, pheromone_map, current_tick, -50, 50, -50, 50)
    
    # setup_legend(..) needs x and y coordinates that are outside of the plotted arena as inputs
    # ax.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0)

    print("Plotted Frame: " + str(i) + " of " + str(total_frames) + " total frames")


if __name__ == "__main__":
    fig, ax = plt.subplots()
    pheromone_map = dl.load_pheromone(st.INPUT_DIRECTORY_PATH + "/warningPheromone.csv")

    fig.set_dpi(fig.get_dpi() * st.OUTPUT_RESOLUTION_SCALE)

    total_frames = len(pheromone_map[0][0].keys())

    # this assums even distribution of recorded ticks in input files
    frame_to_tick_map = [i for i in pheromone_map[0][0].keys()]

    animation = FuncAnimation(fig, animate, frames=total_frames, interval=20, repeat=False)

    # saving to gif using FFMpeg writer
    writer = anim.FFMpegWriter(fps=60)
    animation.save(st.OUTPUT_DIRECTORY_PATH + "/warnPheromone.mp4", writer=writer) 