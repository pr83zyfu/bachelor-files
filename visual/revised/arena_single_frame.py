import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import numpy as np


from src.settings import SEIR_DICT
import src.data_loader as dl

import src.arena as arena
import src.pheromone as pheromone
import src.resource as resource

ARENA_WIDTH = 100
ARENA_HEIGHT = 100
ARENA_X_TICK_SECTIONS = 10
ARENA_Y_TICK_SECTIONS = 10

BOT_TAIL_LENGTH = 0
BOT_TAIL_COLOUR = "black"
BOT_TAIL_OPACITY = 0.5

INFECTION_GRAPH_X_TICK_SECTIONS = 10
INFECTION_GRAPH_Y_TICK_SECTIONS = 10

RESOURCE_MARKER_STYLE = "H"
RESOURCE_MARKER_SIZE = 10.0
RESOURCE_MARKER_FACE_COLOUR = "lime"
RESOURCE_MARKER_EDGE_COLOUR = "black"
RESOURCE_MARKER_OPACITY = 0.5

RESOURCE_INDICATOR_COLOUR = "green"
WARN_PHEROMONE_COLOUR = "red"

INPUT_DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/simulation/output"
OUTPUT_DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/visual/revised/output"

OUTPUT_RESOLUTION_SCALE = 2.0


def setup():
    fig.patch.set_facecolor("#ffffff")
    arena.setup(
        axArena,
        xmin,
        ymin,
        xmax,
        ymax,
        ARENA_X_TICK_SECTIONS,
        ARENA_Y_TICK_SECTIONS,
    )


def animate(i):
    # clear arena plot by removing all lines
    axArena.lines.clear()

    current_tick = frame_to_tick_map[i]

    # Draw Arena
    if resource_indicator_pheromone is not None:
        pheromone.draw(
            axArena,
            resource_indicator_pheromone,
            current_tick,
            round(xmin),
            round(xmax),
            round(ymin),
            round(ymax),
            RESOURCE_INDICATOR_COLOUR,
            xlist,
            ylist,
            X,
            Y,
        )
    if warn_pheromone is not None:
        pheromone.draw(
            axArena,
            warn_pheromone,
            current_tick,
            round(xmin),
            round(xmax),
            round(ymin),
            round(ymax),
            WARN_PHEROMONE_COLOUR,
            xlist,
            ylist,
            X,
            Y,
        )
    if resource_data is not None:
        resource.draw_resources(
            axArena,
            current_tick,
            resource_data,
            RESOURCE_MARKER_STYLE,
            RESOURCE_MARKER_SIZE,
            RESOURCE_MARKER_FACE_COLOUR,
            RESOURCE_MARKER_EDGE_COLOUR,
            50.0,
        )
    arena.draw(
        axArena,
        current_tick,
        bots_data,
        xmin,
        ymin,
        xmax,
        ymax,
        SEIR_DICT,
        BOT_TAIL_LENGTH,
        BOT_TAIL_COLOUR,
        BOT_TAIL_OPACITY,
    )

    print("Plotted " + str(i) + " of " + str(total_frames) + " total frames.", end="\r")


if __name__ == "__main__":
    # matplotlib.use("pgf")
    # matplotlib.rcParams.update(
    #     {
    #         "pgf.texsystem": "pdflatex",
    #         "font.family": "serif",
    #         "text.usetex": True,
    #         "pgf.rcfonts": False,
    #     }
    # )

    # Load simulation properties
    xmin, xmax, ymin, ymax = (
        -ARENA_WIDTH / 2.0,
        ARENA_WIDTH / 2.0,
        -ARENA_HEIGHT / 2.0,
        ARENA_HEIGHT / 2.0,
    )
    # xmin = 37.5
    # xmax = 48.0
    # ymin = -8.0
    # ymax = 4.0
    xlist = np.arange(round(xmin), round(xmax) + 1, 1)
    ylist = np.arange(round(ymin), round(ymax) + 1, 1)
    X = [[x for y in ylist] for x in xlist]
    Y = [[y for y in ylist] for x in xlist]

    # Load simulation output
    bots_data = dl.load_bots_data_condensed(
        INPUT_DIRECTORY_PATH + "/agents_condensed.csv"
    )
    print("Loaded data of " + str(len(bots_data)) + " bots.")
    resource_data = dl.load_resource_states_condensed(
        INPUT_DIRECTORY_PATH + "/resources_condensed.csv"
    )
    # resource_indicator_pheromone    = dl.load_pheromone(INPUT_DIRECTORY_PATH + "/resource_indicator_pheromone.csv", round(xmin), round(ymin), round(xmax), round(ymax))
    warn_pheromone = dl.load_pheromone(
        INPUT_DIRECTORY_PATH + "/warning_pheromone.csv",
        round(xmin),
        round(ymin),
        round(xmax),
        round(ymax),
    )

    resource_indicator_pheromone = None

    # Calculate simulation parameters based on loaded data
    total_bots = len(bots_data)

    # Calculate plot parameters based on loaded data
    first_key = list(bots_data.keys())[0]
    tick_list = bots_data[first_key]["x"].keys()
    total_frames = len(tick_list)
    frame_to_tick_map = (
        tick_list  # this assums even distribution of recorded ticks in input files
    )
    max_frame = total_frames - 1
    max_tick = frame_to_tick_map[max_frame]

    # Setup plot
    fig = plt.figure()
    fig.set_dpi(fig.get_dpi() * OUTPUT_RESOLUTION_SCALE)
    axArena: plt.Axes = fig.subplots()
    # axArena: plt.Axes = fig.subplots()

    # Draw
    setup()
    animate(0)
    # axArena.get_legend().remove()
    # for spineName in ["top", "bottom", "left", "right"]:
    #     axArena.spines[spineName].set_visible(False)
    # axArena.axis("off")
    # plt.savefig(
    #     OUTPUT_DIRECTORY_PATH + "/single_frame.png", bbox_inches=0, pad_inches=0
    # )
    plt.show()

    # Animate
    # animation = anim.FuncAnimation(fig, animate, frames=total_frames, interval=20, repeat=False)
    # saving to gif using FFMpeg writer
    # writer = anim.FFMpegWriter(fps=60)
    # animation.save(OUTPUT_DIRECTORY_PATH + "/" + "animation.mp4", writer=writer)
