import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator

from os.path import exists
import pandas as pd
import numpy as np


GRAPH_Y_TICK_SECTIONS = 5
BAR_WIDTH = 0.25  # The width of the bars

RUN_TYPES = [2, 3, 4]

INPUT_DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/save/post_wieseke_1000"
INPUT_PATH = INPUT_DIRECTORY_PATH + "/avg_results.csv"
OUTPUT_PATH_DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/visual/revised/output"
OUTPUT_PATH = OUTPUT_PATH_DIRECTORY_PATH + "/avg_results_bars.pgf"


def cm_to_inches(input: float) -> float:
    return input / 2.54


def setup(ax: plt.Axes, ymax: float):
    ax.set(ylim=(0, ymax))

    # ax.spines["bottom"].set_position("zero")
    # ax.spines["left"].set_position("zero")
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)

    # ax.spines["bottom"].set
    # ax.spines["left"].set_axisline_style("-|>")

    # ax.plot(1, 0, ">k", transform=ax.get_yaxis_transform(), clip_on=False)
    # ax.plot(0, 1, "^k", transform=ax.get_xaxis_transform(), clip_on=False)

    # ax.set_xlabel("")
    ax.set_ylabel("Wert")

    ax.xaxis.set_ticks([])
    # ax.xaxis.set_major_locator(MultipleLocator(xmax / GRAPH_X_TICK_SECTIONS))
    ax.yaxis.set_major_locator(MultipleLocator(ymax / GRAPH_Y_TICK_SECTIONS))
    # ax.xaxis.set_minor_locator(AutoMinorLocator(5.0))
    ax.yaxis.set_minor_locator(AutoMinorLocator(5.0))

    ax.grid(which="major", color="grey", linewidth=1, linestyle="-", alpha=0.4)
    ax.grid(which="minor", color="grey", linewidth=1, linestyle="-", alpha=0.1)

    ax.xaxis.grid(False)
    ax.set_axisbelow(True)


def draw(ax: plt.Axes):
    if not exists(INPUT_PATH):
        return

    with open(INPUT_PATH) as input_file:
        df = pd.read_csv(input_file, index_col="run_type")

        index = np.arange(start=1, stop=len(RUN_TYPES) + 1)

        print(index)

        ax.set_xticks(index)
        ax.set_xticklabels(RUN_TYPES)

        values_transmiision = [df["transmission_1"][run] for run in RUN_TYPES]
        values_productivity = [df["productivity_1"][run] for run in RUN_TYPES]
        values_efficiency = [df["efficiency_1"][run] for run in RUN_TYPES]

        bars_transmission = ax.bar(
            index - BAR_WIDTH,
            values_transmiision,
            BAR_WIDTH,
            label="Krankheitsausbreitung",
        )
        bars_productivity = ax.bar(
            index, values_productivity, BAR_WIDTH, label="Produktivität"
        )
        bars_efficiency = ax.bar(
            index + BAR_WIDTH, values_efficiency, BAR_WIDTH, label="Effizienz"
        )

        ax.legend()


def main():
    # matplotlib.use("pgf")
    matplotlib.rcParams.update(
        {
            "pgf.texsystem": "pdflatex",
            "font.family": "serif",
            "font.size": 11,
            "text.usetex": True,
            "pgf.rcfonts": False,
        }
    )

    fig, ax = plt.subplots()

    textwidth_in_cm: float = 16.39766
    figure_width_scale: float = 0.75
    fig.set_figwidth(cm_to_inches(textwidth_in_cm * figure_width_scale))
    fig.set_figheight(fig.get_figheight() * figure_width_scale)

    setup(ax, 4.0)
    draw(ax)

    plt.show()
    plt.savefig(OUTPUT_PATH)


if __name__ == "__main__":
    main()
