from src.settings import SEIR_DICT
import src.data_loader as dl

import sys

DEFAULT_INPUT_DIRECTORY : str = "/home/philipp/Gits/bachelor-repast/save/output_100_2/avg/"

def main():
    command_arguments = sys.argv[1:]
    if len(command_arguments) == 0:
        input_directory = DEFAULT_INPUT_DIRECTORY
    elif len(command_arguments) == 1:
        input_directory = command_arguments[0]
    else:
        sys.exit("Incoorect number of arguments given. Either give none for default input or use \"python3 ./get_max.py [input_directory]\"")



    infection_sums = dl.load_infection_sums(input_directory + "/infection_sums.csv")
    infection_tick_list = infection_sums[infection_sums.keys()[0]].keys()

    for state in infection_sums.keys():
        state_counts = [infection_sums[state][tick] for tick in infection_tick_list]
        state_max = max(state_counts)
        print("Maximum {state_name}: {state_max}".format(state_name=state, state_max=state_max))

    resource_sum = dl.load_resource_sums(input_directory + "/resource_sum.csv")
    resource_tick_list = resource_sum[resource_sum.keys()[0]].keys()

    for field in resource_sum.keys():
        field_counts = [resource_sum[field][tick] for tick in resource_tick_list]
        field_max = max(field_counts)
        print("Maximum {field_name}: {field_max}".format(field_name=field, field_max=field_max))



if __name__ == "__main__":
    main()