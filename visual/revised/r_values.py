from src.settings import SEIR_DICT

def calculate_change_in_list(list: list[float]) -> list[float]:
    return [list[index] - list[max(0, index - 1)] for index in range(len(list))]

def calculate_new_infections(infection_sums: dict[str, dict[int, float]]) -> list[float]:
    susceptible = SEIR_DICT[0].name_file

    change = calculate_change_in_list(infection_sums[susceptible])

    return [abs(i) for i in change]

def calculate_averaged_new_infections(infection_sums: dict[str, dict[int, float]], average_over_timesteps: int):
    change = calculate_new_infections(infection_sums)

    return [sum(change[max(0, i - average_over_timesteps):i]) / average_over_timesteps for i in range(len(change))]

def calculate_r_values(infection_sums: dict[str, dict[int, float]], tick_intervall: int) -> dict[int, float]:
    # based on:  https://doi.org/10.1093/aje/kwt133
    # https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Projekte_RKI/R-Wert-Erlaeuterung.pdf?__blob=publicationFile
    result = dict()

    susceptible = SEIR_DICT[0].name_file
    tick_list = list(infection_sums[susceptible].keys())

    # calculate list of new infections
    new_infections_average_over_intervall_days = calculate_averaged_new_infections(infection_sums, tick_intervall)

    for tick in tick_list:
        now = new_infections_average_over_intervall_days[tick]
        before = new_infections_average_over_intervall_days[max(0, tick - tick_intervall)]

        if (before != 0):
            r_value = now / before
        else:
            r_value = -float('Inf')

        result[tick] = r_value


    return result

def main():
    import src.data_loader as dl

    input_file = "/home/philipp/Gits/bachelor-repast/save/output_100_0/avg/infection_sums.csv"
    infections_sums = dl.load_infection_sums(input_file)

    r_values = calculate_r_values(infections_sums, 10)
    
    max_value = -float("Inf")
    max_index = 0

    for i in r_values.keys():
        if r_values[i] > max_value:
            max_index = i
            max_value = r_values[i]

    print("Max: {index}: {value}".format(index = max_index, value = max_value))
    for i in r_values.keys():
        print("{index}: {value}".format(index = i, value = r_values[i]))

if __name__ == "__main__":
    main()