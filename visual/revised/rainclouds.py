import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, LinearLocator
from matplotlib.pyplot import Axes
import matplotlib.collections as clt

import seaborn as sns

sns.set(style="whitegrid", font_scale=2)
import ptitprince as pt

from src.settings import SEIR_DICT
import src.data_loader as dl

import src.seir as seir

import sys

INFECTION_GRAPH_X_TICK_SECTIONS = 5
INFECTION_GRAPH_Y_TICK_SECTIONS = 5

X_VALUE_OFFSET = 2

DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/save/post_wieseke/output_7"
INPUT_DIRECTORY_PATH = DIRECTORY_PATH
OUTPUT_DIRECTORY_PATH = DIRECTORY_PATH


def setup(ax: Axes, xmin: int, xmax: int, graph_x_tick_sections: int):
    min_value = max(0, xmin - X_VALUE_OFFSET)
    max_value = max(0, xmax + X_VALUE_OFFSET)
    ax.set(xlim=(min_value, max_value))
    ax.set_aspect(20.0)

    ax.spines["bottom"].set_visible(True)
    ax.spines["bottom"].set_position(("axes", 0.0))

    ax.spines["left"].set_visible(False)
    ax.spines["left"].set_position(("axes", 0.0))

    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.xaxis.tick_bottom()

    ax.set_xlabel("")
    ax.set_ylabel("")

    ax.yaxis.set_ticks([])
    ax.xaxis.set_major_locator(LinearLocator(numticks=graph_x_tick_sections + 1))
    ax.xaxis.set_minor_locator(AutoMinorLocator(5.0))

    ax.grid(which="major", color="grey", linewidth=1, linestyle="-", alpha=0.4)
    ax.grid(which="minor", color="grey", linewidth=1, linestyle="-", alpha=0.1)


def cm_to_inches(input: float) -> float:
    return input / 2.54


def main():
    command_arguments = sys.argv[1:]
    if len(command_arguments) == 0:
        # no arguments given. Use Defaults
        input_file = INPUT_DIRECTORY_PATH + "/distribution.csv"
        input_column = "imax"
        output_file = INPUT_DIRECTORY_PATH + "raincloud.pgf"
    elif len(command_arguments) == 3:
        input_file = command_arguments[0]
        input_column = command_arguments[1]
        output_file = command_arguments[2]
    else:
        sys.exit(
            'Incoorect number of arguments given. Either give none for default input/output or use "python3 ./rainclouds.py [input_file] [input_column] [output_file]"'
        )

    df = dl.load_df(input_file, "run")
    data = [df[input_column][key] for key in df[input_column].keys()]
    xmin = min(data)
    xmax = max(data)

    matplotlib.use("pgf")
    matplotlib.rcParams.update(
        {
            "pgf.texsystem": "pdflatex",
            "font.family": "serif",
            "font.size": 11,
            "text.usetex": True,
            "pgf.rcfonts": False,
        }
    )

    fig, ax = plt.subplots()
    textwidth_in_cm: float = 16.39766
    figure_width_scale: float = 0.75
    aspect: float = fig.get_figwidth() / fig.get_figheight()
    new_width: float = cm_to_inches(textwidth_in_cm * figure_width_scale)
    fig.set_figwidth(new_width)
    fig.set_figheight(aspect * new_width)

    setup(ax, xmin, xmax, INFECTION_GRAPH_X_TICK_SECTIONS)
    # ax.set_axisbelow(True)
    # plotting the clouds
    dy = input_column
    ort = "h"
    pal = "Set2"
    sigma = 0.2

    ax = pt.RainCloud(
        y=dy,
        data=df,
        palette=pal,
        bw=sigma,
        width_viol=0.6,
        ax=ax,
        orient=ort,
        move=0.2,
    )

    setup(ax, xmin, xmax, INFECTION_GRAPH_X_TICK_SECTIONS)

    # plt.show()
    plt.savefig(output_file, bbox_inches="tight", pad_inches=0)


if __name__ == "__main__":
    main()
