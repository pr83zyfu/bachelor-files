import matplotlib
import matplotlib.pyplot as plt

from src.settings import SEIR_DICT
import src.data_loader as dl

import src.resource as resource

import sys

RESOURCE_GRAPH_X_TICK_SECTIONS = 5
RESOURCE_GRAPH_Y_TICK_SECTIONS = 5

RESOURCE_GRAPH_WITH_COLOUR = "blue"
RESOURCE_GRAPH_WITHOUT_COLOUR = "red"

DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/save/post_wieseke/output_7"
INPUT_DIRECTORY_PATH = DIRECTORY_PATH + "/avg"
OUTPUT_DIRECTORY_PATH = DIRECTORY_PATH


def main():
    command_arguments = sys.argv[1:]
    if len(command_arguments) == 0:
        # no arguments given. Use Defaults
        input_file = INPUT_DIRECTORY_PATH + "/resource_sum.csv"
        output_file = OUTPUT_DIRECTORY_PATH + "/resource.pgf"
    elif len(command_arguments) == 2 or len(command_arguments) == 3:
        input_file = command_arguments[0]
        output_file = command_arguments[1]
    else:
        sys.exit(
            'Incorrect number of arguments given. Either give none for default input/output or use "python3 ./seir_graphs.py [input_file] [output_file] [max y value (optional)]"'
        )

    matplotlib.use("pgf")
    matplotlib.rcParams.update(
        {
            "pgf.texsystem": "pdflatex",
            "font.family": "serif",
            "text.usetex": True,
            "pgf.rcfonts": False,
        }
    )

    resource_sums = dl.load_resource_sums(input_file)

    fig, ax = plt.subplots()

    tick_list = resource_sums["total"].keys()
    max_tick = tick_list[len(tick_list) - 1]
    max_sum = resource_sums["total"][max_tick]

    if len(command_arguments) == 3:
        max_sum = float(command_arguments[2])

    # resource.setup(ax, max_tick, max_sum, RESOURCE_GRAPH_X_TICK_SECTIONS, RESOURCE_GRAPH_Y_TICK_SECTIONS)
    resource.setup(
        ax,
        max_tick,
        135,
        RESOURCE_GRAPH_X_TICK_SECTIONS,
        RESOURCE_GRAPH_Y_TICK_SECTIONS,
    )

    resource.draw(
        ax,
        tick_list,
        resource_sums,
        RESOURCE_GRAPH_WITH_COLOUR,
        "Summe der gesammelten Ressourcen",
        "-",
    )
    ax.legend()

    # plt.show()
    plt.savefig(output_file)


if __name__ == "__main__":
    main()
