import matplotlib
import matplotlib.pyplot as plt

from src.settings import SEIR_DICT
import src.data_loader as dl

import src.seir as seir

import sys

INFECTION_GRAPH_X_TICK_SECTIONS = 5
INFECTION_GRAPH_Y_TICK_SECTIONS = 5

DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/simulation/output"
INPUT_DIRECTORY_PATH = DIRECTORY_PATH
OUTPUT_DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/visual/revised/output"


def cm_to_inches(input: float) -> float:
    return input / 2.54


def main():
    command_arguments = sys.argv[1:]
    if len(command_arguments) == 0:
        # no arguments given. Use Defaults
        input_file = INPUT_DIRECTORY_PATH + "/infection_sums.csv"
        output_file = OUTPUT_DIRECTORY_PATH + "/seir.pgf"
    elif len(command_arguments) == 2:
        input_file = command_arguments[0]
        output_file = command_arguments[1]
    else:
        sys.exit(
            'Incorrect number of arguments given. Either give none for default input/output or use "python3 ./seir_graphs.py [input_file] [output_file] "'
        )

    matplotlib.use("pgf")
    matplotlib.rcParams.update(
        {
            "pgf.texsystem": "pdflatex",
            "font.family": "serif",
            "font.size": 11,
            "text.usetex": True,
            "pgf.rcfonts": False,
        }
    )

    infection_sums = dl.load_infection_sums(input_file)

    fig, ax = plt.subplots()

    textwidth_in_cm: float = 16.39766
    figure_width_scale: float = 0.75
    fig.set_figwidth(cm_to_inches(textwidth_in_cm * figure_width_scale))
    fig.set_figheight(fig.get_figheight() * figure_width_scale)

    tick_list = infection_sums[SEIR_DICT[0].name_file].keys()
    max_tick = tick_list[len(tick_list) - 1]
    total_bots = sum([infection_sums[key][0] for key in infection_sums.keys()])

    seir.setup(
        ax,
        max_tick,
        total_bots,
        INFECTION_GRAPH_X_TICK_SECTIONS,
        INFECTION_GRAPH_Y_TICK_SECTIONS,
    )
    seir.draw(ax, tick_list, infection_sums, SEIR_DICT, [0, 3, 1, 2])
    # plt.show()
    plt.savefig(output_file)


if __name__ == "__main__":
    main()
