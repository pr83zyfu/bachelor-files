import matplotlib
import matplotlib.pyplot as plt

from src.settings import SEIR_DICT
import src.data_loader as dl

import src.seir as seir
import src.seir_wikipedia as wiki

INFECTION_GRAPH_X_TICK_SECTIONS = 5
INFECTION_GRAPH_Y_TICK_SECTIONS = 5

OUTPUT_DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/visual/revised/output"

OUTPUT_RESOLUTION_SCALE = 2.0

def simulation1():
    N = 83_200_000 # Einwohnerzahl von Deutschland 2019/2020
    R0 = 2.4; gamma = 1/3.0
    return wiki.SEIR_simulation(
        alpha = 1/5.5, 
        beta = R0*gamma, 
        gamma = gamma,
        e0 = 40_000.0/N, 
        i0 = 10_000.0/N, 
        days = 120)

def load_reference_seir_sums():
    result = dict()
    for state in SEIR_DICT.keys():
        result[SEIR_DICT[state].name_file] = dict()

    t,x = zip(*simulation1())
    s, e, i, r = zip(*x)

    result[SEIR_DICT[0].name_file] = s
    result[SEIR_DICT[1].name_file] = e
    result[SEIR_DICT[2].name_file] = i
    result[SEIR_DICT[3].name_file] = r

    return list(t), result

def cm_to_inches(input: float) -> float:
    return input / 2.54

if __name__ == "__main__":
    matplotlib.use("pgf")
    matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'font.size': 11,
    'text.usetex': True,
    'pgf.rcfonts': False,
    })
    
    tick_list, infection_sums = load_reference_seir_sums()

    fig, ax = plt.subplots()

    textwidth_in_cm :float = 16.39766
    figure_width_scale :float = 0.75
    fig.set_figwidth(cm_to_inches(textwidth_in_cm * figure_width_scale))
    fig.set_figheight(fig.get_figheight() * figure_width_scale)

    max_tick = tick_list[len(tick_list) - 1]
    total_bots = sum([infection_sums[key][0] for key in infection_sums.keys()])

    seir.setup(ax, max_tick, total_bots, INFECTION_GRAPH_X_TICK_SECTIONS, INFECTION_GRAPH_Y_TICK_SECTIONS)
    seir.setup_reference(ax, max_tick, total_bots, INFECTION_GRAPH_X_TICK_SECTIONS, INFECTION_GRAPH_Y_TICK_SECTIONS)
    seir.draw(ax, tick_list, infection_sums, SEIR_DICT, [0, 3, 1, 2])

    plt.savefig(OUTPUT_DIRECTORY_PATH + "/seir_example_germany.pgf")
    #plt.show()