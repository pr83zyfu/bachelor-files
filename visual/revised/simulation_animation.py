import matplotlib.pyplot as plt
import matplotlib.animation as anim

from src.settings import SEIR_DICT
import src.data_loader as dl

import src.arena as arena
import src.pheromone as pheromone
import src.seir as seir

ARENA_WIDTH = 100
ARENA_HEIGHT = 100
ARENA_X_TICK_SECTIONS = 10
ARENA_Y_TICK_SECTIONS = 10

BOT_TAIL_LENGTH = 0
BOT_TAIL_COLOUR = "black"
BOT_TAIL_OPACITY = 0.5

INFECTION_GRAPH_X_TICK_SECTIONS = 10
INFECTION_GRAPH_Y_TICK_SECTIONS = 10

RESOURCE_INDICATOR_COLOUR = "green"
WARN_PHEROMONE_COLOUR = "red"

INPUT_DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/simulation/output"
OUTPUT_DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/visual/revised/output"

OUTPUT_RESOLUTION_SCALE = 2.0


def setup():
    fig.patch.set_facecolor('#ffffff')

    arena.setup(axArena, xmin, ymin, xmax, ymax, ARENA_X_TICK_SECTIONS, ARENA_Y_TICK_SECTIONS)
    seir.setup(axInfection, max_tick, total_bots, INFECTION_GRAPH_X_TICK_SECTIONS, INFECTION_GRAPH_Y_TICK_SECTIONS)
    # Draw infection graph
    seir.draw(axInfection, tick_list, infection_sums, SEIR_DICT, [0, 3, 1, 2])

def animate(i):
    # clear arena plot by removing all lines
    for _ in range(len(axArena.lines)):
        axArena.lines.pop(0)

    current_tick = frame_to_tick_map[i]

    # Draw Arena
    if resource_indicator_pheromone is not None:
        pheromone.draw(axArena, resource_indicator_pheromone, current_tick, round(xmin), round(xmax), round(ymin), round(ymax), RESOURCE_INDICATOR_COLOUR)
    if warn_pheromone is not None:
        pheromone.draw(axArena, warn_pheromone, current_tick, round(xmin), round(xmax), round(ymin), round(ymax), WARN_PHEROMONE_COLOUR)
    # Resource States
    arena.draw(axArena, current_tick, bots_data, xmin, ymin, xmax, ymax, SEIR_DICT, BOT_TAIL_LENGTH, BOT_TAIL_COLOUR, BOT_TAIL_OPACITY)

    # Draw productivity graph

    print("Plotted " + str(i) + " of " + str(total_frames) + " total frames.", end="\r")


if __name__=="__main__":
    # Load simulation properties
    xmin, xmax, ymin, ymax = -ARENA_WIDTH/2.0, ARENA_WIDTH/2.0, -ARENA_HEIGHT/2.0, ARENA_HEIGHT/2.0

    # Load simulation output
    bots_data                       = dl.load_bots_data_condensed(INPUT_DIRECTORY_PATH + "/agents_condensed.csv")
    print("Loaded data of " + str(len(bots_data)) + " bots.")
    resource_indicator_pheromone    = dl.load_pheromone(INPUT_DIRECTORY_PATH + "/resource_indicator_pheromone.csv", round(xmin), round(ymin), round(xmax), round(ymax))
    warn_pheromone                  = dl.load_pheromone(INPUT_DIRECTORY_PATH + "/warningPheromone.csv", round(xmin), round(ymin), round(xmax), round(ymax))
    infection_sums                  = dl.load_infection_sums(INPUT_DIRECTORY_PATH + "/infection_sums.csv")

    # Calculate simulation parameters based on loaded data
    total_bots = len(bots_data)

    # Calculate plot parameters based on loaded data
    first_key = list(bots_data.keys())[0]
    tick_list = bots_data[first_key]["x"].keys()
    total_frames = len(tick_list)
    frame_to_tick_map = tick_list # this assums even distribution of recorded ticks in input files
    max_frame = total_frames - 1
    max_tick = frame_to_tick_map[max_frame]

    # Setup plot
    fig = plt.figure()
    fig.set_dpi(fig.get_dpi() * OUTPUT_RESOLUTION_SCALE)
    axArena         = plt.subplot2grid((2, 4), (0, 0), rowspan=2, colspan=2)
    axInfection     = plt.subplot2grid((2, 4), (0, 2), colspan=2)
    axProductivity  = plt.subplot2grid((2, 4), (1, 2), colspan=2)
    #ax11            = plt.subplot2grid((3, 4), (1, 1))
    setup()

    # Animate
    animation = anim.FuncAnimation(fig, animate, frames=total_frames, interval=20, repeat=False)
    # saving to gif using FFMpeg writer
    writer = anim.FFMpegWriter(fps=60)
    animation.save(OUTPUT_DIRECTORY_PATH + "/" + "animation.mp4", writer=writer)   
