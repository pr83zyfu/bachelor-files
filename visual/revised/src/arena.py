from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)

def setup(ax, xmin, ymin, xmax, ymax, arena_x_tick_sections, arena_y_tick_sections, aspect: str = "equal"):
    # Set up cartesian plot (https://pygmalion.nitri.org/cartesian-coordinates-with-matplotlib-1263.html)
    ax.set(xlim=(xmin, xmax), ylim=(ymin, ymax), aspect=aspect)

    ax.spines['bottom'].set_position(('outward', 10.0))
    ax.spines['left'].set_position(('outward', 10.0))
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    ax.set_xlabel('$x$', size=14, labelpad=-24, x=1.02)
    ax.set_ylabel('$y$', size=14, labelpad=-21, y=1.02, rotation=0)
    #plt.text(0.49, 0.49, r"$O$", ha='right', va='top', transform=ax.transAxes, horizontalalignment='center', fontsize=14)
    
    # Change grid lines to be shown every TICK_FREQUENCY steps in big and smaller every TICK_FREQUENCY/5 steps
    ax.xaxis.set_major_locator(MultipleLocator((xmax - xmin) / arena_x_tick_sections))
    ax.yaxis.set_major_locator(MultipleLocator((ymax - ymin) / arena_y_tick_sections))
    ax.xaxis.set_minor_locator(AutoMinorLocator(5.0))
    ax.yaxis.set_minor_locator(AutoMinorLocator(5.0))

    ax.grid(which='major', color='grey', linewidth=2, linestyle='-', alpha=0.3)
    ax.grid(which='minor', color='grey', linewidth=1, linestyle='-', alpha=0.1)

def draw(ax, current_tick, bots_data, xmin, ymin, xmax, ymax, seir_dict, bot_tail_length: int, bot_tail_colour: str, bot_tail_opacity: float):
    # for every bot
    for k in bots_data:
        df = bots_data[k]

        # draw bot tail
        tail_x = []
        tail_y = []
        for l in range(bot_tail_length):
            if current_tick - l < 1:
                break
            tail_x.append(df["x"][current_tick - l])
            tail_y.append(df["y"][current_tick - l])

        ax.plot(tail_x, tail_y, color=bot_tail_colour, alpha=bot_tail_opacity)

    # draw bot current position in infection state colour
        x = [df["x"][current_tick]]
        y = [df["y"][current_tick]]
        seir_state = df["infection state"][current_tick]
        ax.plot(
            x, 
            y, 
            marker=seir_dict[seir_state].marker_style, 
            markersize=seir_dict[seir_state].marker_size, 
            markerfacecolor=seir_dict[seir_state].colour, 
            markeredgecolor=seir_dict[seir_state].marker_edge_colour,
            linestyle='None'
        )
    

    # setup_legend(..) needs x and y coordinates that are outside of the plotted arena as inputs
    setup_legend(ax, seir_dict, xmin - 100.0, ymin - 100.0)
    ax.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0)

def setup_legend(ax, seir_dict, x_outside_view: float, y_outside_view: float):
    for j in seir_dict.keys():
        ax.plot(
            x_outside_view,
            y_outside_view,
            marker=seir_dict[j].marker_style,
            markersize=seir_dict[j].marker_size,
            markerfacecolor=seir_dict[j].colour,
            markeredgecolor=seir_dict[j].marker_edge_colour,
            label=seir_dict[j].name_short,
            linestyle='None'
        )