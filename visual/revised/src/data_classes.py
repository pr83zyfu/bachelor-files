from dataclasses import dataclass

@dataclass
class SEIRState:
    id: int
    name: str
    name_file: str
    name_short: str
    colour: str
    line_style: str
    line_opacity: float
    line_width: float
    marker_style: str
    marker_size: float
    marker_edge_colour: str