def draw(ax, pheromone_map: dict, current_tick: int, xmin: int, xmax: int, ymin: int, ymax: int, colour: str, xlist, ylist, X, Y):
    Z = [[pheromone_map[x][y][current_tick] for y in ylist] for x in xlist]
    ax.contourf(
        X,
        Y,
        Z,
        colors=("#ffffff", colour, colour),
        levels=[0.0, 0.5, 1.0]
    )

    return
    # for every cell
    for x in range(xmin, xmax):
        for y in range(ymin, ymax):
            if pheromone_map[x][y][current_tick] > 0:
                ax.plot(x, y, marker="s", markersize=2, markerfacecolor=colour, markeredgecolor=colour, linestyle="None")
