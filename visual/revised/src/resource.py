from matplotlib.ticker import AutoMinorLocator, MultipleLocator


def setup(ax, xmax, ymax, graph_x_tick_sections, graph_y_tick_sections):
    ax.set(xlim=(0, xmax), ylim=(0, ymax))

    ax.spines["bottom"].set_position("zero")
    ax.spines["left"].set_position("zero")
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)

    # ax.spines["bottom"].set
    # ax.spines["left"].set_axisline_style("-|>")

    # ax.plot(1, 0, ">k", transform=ax.get_yaxis_transform(), clip_on=False)
    # ax.plot(0, 1, "^k", transform=ax.get_xaxis_transform(), clip_on=False)

    ax.set_xlabel("Simulations Zeitschritt")
    ax.set_ylabel("Summe der gesammelten Ressourcen")

    ax.xaxis.set_major_locator(MultipleLocator(xmax / graph_x_tick_sections))
    ax.yaxis.set_major_locator(MultipleLocator(ymax / graph_y_tick_sections))
    ax.xaxis.set_minor_locator(AutoMinorLocator(5.0))
    ax.yaxis.set_minor_locator(AutoMinorLocator(5.0))

    ax.grid(which="major", color="grey", linewidth=1, linestyle="-", alpha=0.4)
    ax.grid(which="minor", color="grey", linewidth=1, linestyle="-", alpha=0.1)


def draw(ax, tick_list, resource_sums, colour: str, label: str, linestyle: str):
    ax.plot(
        tick_list,
        [resource_sums["total"][i] for i in tick_list],
        color=colour,
        label=label,
        linestyle=linestyle,
        alpha=1.0,
        linewidth=2.0,
    )

    # ax.legend()


def draw_resources(
    ax,
    current_tick,
    resource_data,
    marker_style: str,
    marker_size: float,
    marker_face_colour: str,
    marker_edge_colour: str,
    resource_max_size: float,
):
    for i in resource_data.keys():
        ax.plot(
            [resource_data[i]["x"][current_tick]],
            [resource_data[i]["y"][current_tick]],
            marker=marker_style,
            markersize=marker_size,
            markerfacecolor=marker_face_colour,
            markeredgecolor=marker_edge_colour,
            linestyle="None",
            alpha=(resource_data[i]["size"][current_tick] / resource_max_size),
        )
