from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)
from matplotlib.pyplot import Axes

def setup(ax: Axes, xmax, ymax, graph_x_tick_sections, graph_y_tick_sections):
    ax.set(xlim=(0, xmax), ylim=(0, ymax))

    ax.spines['bottom'].set_position('zero')
    ax.spines['left'].set_position('zero')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    #ax.spines["bottom"].set
    #ax.spines["left"].set_axisline_style("-|>")

    #ax.plot(1, 0, ">k", transform=ax.get_yaxis_transform(), clip_on=False)
    #ax.plot(0, 1, "^k", transform=ax.get_xaxis_transform(), clip_on=False)

    ax.set_xlabel("Simulations Zeitschritt")
    ax.set_ylabel("Größe der Gruppen (Anzahl der Agenten)")

    ax.xaxis.set_major_locator(MultipleLocator(xmax / graph_x_tick_sections))
    ax.yaxis.set_major_locator(MultipleLocator(ymax / graph_y_tick_sections))
    ax.xaxis.set_minor_locator(AutoMinorLocator(5.0))
    ax.yaxis.set_minor_locator(AutoMinorLocator(5.0))

    ax.grid(which='major', color='grey', linewidth=1, linestyle='-', alpha=0.4)
    ax.grid(which='minor', color='grey', linewidth=1, linestyle='-', alpha=0.1)

def setup_reference(ax: Axes, xmax, ymax, graph_x_tick_sections, graph_y_tick_sections):
    # is called after setup. Only make changes
    ax.set_xlabel("Vergange Zeit ab Anfang der Beobachtung")
    ax.set_ylabel("Relative Größe der SEIR-Gruppen")

def draw(ax : Axes, tick_list, infection_sums, seir_dict, draw_order, keep_label_order: bool = True):
    states_lists = dict()
    for key in seir_dict.keys():
        states_lists[key] = infection_sums[seir_dict[key].name_file]                     
    
    # create fake plots outside of graph to keep label order
    if keep_label_order:
        x = [-10_000]
        y = [-10_000]
        for state in states_lists.keys():
            ax.plot(
                x, 
                y, 
                color=seir_dict[state].colour, 
                label=seir_dict[state].name,
                linestyle=seir_dict[state].line_style, 
                alpha=seir_dict[state].line_opacity,
                linewidth=seir_dict[state].line_width
            )


    for j in states_lists.keys():
        state = draw_order[j]
        ax.plot(
            tick_list, 
            states_lists[state], 
            color=seir_dict[state].colour, 
            label=seir_dict[state].name,
            linestyle=seir_dict[state].line_style, 
            alpha=seir_dict[state].line_opacity,
            linewidth=seir_dict[state].line_width
        )
    
    # remove duplicate labels
    handles, labels = ax.get_legend_handles_labels()
    by_label = dict(zip(labels, handles))
    ax.legend(by_label.values(), by_label.keys())
