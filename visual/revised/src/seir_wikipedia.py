from numpy import array as vector

# kopiert von: https://de.wikipedia.org/wiki/SEIR-Modell#Beispielrechnung

# Explizites Euler-Verfahren
def euler_method(f, t0, x0, t1, h):
    t = t0; x = x0
    a = [[t, x]]
    for k in range(0, 1 + int((t1 - t0)/h)):
        t = t0 + k*h
        x = x + h*f(t, x)
        a.append([t, x])
    return a

def SEIR_model(alpha, beta, gamma):
    def f(t, x):
        s, e, i, r = x
        return vector([
            -beta*s*i,
            beta*s*i - alpha*e,
            alpha*e - gamma*i,
            gamma*i
        ])
    return f

def SEIR_simulation(alpha, beta, gamma, e0, i0, days, step=0.1):
    x0 = vector([1.0 - e0 - i0, e0, i0, 0.0])
    f = SEIR_model(alpha, beta, gamma)
    return euler_method(f, 0, x0, days, step)