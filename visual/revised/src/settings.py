from src.data_classes import SEIRState

SEIR_DICT = {
    0: SEIRState(
        0, "Susceptible", "susceptible", "S", "orange", "-", 1.0, 2.0, "o", 3.0, "black"
    ),
    1: SEIRState(1, "Exposed", "exposed", "E", "red", ":", 0.5, 2.0, "s", 3.0, "black"),
    2: SEIRState(
        2, "Infectious", "infectious", "I", "red", "--", 1.0, 2.0, "^", 3.0, "black"
    ),
    3: SEIRState(
        3, "Recovered", "recovered", "R", "green", "-.", 1.0, 2.0, "p", 1.0, "black"
    ),
}
