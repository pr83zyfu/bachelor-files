import matplotlib.pyplot as plt
import matplotlib.animation as anim
from matplotlib import cm
import numpy as np

from src.data_classes import SEIRState
from src.data_classes import load_simulation_parametrs_from_props
from src.Arena import Arena
from src.Pheromone import Pheromone
from src.Resources import Resources

ARENA_X_TICK_SECTIONS = 10
ARENA_Y_TICK_SECTIONS = 10

BOT_TAIL_LENGTH = 0
BOT_TAIL_COLOUR = "black"
BOT_TAIL_OPACITY = 0.5

INFECTION_GRAPH_X_TICK_SECTIONS = 10
INFECTION_GRAPH_Y_TICK_SECTIONS = 10

RESOURCE_MARKER_STYLE = "H"
RESOURCE_MARKER_SIZE = 10.0
RESOURCE_MARKER_FACE_COLOUR = "lime"
RESOURCE_MARKER_EDGE_COLOUR = "black"
RESOURCE_MARKER_OPACITY = 0.5

RESOURCE_INDICATOR_COLOUR = "green"
WARN_PHEROMONE_COLOUR = "red"

INPUT_DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/simulation/output"
OUTPUT_DIRECTORY_PATH = "/home/philipp/Gits/bachelor-repast/visual/v3/output"

OUTPUT_RESOLUTION_SCALE = 2.0

SEIR_DICT = {
    0: SEIRState(
        0, "Susceptible", "susceptible", "S", "orange", "-", 1.0, 2.0, "o", 3.0, "black"
    ),
    1: SEIRState(1, "Exposed", "exposed", "E", "red", ":", 0.5, 2.0, "s", 3.0, "black"),
    2: SEIRState(
        2, "Infectious", "infectious", "I", "red", "-", 1.0, 2.0, "^", 3.0, "black"
    ),
    3: SEIRState(
        3, "Recovered", "recovered", "R", "green", "--", 1.0, 2.0, "p", 1.0, "black"
    ),
}

RESOURCE_SEARCH_STATE_DICT = {
    0: SEIRState(
        0, "Explore", "explore", "E", "black", "-", 0.0, 0.0, "+", "1.0", "black"
    ),
    1: SEIRState(1, "Follow", "follow", "F", "green", "-", 0.0, 0.0, "o", 3.0, "black"),
    2: SEIRState(2, "Draw", "draw", "D", "red", "-", 0.0, 0.0, "^", 3.0, "black"),
    3: SEIRState(3, "WAIT", "wait", "W", "black", "-", 0.0, 0.0, "s", 2.0, "black"),
}


def init():
    arena.setup(ARENA_X_TICK_SECTIONS, ARENA_Y_TICK_SECTIONS)


def update(i):
    arena.clear()

    current_tick = frame_to_tick_map[i]

    arena.drawInfectionStates(current_tick)
    # arena.drawResourceSearchState(current_tick)
    # resource_indicator_pheromone.draw(current_tick)
    # warn_pheromone.draw(current_tick)
    # resources.draw(current_tick)

    arena.drawInfectionLegend()

    print("Plotted " + str(i) + " of " + str(total_frames) + " total frames.", end="\r")


if __name__ == "__main__":
    simulation_parameters = load_simulation_parametrs_from_props(
        INPUT_DIRECTORY_PATH + "/model.props"
    )

    # Setup plot
    fig = plt.figure()
    fig.patch.set_facecolor("#ffffff")
    fig.set_dpi(fig.get_dpi() * OUTPUT_RESOLUTION_SCALE)
    axArena: plt.Axes = fig.subplots(1, 1)

    arena = Arena(
        INPUT_DIRECTORY_PATH + "/agents_condensed.csv",
        SEIR_DICT,
        RESOURCE_SEARCH_STATE_DICT,
        axArena,
        simulation_parameters,
        2.0,
    )
    # resource_indicator_pheromone = Pheromone(
    #     INPUT_DIRECTORY_PATH + "/resource_indicator_pheromone.csv",
    #     axArena,
    #     1.0,
    #     RESOURCE_INDICATOR_COLOUR,
    #     OUTPUT_RESOLUTION_SCALE,
    # )
    # warn_pheromone = Pheromone(
    #     INPUT_DIRECTORY_PATH + "/warning_pheromone.csv",
    #     axArena,
    #     0.5,
    #     WARN_PHEROMONE_COLOUR,
    #     OUTPUT_RESOLUTION_SCALE,
    # )
    # resources = Resources(
    #     INPUT_DIRECTORY_PATH + "/resources_condensed.csv",
    #     axArena,
    #     simulation_parameters,
    #     1.5,
    #     OUTPUT_RESOLUTION_SCALE,
    # )

    frame_to_tick_map = arena.get_frame_to_tick_map()
    total_frames = len(frame_to_tick_map)
    start_frame = 1900

    # init()
    # update(750)
    # plt.show()
    # assert False

    # Animate
    animation = anim.FuncAnimation(
        fig=fig,
        func=update,
        init_func=init,
        frames=range(start_frame, total_frames),
        interval=1,
        repeat=False,
        blit=False,
    )
    # saving to gif using FFMpeg writer
    writer = anim.FFMpegWriter(fps=30)
    animation.save(OUTPUT_DIRECTORY_PATH + "/" + "animation.mp4", writer=writer)
