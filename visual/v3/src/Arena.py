import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator

from src.data_classes import SEIRState
from src.data_classes import SimulationParameters

CONDENSED_AGENT_X_COLUMN_PREFIX = "x_"
CONDENSED_AGENT_Y_COLUMN_PREFIX = "y_"
CONDENSED_AGENT_SEIR_COLUMN_PREFIX = "infection_state_"
CONDENSED_AGENT_RESOURCE_SEARCH_STATE_COLUMN_PREFIX = "resource_search_state_"


class Arena:
    data_frame: pd.DataFrame
    seir_dict: dict[int, SEIRState]
    rss_dict: dict[int, SEIRState]
    simulation_parameters: SimulationParameters
    ax: plt.Axes
    num_of_bots: int
    zorder: float

    def __init__(
        self,
        input_file: str,
        seir_dict: dict[int, SEIRState],
        resource_search_state_dict: dict[int, SEIRState],
        ax: plt.Axes,
        simulation_parameters: SimulationParameters,
        zorder: float,
    ):
        with open(input_file) as csv_file:
            self.data_frame = pd.read_csv(csv_file, index_col="tick")
        self.seir_dict = seir_dict
        self.rss_dict = resource_search_state_dict
        self.simulation_parameters = simulation_parameters
        self.ax = ax

        # every bot has 4 attributes (x, y, seir, rss)
        self.num_of_bots, remainder = divmod(len(self.data_frame.keys()), 4)
        assert remainder == 0

        self.zorder = zorder

    def drawInfectionStates(self, tick: int):
        # for every bot
        for k in range(self.num_of_bots):
            # draw bot current position in infection state colour
            x = [self.data_frame[CONDENSED_AGENT_X_COLUMN_PREFIX + str(k)][tick]]
            y = [self.data_frame[CONDENSED_AGENT_Y_COLUMN_PREFIX + str(k)][tick]]
            seir_state = self.data_frame[CONDENSED_AGENT_SEIR_COLUMN_PREFIX + str(k)][
                tick
            ]

            self.ax.plot(
                x,
                y,
                marker=self.seir_dict[seir_state].marker_style,
                markersize=self.seir_dict[seir_state].marker_size,
                markerfacecolor=self.seir_dict[seir_state].colour,
                markeredgecolor=self.seir_dict[seir_state].marker_edge_colour,
                linestyle="None",
                zorder=self.zorder,
            )

    def drawInfectionLegend(self):
        for j in self.seir_dict.keys():
            self.ax.plot(
                self.simulation_parameters.arena_width,
                self.simulation_parameters.arena_height,
                marker=self.seir_dict[j].marker_style,
                markersize=self.seir_dict[j].marker_size,
                markerfacecolor=self.seir_dict[j].colour,
                markeredgecolor=self.seir_dict[j].marker_edge_colour,
                label=self.seir_dict[j].name_short,
                linestyle="None",
            )
        # self.ax.legend()
        self.ax.legend(bbox_to_anchor=(1.05, 1), loc="upper left", borderaxespad=0)

    def drawResourceSearchState(self, tick: int):
        # for every bot
        for k in range(self.num_of_bots):
            # draw bot current position in infection state colour
            x = [self.data_frame[CONDENSED_AGENT_X_COLUMN_PREFIX + str(k)][tick]]
            y = [self.data_frame[CONDENSED_AGENT_Y_COLUMN_PREFIX + str(k)][tick]]
            rss = self.data_frame[
                CONDENSED_AGENT_RESOURCE_SEARCH_STATE_COLUMN_PREFIX + str(k)
            ][tick]

            self.ax.plot(
                x,
                y,
                marker=self.rss_dict[rss].marker_style,
                markersize=self.rss_dict[rss].marker_size,
                markerfacecolor=self.rss_dict[rss].colour,
                markeredgecolor=self.rss_dict[rss].marker_edge_colour,
                linestyle="None",
                zorder=self.zorder,
            )

    def clear(self):
        self.ax.lines.clear()

    def setup(self, arena_x_tick_sections, arena_y_tick_sections):
        xmin = self.simulation_parameters.arena_xmin
        xmax = self.simulation_parameters.arena_xmax
        ymin = self.simulation_parameters.arena_ymin
        ymax = self.simulation_parameters.arena_ymax
        # Set up cartesian plot (https://pygmalion.nitri.org/cartesian-coordinates-with-matplotlib-1263.html)
        self.ax.set(xlim=(xmin, xmax), ylim=(ymin, ymax), aspect="equal")

        self.ax.spines["bottom"].set_position(("outward", 10.0))
        self.ax.spines["left"].set_position(("outward", 10.0))
        self.ax.spines["top"].set_visible(False)
        self.ax.spines["right"].set_visible(False)

        self.ax.set_xlabel("$x$", size=14, labelpad=-24, x=1.02)
        self.ax.set_ylabel("$y$", size=14, labelpad=-21, y=1.02, rotation=0)
        # plt.text(0.49, 0.49, r"$O$", ha='right', va='top', transform=ax.transAxes, horizontalalignment='center', fontsize=14)

        # Change grid lines to be shown every TICK_FREQUENCY steps in big and smaller every TICK_FREQUENCY/5 steps
        self.ax.xaxis.set_major_locator(
            MultipleLocator((xmax - xmin) / arena_x_tick_sections)
        )
        self.ax.yaxis.set_major_locator(
            MultipleLocator((ymax - ymin) / arena_y_tick_sections)
        )
        self.ax.xaxis.set_minor_locator(AutoMinorLocator(5.0))
        self.ax.yaxis.set_minor_locator(AutoMinorLocator(5.0))

        self.ax.grid(which="major", color="grey", linewidth=2, linestyle="-", alpha=0.3)
        self.ax.grid(which="minor", color="grey", linewidth=1, linestyle="-", alpha=0.1)

    def get_frame_to_tick_map(self):
        first_key = list(self.data_frame.keys())[0]
        tick_list = self.data_frame[first_key].keys()
        return tick_list
