import pandas as pd
import matplotlib.pyplot as plt

POSITION_COORDINATES_SPLIT_SYMBOL = "_"

class Pheromone:
    data_frame: pd.DataFrame
    ax: plt.Axes
    colour: str
    cell_size: float
    zorder: float

    def __init__(self, input_file: str, ax: plt.Axes, zorder: float, colour: str, cell_size: float) -> None:
        with open(input_file) as csv_file:
            self.data_frame = pd.read_csv(csv_file, index_col="tick")
        self.ax = ax

        self.colour = colour
        self.cell_size = cell_size
        self.zorder = zorder


    def draw(self, tick: int):
        for key in self.data_frame.keys():
            intensity = self.data_frame[key][tick]

            if intensity > 0:
                x = [int(key.split(POSITION_COORDINATES_SPLIT_SYMBOL)[0])]
                y = [int(key.split(POSITION_COORDINATES_SPLIT_SYMBOL)[1])]

                self.ax.plot(
                    x,
                    y,
                    marker="s",
                    markersize=self.cell_size,
                    markerfacecolor=self.colour,
                    markeredgecolor=self.colour,
                    linestyle=None,
                    zorder=self.zorder,
                    alpha=intensity
                )

    
    def clear(self):
        self.ax.lines.clear()