import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator

from src.data_classes import SEIRState
from src.data_classes import SimulationParameters

CONDENSED_RESOURCE_X_COLUMN_PREFIX = "x_"
CONDENSED_RESOURCE_Y_COLUMN_PREFIX = "y_"
CONDENSED_RESOURCE_SIZE_COLUMN_PREFIX = "size_"


class Resources:
    data_frame: pd.DataFrame
    ax: plt.Axes
    zorder: float
    num_of_resources: int
    simulation_parameters: SimulationParameters
    resolution_scale: float

    def __init__(
        self,
        input_file: str,
        ax: plt.Axes,
        simulation_parameters: SimulationParameters,
        zorder: float,
        resolution_scale: float,
    ):
        with open(input_file) as csv_file:
            self.data_frame = pd.read_csv(csv_file, index_col="tick")
        self.ax = ax
        self.zorder = zorder
        self.simulation_parameters = simulation_parameters
        self.resolution_scale = resolution_scale

        # every resource has 3 attributes (x, y, size)
        self.num_of_resources, remainder = divmod(len(self.data_frame.keys()), 3)
        assert remainder == 0

    def draw(self, tick: int):
        for i in range(self.num_of_resources):
            x = [self.data_frame[CONDENSED_RESOURCE_X_COLUMN_PREFIX + str(i)][tick]]
            y = [self.data_frame[CONDENSED_RESOURCE_Y_COLUMN_PREFIX + str(i)][tick]]
            size = self.data_frame[CONDENSED_RESOURCE_SIZE_COLUMN_PREFIX + str(i)][tick]

            # plot detection radius as Hexagon
            self.ax.plot(
                x,
                y,
                marker="o",
                markersize=self.simulation_parameters.resource_radius_detection
                * self.resolution_scale,
                markerfacecolor="lime",
                markeredgecolor="black",
                alpha=(size / self.simulation_parameters.resource_size),
                zorder=self.zorder,
            )
