from dataclasses import dataclass


@dataclass
class SEIRState:
    id: int
    name: str
    name_file: str
    name_short: str
    colour: str
    line_style: str
    line_opacity: float
    line_width: float
    marker_style: str
    marker_size: float
    marker_edge_colour: str


@dataclass
class SimulationParameters:
    arena_width: float
    arena_height: float
    arena_xmin: float
    arena_xmax: float
    arena_ymin: float
    arena_ymax: float
    resource_count: int
    resource_size: int
    resource_radius_detection: float
    resource_radius_collection: float

    def __init__(self) -> None:
        pass


def get_value_from_line(line: str) -> str:
    # first get right hand part from = sign (the value), then remove trailing comments and all whitespaces
    return line.split("=")[1].split("#")[0].strip()


def get_key_value_pair_from_line(line: str) -> tuple[str, str]:
    # remove trailing comments
    line = line.split("#")[0]
    parts = line.split("=")
    parts = [part.strip() for part in parts]

    key = parts[0]
    value = parts[1]
    return (key, value)


def load_props_dict(input_file_path: str) -> dict[str, str]:
    result: dict[str, str] = dict()
    with open(input_file_path) as file:
        for line in file:
            # skip comments (strip necessary for commented out lines that start with a tab)
            if line.strip().startswith("#"):
                continue

            if line.find("=") != -1:
                (key, value) = get_key_value_pair_from_line(line)
                result[key] = value

    return result


def load_simulation_parametrs_from_props(input_file_path: str) -> SimulationParameters:
    base: dict[str, str] = load_props_dict(input_file_path)

    result = SimulationParameters()

    result.arena_width = float(base["arena.width"])
    result.arena_height = float(base["arena.height"])
    width = result.arena_width
    height = result.arena_height
    result.arena_xmin = -(width / 2.0)
    result.arena_xmax = width / 2.0
    result.arena_ymin = -(height / 2.0)
    result.arena_ymax = height / 2.0

    result.resource_count = int(base["resource.generator.count"])
    result.resource_size = int(base["resource.generator.size"])
    result.resource_radius_detection = float(base["agent.resource.detection.radius"])
    result.resource_radius_collection = float(base["agent.resource.collection.radius"])

    return result
